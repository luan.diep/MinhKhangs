﻿using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Common
{
    public class ReportHelper
    {
        public static Task GenerateXls<T>(List<T> datasource, string filePath)
        {
            return Task.Run(() =>
            {
                using (ExcelPackage pck = new ExcelPackage(new FileInfo(filePath)))
                {
                    //Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add(typeof(T).Name + "s");
                    ws.Cells["A1"].LoadFromCollection(datasource, true, TableStyles.Medium18);
                    ws.Column(20).Style.Numberformat.Format = "mm-dd-yyyy";
                    ws.Column(21).Style.Numberformat.Format = "mm-dd-yyyy";
                    ws.Cells.AutoFitColumns();
                    pck.Save();
                }
            });
        }
        public static Task GenerateXlsThietLapGia<T>(List<T> datasource, string filePath)
        {
            return Task.Run(() =>
            {
                using (ExcelPackage pck = new ExcelPackage(new FileInfo(filePath)))
                {
                    //Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add(typeof(T).Name + "s");
                    ws.Cells["A1"].LoadFromCollection(datasource, true, TableStyles.Medium18);
                    ws.Cells.AutoFitColumns();
                    pck.Save();
                }
            });
        }
        public static Task GenerateXlsPhieuKK<T>(List<T> datasource, string filePath)
        {
            return Task.Run(() =>
            {
                using (ExcelPackage pck = new ExcelPackage(new FileInfo(filePath)))
                {
                    //Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add(typeof(T).Name + "s");
                    ws.Cells["A1"].LoadFromCollection(datasource, true, TableStyles.Medium18);
                    ws.Column(2).Style.Numberformat.Format = "dd-mm-yyyy hh:mm:ss";
                    ws.Column(5).Style.Numberformat.Format = "dd-mm-yyyy hh:mm:ss";
                    ws.Cells.AutoFitColumns();
                    pck.Save();
                }
            });
        }
        public static Task GenerateDoiTacGiaoHang<T>(List<T> datasource, string filePath)
        {
            return Task.Run(() =>
            {
                using (ExcelPackage pck = new ExcelPackage(new FileInfo(filePath)))
                {
                    //Create the worksheet
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add(typeof(T).Name + "s");
                    ws.Cells["A1"].LoadFromCollection(datasource, true, TableStyles.Medium18);
                    ws.Column(13).Style.Numberformat.Format = "mm-dd-yyyy";
                    ws.Column(21).Style.Numberformat.Format = "mm-dd-yyyy";
                    ws.Column(24).Style.Numberformat.Format = "mm-dd-yyyy";
                    ws.Column(28).Style.Numberformat.Format = "mm-dd-yyyy";
                    ws.Cells.AutoFitColumns();
                    pck.Save();
                }
            });
        }

    }
}
