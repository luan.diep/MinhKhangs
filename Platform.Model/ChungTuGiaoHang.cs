﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Platform.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    // các đơn giao hàng
    [Table("ChungTuGiaoHang")]
    public partial class ChungTuGiaoHang
    {
        
        [Key]
        public string MaChungTuGiaoHang { get; set; }
        public string MaChungTuBanHang { get; set; }
        public string MaNguoiGiaoHang { get; set; }
        public string MaSoNhanVien { get; set; }
        public Nullable<int> TrongLuong { get; set; }
        public string DiaChiGui { get; set; }
        public string SdtNguoiNhan { get; set; }
        public Nullable<double> TongTienHang { get; set; }
        public Nullable<DateTime> GioGiaoDuKien { get; set; }
        public double PhiGiaoHang { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public  IEnumerable<TrangThaiGiaoHang> ChiTietTrangThais { get; set; }

   

        public virtual DoiTacGiaoHang DoiTacGiaoHang { get; set; }
    }
}
