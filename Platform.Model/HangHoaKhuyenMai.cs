﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   public class HangHoaKhuyenMai
    {
        public string MaHang { get; set; }
        public string TenHang { get; set; }
        public int MaDonViTinh { get; set; }
        public string TenDonViTinh { get; set; }
        public Nullable<double> SoLuongKhuyenMai2 { get; set; }
        public string HinhAnh { get; set; }

    }
}
