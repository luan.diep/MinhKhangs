﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model.Models
{
    public class ApplicationRole_KH : IdentityRole
    {
        public ApplicationRole_KH() : base()
        {

        }
        [StringLength(250)]
        public string Description { set; get; }
    }
}
