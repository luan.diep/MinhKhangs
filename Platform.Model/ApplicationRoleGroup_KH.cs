﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model.Models
{
    [Table("ApplicationRoleGroups")]
    public class ApplicationRoleGroup_KH
    {
        [Key]
        [Column(Order = 1)]
        public int GroupId { set; get; }

        [Column(Order = 2)]
        [StringLength(128)]
        [Key]
        public string RoleId { set; get; }

        [ForeignKey("RoleId")]
        public virtual ApplicationRole_KH ApplicationRole_KH { set; get; }

        [ForeignKey("GroupId")]
        public virtual ApplicationGroup_KH ApplicationGroup_KH { set; get; }
    }
}
