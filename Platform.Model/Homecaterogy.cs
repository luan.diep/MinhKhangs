﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   public class Homecaterogy
    {

        public string MaHang { get; set; }
        public string TenHang { get; set; }
        public string MoTa { get; set; }
        public Nullable<double> GiaBan { get; set; }
        public string ThanhPham { get; set; }
        public string HinhAnh { get; set; }
        public int MaNhomHH { get; set; }
        public string TenNhom { get; set; }
        public Nullable<double> SoLuongTonThuc { get; set; }

    }
}
