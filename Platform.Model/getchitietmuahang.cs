﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
 public   class getchitietmuahang
    {

        public int MaCTDMH { get; set; }
        public string MaDonMuaHang { get; set; }
        public string MaHang { get; set; }
        public string TenHang { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<int> MaDonViTinh { get; set; }
        public Nullable<double> SoLuongNhan { get; set; }
        public Nullable<double> VAT { get; set; }
        public Nullable<double> ChietKhau { get; set; }
        public Nullable<double> DonGia { get; set; }
        public Nullable<double> ThanhTien { get; set; }
        public Nullable<double> ThueGTGT { get; set; }
        public Nullable<double> TienThueGTGT { get; set; }
        public Nullable<double> GiaKhuyenMai { get; set; }
        public Nullable<double> GiaNhap { get; set; }
        public Nullable<double> TienChietKhau { get; set; }
        public string LenhSanXuat { get; set; }
        public string ThanhPham { get; set; }
        public string TenDonViTinh { get; set; }
    }
}
