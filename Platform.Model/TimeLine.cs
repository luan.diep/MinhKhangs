﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   public class TimeLine
    {
        public int MaTrangThai { get; set; }
        public string TenTrangThai { get; set; }
        public Nullable<System.DateTime> NgayGio { get; set; }
    }
}
