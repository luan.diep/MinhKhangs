﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
    public class HangHoaOnl
    {
        public string MaHang { get; set; }
        public string TenHang { get; set; }
        public int MaDonViTinh { get; set; }
        public string MaCoSo { get; set; }
        public Nullable<bool> heart { get; set; }
        public string TenCoSo { get; set; }
        public string TenDonViTinh { get; set; }
        public string MoTa { get; set; }
        public Nullable<double> GiaNhap { get; set; }
        public Nullable<double> GiaBan { get; set; }
        public Nullable<double> GiaKhuyenMai { get; set; }
        public string KhuyenMai2 { get; set; }
        public Nullable<double> GiaNhapCuoi { get; set; }
        public Nullable<double> VAT { get; set; }
        public Nullable<double> ChietKhau { get; set; }
        public string ThanhPham { get; set; }
        public string HinhAnh { get; set; }
        public int MaNhomHH { get; set; }
        public string TenNhom { get; set; }
        public Nullable<double> SoLuongKhuyenMai2 { get; set; }
        public Nullable<double> SoLuongTonThuc { get; set; }
        public Nullable<bool> NgungKinhDoanh { get; set; }
        public string NhieuHinhAnh { get; set; }
        public HangHoaKhuyenMai ChiTietKhuyenMai { get; set; }
      
    }
}
