﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   
    [Table("Video")]
    public partial class Video
    {
        [Key]
        public int MaViDeo { get; set; }
        public string DuongDan { get; set; }
        public string GhiChu { get; set; }
        public string TieuDe { get; set; }
        public string NoiDung { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }





    }
}
