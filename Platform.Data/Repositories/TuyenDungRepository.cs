﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
   
    public interface ITuyenDungRepository : IRepository<TuyenDung>
    {

    }

    class TuyenDungRepository : RepositoryBase<TuyenDung>, ITuyenDungRepository
    {
        public TuyenDungRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }


    }
}
