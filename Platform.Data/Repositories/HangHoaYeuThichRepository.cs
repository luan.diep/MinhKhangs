﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IHangHoaYeuThichRepository : IRepository<HangHoaYeuThich>
    {

    }

    class HangHoaYeuThichRepository : RepositoryBase<HangHoaYeuThich>, IHangHoaYeuThichRepository
    {
        public HangHoaYeuThichRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}

