﻿
using Platform.Data.Infrastructure;
using Platform.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IHangHoa_CoSoRepository : IRepository<HangHoa_CoSo>
    {
        IEnumerable<LayKhuyenMai> GetKhuyenMai();
    }

  public  class HangHoa_CoSoRepository : RepositoryBase<HangHoa_CoSo>, IHangHoa_CoSoRepository
    {
        public HangHoa_CoSoRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        public IEnumerable<LayKhuyenMai> GetKhuyenMai()
        {
            var query = from b in DbContext.HangHoa_CoSos
                        join a in DbContext.hangHoas
                        on b.MaHang equals a.MaHang
                        join c in DbContext.donViTinhs
                        on a.MaDonViTinh equals c.MaDonViTinh
                        where b.MaCoSo == "000" && (b.GiaKhuyenMai != null || b.KhuyenMai2 != null) && b.GiaKhuyenMai != 0&&b.KhuyenMai2!=""
                        select new LayKhuyenMai
                        {
                            MaHang = a.MaHang,
                            TenHang = a.TenHang,
                            GiaBan = b.GiaBan,
                            GiaKhuyenMai = b.GiaKhuyenMai,
                           
                            TenDonViTinh = c.TenDonViTinh,
                            MaDonViTinh = c.MaDonViTinh,
                            HinhAnh = a.HinhAnh,
                            KhuyenMai2 = (from f in DbContext.HangHoa_CoSos
                                          join g in DbContext.hangHoas
                                          on f.KhuyenMai2 equals g.MaHang
                                          where f.KhuyenMai2 == b.KhuyenMai2
                                          select g.TenHang
                                                  ).FirstOrDefault(),
                            ChiTietKhuyenMai = (from f in DbContext.HangHoa_CoSos
                                                join g in DbContext.hangHoas
                                                on f.KhuyenMai2 equals g.MaHang
                                                join j in DbContext.donViTinhs
                                                on a.MaDonViTinh equals j.MaDonViTinh
                                                where f.KhuyenMai2 == b.KhuyenMai2
                                                select new HangHoaKhuyenMai()
                                                {
                                                    MaHang = b.KhuyenMai2,
                                                    TenHang = g.TenHang,
                                                    MaDonViTinh = j.MaDonViTinh,
                                                    TenDonViTinh = j.TenDonViTinh,
                                                    SoLuongKhuyenMai2 = b.SoLuongKhuyenMai2,
                                                    HinhAnh = g.HinhAnh

                                                }).FirstOrDefault(),




                        };
            return query;


        }

    }
}