﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
   
    public interface IHinhAnhRepository : IRepository<HinhAnhs>
    {
        IEnumerable<AlbumHinhAnh> gethinhanhcoso();
    }

    class HinhAnhRepository : RepositoryBase<HinhAnhs>, IHinhAnhRepository
    {
        public HinhAnhRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<AlbumHinhAnh> gethinhanhcoso()
        {
            var query = from a in DbContext.CoSo
                        join b in DbContext.hinhAnhs
                        on a.MaCoSo equals b.MaCoSo
                        
                        select new AlbumHinhAnh
                        {
                            MaCoSo = a.MaCoSo,
                            TenCoSo = a.TenCoSo,
                            HinhAnh = a.HinhAnh,
                            NhieuHinhAnh = b.NhieuHinhAnh
                        };
            return query;
        }
    }
}
