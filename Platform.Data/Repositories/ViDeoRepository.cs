﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
   
    public interface IVideoRepository : IRepository<Video>
    {

    }

    class ViDeoRepository : RepositoryBase<Video>, IVideoRepository
    {
        public ViDeoRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }


    }
}
