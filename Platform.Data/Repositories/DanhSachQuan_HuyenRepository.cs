﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IDanhSachQuan_HuyenRepository : IRepository<DanhSachQuan_Huyen>
    {
        IEnumerable<DanhSachQuan_Huyen> ChonTheoTinh(string matinh);
    }

    class DanhSachQuan_HuyenRepository : RepositoryBase<DanhSachQuan_Huyen>, IDanhSachQuan_HuyenRepository
    {
        public DanhSachQuan_HuyenRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<DanhSachQuan_Huyen> ChonTheoTinh(string matinh)
        {
            var query = from A in DbContext.danhSachQuan_Huyens
                        join B in DbContext.danhSachTinh_TPs
                        on A.MaTinh_TP equals B.MaTinh_TP
                        where A.MaTinh_TP.Equals(matinh)
                        select A;
            return query;
        }
    }
}
