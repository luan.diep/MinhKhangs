﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
    public interface IChungTuDatHangRepository : IRepository<ChungTuDatHang>
    {
        string LayTenHinhThucThanhToan(int id);
        string GetNewID();
        ChungTuDatHang getByMaCT(string mact);
    }

    class ChungTuDatHangRepository : RepositoryBase<ChungTuDatHang>, IChungTuDatHangRepository
    {
        public ChungTuDatHangRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public ChungTuDatHang getByMaCT(string mact)
        {
            var query = from A in DbContext.chungTuDatHangs
                        where A.MaChungTuDatHang.Equals(mact)
                        select A;
            return query.First();
        }

        public string GetNewID()
        {
            var query = (from CTDH in DbContext.chungTuDatHangs orderby CTDH.MaChungTuDatHang descending select CTDH);
            if (query.Count() > 0)
            {
                string laytatca = query.FirstOrDefault().MaChungTuDatHang.Split(new[] { "DH" }, StringSplitOptions.None)[1];
                string str = "" + (Convert.ToInt32(laytatca) + 1);
                string pad1 = "000000";
                string MaCuoiCung = pad1.Substring(0, pad1.Length - str.Length) + str;
                string mamoi = "DH" + MaCuoiCung;
                return mamoi;
            }
            return "DH000001";
        }

        public string LayTenHinhThucThanhToan(int id)
        {
            var query = from A in DbContext.hinhThucThanhToans
                        where A.MaHinhThucThanhToan.Equals(id)
                        select A.TenHinhThucThanhToan;
            return query.FirstOrDefault();
                      
        }
    }
}
