﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
    public interface IHangHoaRepository : IRepository<HangHoa>
    {
      
        string GetMaHangHoaCuoiCung();
        IEnumerable<HangHoaOnl> layhanghoaonl();
        IEnumerable<HangHoaOnl> layhanghoaonls(int id);
        IEnumerable<Homecaterogy> homecategory(int manhom);
        IEnumerable<Homecaterogy> Groupproduct(int manhom);
        HangHoaOnl layhanghoaonlById(string id);
        IEnumerable<HangHoa> GetByName(string id);
    
    }

    class HangHoaRepository : RepositoryBase<HangHoa>, IHangHoaRepository
    {
        public HangHoaRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
       
        public string GetMaHangHoaCuoiCung()
        {
            var MaHangHoaCuoiCung = (from a in DbContext.hangHoas orderby a.MaHang descending select a);
            if (MaHangHoaCuoiCung.Count()>0)
            {
                return MaHangHoaCuoiCung.FirstOrDefault().MaHang;
            }
            return null;
        }

       
        public IEnumerable<HangHoa> GetByName(string id)
        {
            var query = from a in DbContext.hangHoas
                        where a.TenHang.ToLower().Contains(id.ToLower())
                        select a;
            return query;
        }

        public IEnumerable<HangHoaOnl> layhanghoaonl()
        {
            var query = from b in DbContext.HangHoa_CoSos
                        join a in DbContext.hangHoas
                        on b.MaHang equals a.MaHang
                        join c in DbContext.donViTinhs
                        on a.MaDonViTinh equals c.MaDonViTinh
                        join d in DbContext.CoSo
                        on b.MaCoSo equals d.MaCoSo
                        join e in DbContext.nhomHangHoas
                        on a.MaNhomHH equals e.MaNhomHH
                      
                        where b.MaCoSo == "000" && a.GiaBan != 0 && a.GiaBan != null
                        select new HangHoaOnl
                        {
                            MaHang = a.MaHang,
                            TenHang = a.TenHang,
                            TenDonViTinh = c.TenDonViTinh,
                            MoTa = a.MoTa,
                            GiaNhap = b.GiaNhap,
                            GiaBan =b.GiaBan,
                            GiaKhuyenMai = b.GiaKhuyenMai,
                            SoLuongTonThuc = b.SoLuongTonThuc,
                            HinhAnh = a.HinhAnh,
                            NhieuHinhAnh = a.NhieuHinhAnh,
                            VAT = a.VAT,
                            ChietKhau = a.ChietKhau,
                            ThanhPham = a.ThanhPham,
                            MaCoSo=b.MaCoSo,
                            TenCoSo=d.TenCoSo,
                            MaNhomHH=a.MaNhomHH,
                            TenNhom=e.TenNhom,
                            NgungKinhDoanh=b.NgungKinhDoanh
                            

                        };
            return query;
        }

        public HangHoaOnl layhanghoaonlById(string id)
        {
            var query = from b in DbContext.HangHoa_CoSos
                        join a in DbContext.hangHoas
                        on b.MaHang equals a.MaHang
                        join c in DbContext.donViTinhs
                        on a.MaDonViTinh equals c.MaDonViTinh
                        join d in DbContext.CoSo
                        on b.MaCoSo equals d.MaCoSo
                        join e in DbContext.nhomHangHoas
                        on a.MaNhomHH equals e.MaNhomHH
                        where b.MaCoSo == "000" && a.MaHang == id 
                        select new HangHoaOnl
                        {
                            MaHang = a.MaHang,
                            TenHang = a.TenHang,
                            TenDonViTinh = c.TenDonViTinh,
                            MoTa = a.MoTa,
                            GiaNhap = b.GiaNhap,
                            GiaBan = b.GiaBan,
                            GiaKhuyenMai = b.GiaKhuyenMai,
                            SoLuongTonThuc = b.SoLuongTonThuc,
                            HinhAnh = a.HinhAnh,
                            NhieuHinhAnh = a.NhieuHinhAnh,
                            VAT = a.VAT,
                            ChietKhau = a.ChietKhau,
                            ThanhPham = a.ThanhPham,
                            MaCoSo = b.MaCoSo,
                            TenCoSo = d.TenCoSo,
                            MaNhomHH = a.MaNhomHH,
                            TenNhom = e.TenNhom,
                            NgungKinhDoanh = b.NgungKinhDoanh


                        };
            return query.FirstOrDefault();
        }

        public IEnumerable<Homecaterogy> homecategory(int manhom)
        {
            var query = from b in DbContext.HangHoa_CoSos
                        join a in DbContext.hangHoas
                        on b.MaHang equals a.MaHang
                        join e in DbContext.nhomHangHoas
                        on a.MaNhomHH equals e.MaNhomHH
                        where b.MaCoSo == "000" && a.MaNhomHH == (manhom)
                        select new Homecaterogy
                        {
                            MaHang = a.MaHang,
                            TenHang = a.TenHang,
                            MoTa = a.MoTa,
                            GiaBan = b.GiaBan,
                            HinhAnh = a.HinhAnh,
                            ThanhPham = a.ThanhPham,
                            MaNhomHH = a.MaNhomHH,
                            TenNhom = e.TenNhom,
                            SoLuongTonThuc = b.SoLuongTonThuc
                        };
            return query.Take(20);
        }

        public IEnumerable<Homecaterogy> Groupproduct(int manhom)
        {
            var query = from b in DbContext.HangHoa_CoSos
                        join a in DbContext.hangHoas
                        on b.MaHang equals a.MaHang
                        join e in DbContext.nhomHangHoas
                        on a.MaNhomHH equals e.MaNhomHH
                        where b.MaCoSo == "000" && a.MaNhomHH == (manhom)
                        select new Homecaterogy
                        {
                            MaHang = a.MaHang,
                            TenHang = a.TenHang,
                            MoTa = a.MoTa,
                            GiaBan = a.GiaBan,
                            HinhAnh = a.HinhAnh,
                            ThanhPham = a.ThanhPham,
                            MaNhomHH = a.MaNhomHH,
                            TenNhom = e.TenNhom,
                            SoLuongTonThuc = b.SoLuongTonThuc
                        };
            return query;
        }

        public IEnumerable<HangHoaOnl> layhanghoaonls(int id)
        {
            var query = from b in DbContext.HangHoa_CoSos
                        join a in DbContext.hangHoas
                        on b.MaHang equals a.MaHang
                        join c in DbContext.donViTinhs
                        on a.MaDonViTinh equals c.MaDonViTinh
                        join d in DbContext.CoSo
                        on b.MaCoSo equals d.MaCoSo
                        join e in DbContext.nhomHangHoas
                        on a.MaNhomHH equals e.MaNhomHH
                        where b.MaCoSo == "000" && a.GiaBan != 0 && a.GiaBan != null &&a.MaNhomHH==id
                        select new HangHoaOnl
                        {
                            MaHang = a.MaHang,
                            TenHang = a.TenHang,
                            TenDonViTinh = c.TenDonViTinh,
                            MoTa = a.MoTa,
                            GiaNhap = b.GiaNhap,
                            GiaBan = b.GiaBan,
                            GiaKhuyenMai = b.GiaKhuyenMai,
                            SoLuongTonThuc = b.SoLuongTonThuc,
                            HinhAnh = a.HinhAnh,
                            NhieuHinhAnh = a.NhieuHinhAnh,
                            VAT = a.VAT,
                            ChietKhau = a.ChietKhau,
                            ThanhPham = a.ThanhPham,
                            MaCoSo = b.MaCoSo,
                            TenCoSo = d.TenCoSo,
                            MaNhomHH = a.MaNhomHH,
                            TenNhom = e.TenNhom,
                            NgungKinhDoanh = b.NgungKinhDoanh


                        };
            return query.Take(8);
        }
    }
}
