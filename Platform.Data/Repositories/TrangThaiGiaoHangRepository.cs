﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
    public interface ITrangThaiGiaoHangRepository : IRepository<TrangThaiGiaoHang>
    {
        IEnumerable<TrangThaiGiaoHang> LayTheoMaDon(string madon);
        IEnumerable<TimeLine> LayTenTrangThai(string maCT);
    }

    class TrangThaiGiaoHangRepository : RepositoryBase<TrangThaiGiaoHang>, ITrangThaiGiaoHangRepository
    {
        public TrangThaiGiaoHangRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<TimeLine> LayTenTrangThai(string maCT)
        {
            var query = from a in DbContext.trangThaiGiaoHangs
                        join b in DbContext.danhSachTrangThaiGiaoHangs
                        on a.MaTrangThai equals b.MaTrangThai
                        where a.MaChungTuDatHang.Equals(maCT)
                        select new TimeLine
                        {
                            MaTrangThai = a.MaTrangThai,
                            TenTrangThai = b.TenTrangThai,
                            NgayGio=a.NgayGio


                        };
            return query;
        }

        public IEnumerable<TrangThaiGiaoHang> LayTheoMaDon(string madon)
        {
            var query = (from a in DbContext.trangThaiGiaoHangs
                         
                         where a.MaChungTuDatHang.Equals(madon)
                         select a).ToList();
            if (query.Any())
            {
                foreach (var item in query)
                {
                    item.DanhSachTrangThaiGiaoHang = (from a in DbContext.danhSachTrangThaiGiaoHangs
                                                      where a.MaTrangThai.Equals(item.MaTrangThai)
                                                      select a).ToList().FirstOrDefault();
                }
            }
            
            return query;
        }
    }
}
