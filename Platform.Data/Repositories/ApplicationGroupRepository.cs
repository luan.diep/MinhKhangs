﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platform.Data.Infrastructure;
using Platform.Model.Models;

namespace Platform.Data.Repositories
{
    public interface IApplicationGroupRepository : IRepository<ApplicationGroup_KH>
    {
        IEnumerable<ApplicationGroup_KH> GetListGroupByUserId(string userId);
        IEnumerable<ApplicationUser_KH> GetListUserByGroupId(int groupId);
    }
    public class ApplicationGroupRepository : RepositoryBase<ApplicationGroup_KH>, IApplicationGroupRepository
    {
        public ApplicationGroupRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IEnumerable<ApplicationGroup_KH> GetListGroupByUserId(string userId)
        {
            var query = from g in DbContext.ApplicationGroup_KH
                        join ug in DbContext.ApplicationUserGroup_KH
                        on g.ID equals ug.GroupId
                        where ug.UserId == userId
                        select g;
            return query;
        }

        public IEnumerable<ApplicationUser_KH> GetListUserByGroupId(int groupId)
        {
            var query = from g in DbContext.ApplicationGroup_KH
                        join ug in DbContext.ApplicationUserGroup_KH
                        on g.ID equals ug.GroupId
                        join u in DbContext.Users
                        on ug.UserId equals u.Id
                        where ug.GroupId == groupId
                        select u;
            return query;
        }
    }
}
