﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platform.Data.Infrastructure;
using Platform.Model.Models;

namespace Platform.Data.Repositories
{
    public interface IApplicationRoleRepository : IRepository<ApplicationRole_KH>
    {
        IEnumerable<ApplicationRole_KH> GetListRoleByGroupId(int groupId);
        IEnumerable<ApplicationRole_KH> GetListRoleByUserId(string userId);
    }
    public class ApplicationRoleRepository : RepositoryBase<ApplicationRole_KH>, IApplicationRoleRepository
    {
        public ApplicationRoleRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
        public IEnumerable<ApplicationRole_KH> GetListRoleByGroupId(int groupId)
        {
            var query = from g in DbContext.ApplicationRole_KH
                        join ug in DbContext.ApplicationRoleGroup_KH
                        on g.Id equals ug.RoleId
                        where ug.GroupId == groupId
                        select g;
            return query;
        }

        public IEnumerable<ApplicationRole_KH> GetListRoleByUserId(string userId)
        {
            var query = from a in DbContext.ApplicationUserGroup_KH
                        join  u in DbContext.ApplicationGroup_KH
                        on a.GroupId equals u.ID
                        join ug in DbContext.ApplicationRoleGroup_KH
                        on u.ID equals ug.GroupId
                        join r in DbContext.ApplicationRole_KH
                        on ug.RoleId equals r.Id
                        where a.UserId == userId
                        select r;
            return query;
        }
    }
}
