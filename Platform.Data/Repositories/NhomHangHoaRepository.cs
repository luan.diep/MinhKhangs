﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
    public interface INhomHangHoaRepository : IRepository<NhomHangHoa>
    {
        IEnumerable<Catelogy_product> getallCatelogy();
        NhomHangHoaGroups getbyid(int id);
      
    }

    class NhomHangHoaRepository : RepositoryBase<NhomHangHoa>, INhomHangHoaRepository
    {
        public NhomHangHoaRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<Catelogy_product> getallCatelogy()
        {
            var query = from A in DbContext.nhomHangHoas
                        select new Catelogy_product()
                        {
                            TenNhom = A.TenNhom,
                            MaNhomHH = A.MaNhomHH,
                            listproduct = from B in DbContext.hangHoas
                                          where B.MaNhomHH.Equals(A.MaNhomHH)
                                          select B

                        };
            return query;

        }

        public NhomHangHoaGroups getbyid(int id)
        {
            var query = from A in DbContext.nhomHangHoas
                        where A.MaNhomHH.Equals(id)
                        select new NhomHangHoaGroups
                        {
                            MaNhomHH = A.MaNhomHH,
                            TenNhom = A.TenNhom,
                            GhiChu = A.GhiChu
                        };
            return query.FirstOrDefault();
        }
    }
}
