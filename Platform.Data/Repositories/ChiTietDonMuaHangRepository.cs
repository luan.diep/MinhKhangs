﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
    public interface IChiTietDonMuaHangRepository : IRepository<ChiTietDonMuaHang>
    {
        IQueryable<getchitietmuahang> getchitiet(string maMH);
     

    }

    class ChiTietDonMuaHangRepository : RepositoryBase<ChiTietDonMuaHang>, IChiTietDonMuaHangRepository
    {
        public ChiTietDonMuaHangRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IQueryable<getchitietmuahang> getchitiet(string maMH)
        {
            var query = from A in DbContext.chiTietDonMuaHangs
                        join B in DbContext.donMuaHangs
                        on A.MaDonMuaHang equals B.MaDonMuaHang
                        join C in DbContext.hangHoas
                        on A.MaHang equals C.MaHang
                        join D in DbContext.donViTinhs
                        on C.MaDonViTinh equals D.MaDonViTinh
                        where A.MaDonMuaHang.Equals(maMH)
                        select new getchitietmuahang {
                         MaDonMuaHang =A.MaDonMuaHang,
                         MaHang =A.MaHang,
                         SoLuong =A.SoLuong,
                        SoLuongNhan =A.SoLuongNhan,
                        DonGia= A.DonGia,
                        ThanhTien =A.ThanhTien,
                        ThueGTGT= A.ThueGTGT,
                        TienThueGTGT =A.TienThueGTGT,
                        LenhSanXuat= A.LenhSanXuat,
                         ThanhPham =A.ThanhPham,
                         TenHang=C.TenHang,
                         GiaKhuyenMai=C.GiaKhuyenMai,
                         MaDonViTinh = C.MaDonViTinh,
                         TenDonViTinh=D.TenDonViTinh,
                         MaCTDMH=A.MaCTDMH,
                         TienChietKhau=B.TienChietKhau,
                         GiaNhap=C.GiaNhap,
                         ChietKhau=C.ChietKhau,
                         VAT=C.VAT,
                         
                        };
            return query;

        }
       
    }
}
