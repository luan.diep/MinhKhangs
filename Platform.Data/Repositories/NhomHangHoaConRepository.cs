﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
    public interface INhomHangHoaConRepository : IRepository<NhomHangHoa_Con>
    {
        NhomHangHoa_Con gettolist(int id);
    }

    class NhomHangHoaConRepository : RepositoryBase<NhomHangHoa_Con>, INhomHangHoaConRepository
    {
        public NhomHangHoaConRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public NhomHangHoa_Con gettolist(int id)
        {
            var query = from A in DbContext.nhomHangHoa_Cons
                        where A.MaNhomHHGoc.Equals(id)
                        select A;

            return query.FirstOrDefault();
        }
    }
}
