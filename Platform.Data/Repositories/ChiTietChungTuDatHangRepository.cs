﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
    public interface IChiTietChungTuDatHangRepository : IRepository<ChiTietChungTuDatHang>
    {
        IEnumerable<ListChiTietChungTuDatHang> getchitietchungtudathang(string Id);
        IEnumerable<ListChiTietChungTuDatHang> getchitietchungtudathangbyemail(string email);
        IEnumerable<Cart_product> Listcart(string email);
        IEnumerable<Cart_product> get_history_product(string mact);

    }

    class ChiTietChungTuDatHangRepository : RepositoryBase<ChiTietChungTuDatHang>, IChiTietChungTuDatHangRepository
    {
        public ChiTietChungTuDatHangRepository(IDbFactory dbFactory) : base(dbFactory)
        {



        }

        public IEnumerable<ListChiTietChungTuDatHang> getchitietchungtudathang(string Id)
        {
            var query = from A in DbContext.chiTietChungTuDatHangs
                        join C in DbContext.hangHoas
                        on A.MaHang equals C.MaHang
                        join D in DbContext.donViTinhs
                        on C.MaDonViTinh equals D.MaDonViTinh
                        where A.MaChungTuDatHang.Equals(Id)
                        select new ListChiTietChungTuDatHang
                        {
                            MaChiTietChungTuDatHang = A.MaChiTietChungTuDatHang,
                            MaChungTuDatHang = A.MaChungTuDatHang,
                            MaHang = C.MaHang,
                            HinhAnh = C.HinhAnh,
                            TenHang = C.TenHang,
                            SoLuong = A.SoLuong,
                            DonGia = A.DonGia,
                            KhuyenMai = A.KhuyenMai,
                            ThanhTien = A.ThanhTien,

                            MaDonViTinh = A.MaDonViTinh,
                            TenDonViTinh = D.TenDonViTinh,



                        };

            return query;

        }

        public IEnumerable<ListChiTietChungTuDatHang> getchitietchungtudathangbyemail(string email)
        {
            var query = from B in DbContext.chungTuDatHangs
                        join E in DbContext.khachHangs
                        on B.MaKhachHang equals E.MaKhachHang
                        join A in DbContext.chiTietChungTuDatHangs
                        on B.MaChungTuDatHang equals A.MaChungTuDatHang
                        join C in DbContext.hangHoas
                        on A.MaHang equals C.MaHang
                        join D in DbContext.donViTinhs
                        on C.MaDonViTinh equals D.MaDonViTinh
                        where E.Email == email && B.MaTinhTrang == 1
                        select new ListChiTietChungTuDatHang
                        {
                            MaChiTietChungTuDatHang = A.MaChiTietChungTuDatHang,
                            MaChungTuDatHang = A.MaChungTuDatHang,
                            MaHang = C.MaHang,
                            HinhAnh = C.HinhAnh,
                            TenHang = C.TenHang,
                            SoLuong = A.SoLuong,
                            DonGia = A.DonGia,
                            KhuyenMai = A.KhuyenMai,
                            GiaKhuyenMai=A.GiaKhuyenMai,
                            KhuyenMai2=A.KhuyenMai2,
                            ThanhTien = A.ThanhTien,
                            TenDonViTinh = D.TenDonViTinh,
                            KhuyenMaiVatPham = C.KhuyenMai2,
                            TienChietKhau=A.TienChietKhau,
                            TienThueGTGT=A.TienThueGTGT,
                        };
            return query;
        }

        public IEnumerable<Cart_product> get_history_product(string mact)
        {
            var query = from E in DbContext.donViTinhs
                        join A in DbContext.hangHoas
                        on E.MaDonViTinh equals A.MaDonViTinh
                        join B in DbContext.chiTietChungTuDatHangs
                        on A.MaHang equals B.MaHang
                        join C in DbContext.chungTuDatHangs
                        on B.MaChungTuDatHang equals C.MaChungTuDatHang
                        join D in DbContext.khachHangs
                        on C.MaKhachHang equals D.MaKhachHang
                       
                        where B.MaChungTuDatHang.Equals(mact)
                        select new Cart_product()
                        {
                            MaHang = A.MaHang,
                            TenHang = A.TenHang,
                            TenDonViTinh = E.TenDonViTinh,
                            MoTa = A.MoTa,
                            DonGia = A.GiaBan,
                            ThanhTien = B.ThanhTien,
                            SoLuong = B.SoLuong,
                            GiaKhuyenMai = B.GiaKhuyenMai,
                            KhuyenMai2 = B.KhuyenMai2,
                            TienChietKhau = B.TienChietKhau,
                            HinhAnh = A.HinhAnh,
                            MaChungTuDatHang = C.MaChungTuDatHang


                        };
            return query;
        }

        public IEnumerable<Cart_product> Listcart(string email)
        {
            var query = from E in DbContext.donViTinhs
                        join A in DbContext.hangHoas
                        on E.MaDonViTinh equals A.MaDonViTinh
                        join B in DbContext.chiTietChungTuDatHangs
                        on A.MaHang equals B.MaHang
                        join C in DbContext.chungTuDatHangs
                        on B.MaChungTuDatHang equals C.MaChungTuDatHang
                        join D in DbContext.khachHangs
                        on C.MaKhachHang equals D.MaKhachHang
                        join G in DbContext.trangThaiGiaoHangs
                        on C.MaChungTuDatHang equals G.MaChungTuDatHang
                        where D.Email.Equals(email) && C.MaTinhTrang == 1
                        select new Cart_product()
                        {
                            MaHang = A.MaHang,
                            TenHang = A.TenHang,
                            TenDonViTinh = E.TenDonViTinh,
                            MoTa = A.MoTa,
                            DonGia = A.GiaBan,
                            ThanhTien = B.ThanhTien,
                            SoLuong = B.SoLuong,
                            GiaKhuyenMai = B.GiaKhuyenMai,
                            KhuyenMai2 = B.KhuyenMai2,
                            TienChietKhau = B.TienChietKhau,
                            HinhAnh = A.HinhAnh,
                            MaChungTuDatHang=C.MaChungTuDatHang


                        };
            return query;
                       
                        
            
        }
    }
}
