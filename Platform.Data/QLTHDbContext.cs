﻿using Microsoft.AspNet.Identity.EntityFramework;
using Platform.Model;
using Platform.Model.Models;
using System.Data.Entity;


namespace Platform.Data
{
    public class QLTHDbContext : IdentityDbContext<ApplicationUser_KH>
    {
       public QLTHDbContext() : base("PlatformTH_KH")
        {
            this.Configuration.LazyLoadingEnabled = true;
        }
        public DbSet<ApplicationGroup_KH> ApplicationGroup_KH { set; get; }
        public DbSet<ApplicationRole_KH> ApplicationRole_KH { set; get; }
        public DbSet<ApplicationRoleGroup_KH> ApplicationRoleGroup_KH { set; get; }
        public DbSet<ApplicationUserGroup_KH> ApplicationUserGroup_KH { set; get; }
        public DbSet<ThongTinCaNhan> ThongTinCaNhan { set; get; }

      
        public DbSet<LichSuLamViec> lichSuLamViecs { set; get; }
   
        public DbSet<CoSo> CoSo { set; get; }
     
        public DbSet<ChucVu> chucVus { set; get; }
      
        public DbSet<KhachHang> khachHangs { set; get; }
     
        public DbSet<TinhTrang> tinhTrangs { set; get; }
      
        public DbSet<NhomHangHoa>  nhomHangHoas { set; get; }
      
        public DbSet<HangHoa>   hangHoas { set; get; }
     
        public DbSet<KhachHang_TaiKhoan> khachHang_TaiKhoans { set; get; }
     
        public DbSet<ChiTietDonMuaHang> chiTietDonMuaHangs { set; get; }
        public DbSet<DonMuaHang> donMuaHangs { set; get; }
     
        public DbSet<ChiTietChungTuMuaHang> chiTietChungTuMuaHangs { set; get; }
      
        public DbSet<DonDatHang> donDatHangs { set; get; }
        public DbSet<ChiTietDonDatHang> chiTietDonDatHangs { set; get; }
      
        public DbSet<HoaDon_BanHang> hoaDon_BanHangs { set; get; }
        public DbSet<ChiTietHoaDon_BanHang> chiTietHoaDon_BanHangs { set; get; }
      
        public DbSet<ThanhPham>  thanhPhams { set; get; }
     
        public DbSet<HeThongTaiKhoan>  heThongTaiKhoans { set; get; }
   
        public DbSet<DonViTinh>  donViTinhs { set; get; }
      
        public DbSet<DoiTacGiaoHang> DoiTacGiaoHangs { set; get; }
        public DbSet<ChungTuGiaoHang> chungTuGiaoHangs { set; get; }
        public DbSet<TrangThaiGiaoHang> trangThaiGiaoHangs { set; get; }
        public DbSet<DanhSachTrangThaiGiaoHang> danhSachTrangThaiGiaoHangs { set; get; }
    
        public DbSet<DanhSachTrangThaiDonMuaHang> danhSachTrangThaiDonMuaHangs { set; get; }
      
        public DbSet<TrangThaiDonMuaHang> trangThaiDonMuaHangs { set; get; }
   
        public DbSet<HinhThucThanhToan> hinhThucThanhToans { set; get; }
      
        public DbSet<LichSuDatHang> lichSuDatHangs { set; get; }
        public DbSet<ChungTuDatHang> chungTuDatHangs { set; get; }
        public DbSet<ChiTietChungTuDatHang> chiTietChungTuDatHangs { set; get; }
        public DbSet<GioiThieuOnl> gioiThieuOnls { set; get; }
        public DbSet<PhanHoi> phanHois { set; get; }
        public DbSet<KhachHang_KhongTaiKhoan> khachHang_KhongTaiKhoans { set; get; }
        public DbSet<DanhSachTinh_TP> danhSachTinh_TPs { set; get; }
        public DbSet<DanhSachQuan_Huyen> danhSachQuan_Huyens { set; get; }
        
        public DbSet<HangHoa_CoSo> HangHoa_CoSos { set; get; }
        public DbSet<TuyenDung> TuyenDungs { set; get; }
        public DbSet<HinhAnhs> hinhAnhs { set; get; }
        public DbSet<Video> videos { set; get; }
        public DbSet<HinhAnh_CoSo> hinhAnh_CoSos { set; get; }
        public DbSet<HangHoaYeuThich> hangHoaYeuThiches { set; get; }
        public DbSet<NhomHangHoa_Con> nhomHangHoa_Cons { set; get; }
     

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Entity<IdentityUserRole>().HasKey(i => new { i.UserId, i.RoleId }).ToTable("ApplicationUserRole_KH");
            builder.Entity<IdentityUserLogin>().HasKey(i => i.UserId).ToTable("ApplicationUserLogin_KH");
            builder.Entity<IdentityRole>().ToTable("ApplicationRole_KH");
            builder.Entity<IdentityUserClaim>().HasKey(i => i.UserId).ToTable("ApplicationUserClaim_KH");
        }
        public static QLTHDbContext Create()
        {
            return new QLTHDbContext();
        }
    }
}
