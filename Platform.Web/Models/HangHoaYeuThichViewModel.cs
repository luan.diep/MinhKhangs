﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class HangHoaYeuThichViewModel
    {
        public int ID { get; set; }
        public string MaKhachHang { get; set; }
        public string MaHang { get; set; }
        public Nullable<System.DateTime> NgayGio { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string GhiChu { get; set; }
    }
}