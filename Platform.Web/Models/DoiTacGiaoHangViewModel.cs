﻿using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public  class DoiTacGiaoHangViewModel
    {
        public string MaNguoiGiaoHang { get; set; }
        public string MaNhaCungCap { get; set; }
      
        public string PhanLoai { get; set; }
        public string XungHo { get; set; }
        public string TenNguoiGiaoHang { get; set; }
        public string ChiNhanh { get; set; }
        public string DiaChi { get; set; }
        public string SoDTDD_1 { get; set; }
        public string SoDTDD_2 { get; set; }
        public string DienThoaiCoDinh { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> NgaySinh { get; set; }
        public string MaSoThue { get; set; }
        public string SoTaiKhoanNganHang { get; set; }
        public string TenNganHang { get; set; }
        public Nullable<int> TongDonDaGiao { get; set; }
        public Nullable<double> TongTienDaGiao { get; set; }
        public Nullable<double> TongTrongLuongDaGiao { get; set; }
        public string CMND { get; set; }
        public Nullable<System.DateTime> NgayCap { get; set; }
        public string NoiCap { get; set; }
        public Nullable<bool> DangHoatDong { get; set; }
        public virtual ICollection<ChungTuGiaoHang> ChungTuGiaoHangs { get; set; }
      
        public Nullable<DateTime> NgayTao { get; set; }
        public string MaNguoiTao { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string MaNguoiXoa { get; set; }
        public DateTime? NgayXoa { get; set; }
        public string NguoiTao { get; set; }
        public string TenNhaCungCap { get; set; }
    }
}