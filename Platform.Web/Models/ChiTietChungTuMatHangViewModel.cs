﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ChiTietChungTuMatHangViewModel
    {
        public int MaChiTietChungTuMatHang { get; set; }
        public string MaChungTuMatHang { get; set; }
        public string MaHang { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<double> DonGia { get; set; }
        public Nullable<double> TongTienMat { get; set; }
        public Nullable<int> MaDonViTinh { get; set; }
    }
}