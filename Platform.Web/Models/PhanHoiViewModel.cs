﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class PhanHoiViewModel
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string SoDienThoai { get; set; }
        public string DiaChi { get; set; }
        public string Tinh_TP { get; set; }
        public string GhiChu { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }
        public Nullable<bool> TrangThai { get; set; }
    }
}