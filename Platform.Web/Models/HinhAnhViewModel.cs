﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class HinhAnhViewModel
    {
        public int MaHinhAnh { get; set; }
        public string MaCoSo { get; set; }
        public string HinhAnh { get; set; }
        public string NhieuHinhAnh { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }
    }
}