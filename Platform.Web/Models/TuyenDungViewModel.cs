﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class TuyenDungViewModel
    {
        public int MaTuyenDung { get; set; }
        public string MaCoSoTuyenDung { get; set; }
        public string DiaChi { get; set; }
        public string SoDienThoai { get; set; }
        public string TieuDe { get; set; }
        public string Email { get; set; }
        public string NoiDung { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }

    }
}