﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ChiTietChungTuDatHangViewModel
    {
        public int MaChiTietChungTuDatHang { get; set; }
        public string MaChungTuDatHang { get; set; }
        public string MaHang { get; set; }
        public string TKCongNo_ChiPhi { get; set; }
        public string TKDoanhThu { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<double> DonGia { get; set; }
        public string KhuyenMai { get; set; }
        public Nullable<double> ThanhTien { get; set; }
        public Nullable<double> TienChietKhau { get; set; }
        public Nullable<double> TienThueGTGT { get; set; }
        public Nullable<int> MaDonViTinh { get; set; }
        public Nullable<double> GiaKhuyenMai { get; set; }
        public string KhuyenMai2 { get; set; }
        public Nullable<double> SoLuongKhuyenMai2 { get; set; }
    }
}