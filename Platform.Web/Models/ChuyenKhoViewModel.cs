﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ChuyenKhoViewModel
    {
        public string MaChuyenKho { get; set; }
        public string TuChiNhanh { get; set; }
        public string ToiChiNhanh { get; set; }
        public string NguoiTao { get; set; }
        public string NguoiNhan { get; set; }
        public Nullable<System.DateTime> NgayChuyen { get; set; }
        public Nullable<System.DateTime> NgayNhan { get; set; }
        public Nullable<System.DateTime> ThoiGianTao { get; set; }
        public string TongSLChuyen { get; set; }
        public Nullable<double> GiaTriChuyen { get; set; }
        public string TongSLNhan { get; set; }
        public Nullable<double> GiaTriNhan { get; set; }
        public Nullable<int> TongSoMatHang { get; set; }
        public string GhiChu { get; set; }
        public Nullable<int> MaTinhTrang { get; set; }
    }
}