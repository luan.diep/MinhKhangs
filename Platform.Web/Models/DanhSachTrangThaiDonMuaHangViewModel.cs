﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class DanhSachTrangThaiDonMuaHangViewModel
    {
        public int MaTrangThai { get; set; }
        public string TenTrangThai { get; set; }
        public string GhiChu { get; set; }
    }
}