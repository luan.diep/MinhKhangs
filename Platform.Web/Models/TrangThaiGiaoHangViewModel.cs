﻿using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public  class TrangThaiGiaoHangViewModel
    {
        public int MaChiTietTrangThai { get; set; }
        public string MaChungTuDatHang { get; set; }
        public int MaTrangThai { get; set; }
        public string DaLayHang { get; set; }
        public Nullable<System.DateTime> NgayGio { get; set; }
        public string LyDo { get; set; }
        public string MaNhanVienTiepNhan { get; set; }
        public string TenNhanVien { get; set; }
        public bool? ChoHienThi { get; set; }
        public DanhSachTrangThaiGiaoHang DanhSachTrangThaiGiaoHang { get; set; }
    }
}