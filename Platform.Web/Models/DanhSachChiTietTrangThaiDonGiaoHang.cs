﻿using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class DanhSachChiTietTrangThaiDonGiaoHang
    {
        public string MaChungTuDatHang { get; set; }
        public string MaKhachHang { get; set; }
        public Nullable<System.DateTime> NgayChungTu { get; set; }
        public Nullable<double> TongTienHang { get; set; }
        public Nullable<double> TienThueGTGT { get; set; }
        public Nullable<double> TienChietKhau { get; set; }
        public Nullable<double> TongTienThanhToan { get; set; }
        public Nullable<bool> DaThanhToan { get; set; }
        public int MaHinhThucThanhToan { get; set; }
        public string TenHinhThucThanhToan { get; set; }
        public int MaTinhTrang { get; set; }
        public Nullable<DateTime> ThoiGianMuonNhanHang { get; set; }
        public IEnumerable<Cart_product> ChiTiet { get; set; }
        public IEnumerable<TimeLine> TrangThaiGiaoHang { get; set; }
    }
}