﻿using System.Web;
using System.Web.Optimization;

namespace Platform.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Index/js").Include(
                "~/Assets/apphome/js/jquery.min.js",
                "~/node_modules/lazysizes/lazysizes.js",
                "~/Assets/apphome/js/slick.min.js",
                "~/Assets/apphome/js/select2.min.js",
                "~/Assets/apphome/jslocal/angular-1.8.0/angular.js",
                "~/Assets/apphome/jslocal/angular-loading-bar-master/src/loading-bar.js",
                "~/Assets/apphome/jslocal/angular-1.8.0/angular-animate.js",
                "~/Assets/apphome/js/angular-filter.js",
                "~/Assets/apphome/js/bootstrap.min.js",
                "~/Assets/apphome/js/angular-recaptcha-master/angular-recaptcha-master/release/angular-recaptcha.js",
                "~/Assets/apphome/jslocal/angular-ui-router.js",
                "~/Assets/apphome/lib/bootbox/bootbox.js",
                "~/Assets/apphome/lib/ngBootbox/ngBootbox.js",
                "~/Assets/apphome/lib/toastr/toastr.js ",
                "~/apphome/shared/modules/platformTH.common.js",
                "~/apphome/shared/services/apiService.js",
                "~/apphome/shared/services/authData.js",
                "~/apphome/shared/services/authenticationService.js",
                "~/apphome/shared/services/loginService.js",
                "~/apphome/shared/app.js",
                "~/Assets/apphome/lib/angular-local-storage/dist/angular-local-storage.js",
                "~/Assets/apphome/lib/angular-cookies/angular-cookies.min.js",
                "~/Assets/apphome/lib/angular-ui-router/release/stateEvents.js",
                "~/apphome/shared/services/notificationService.js",
                "~/apphome/shared/directives/pagerDirective.js",
                "~/apphome/shared/filters/totalFilter.js",
                "~/Assets/apphome/js/angular-locale_vi-vn.js",
                "~/apphome/conponents/trangchu/rootController.js",
                "~/apphome/conponents/trangchu/topSearchController.js",
                "~/apphome/conponents/trangchu/trangChu.module.js",
                "~/apphome/conponents/trangchu/trangChuController.js",
                "~/apphome/conponents/aboutus/Aboutus.module.js",
                "~/apphome/conponents/aboutus/AboutusController.js",
                "~/apphome/conponents/cart/Cart.module.js",
                "~/apphome/conponents/cart/CartController.js",
                "~/apphome/conponents/service/Service.module.js",
                "~/apphome/conponents/service/ServiceController.js",
                "~/apphome/conponents/chitietsanpham/chiTietSanPham.module.js",
                "~/apphome/conponents/chitietsanpham/chiTietSanPhamController.js",
                "~/apphome/conponents/community/Community.module.js",
                "~/apphome/conponents/community/WelcomeController.js",
                "~/apphome/conponents/community/BlogController.js",
                "~/apphome/conponents/fssupport/Fssupport.module.js",
                "~/apphome/conponents/fssupport/FssupportController.js",
                "~/apphome/conponents/product/Product.module.js",
                "~/apphome/conponents/product/ProductController.js",
                "~/apphome/conponents/paymentmethods/Paymentmethods.module.js",
                "~/apphome/conponents/paymentmethods/PaymentmethodsController.js",
                "~/apphome/conponents/returnpolicy/Returnpolicy.module.js",
                "~/apphome/conponents/returnpolicy/ReturnpolicyController.js",
                "~/apphome/conponents/recruitment/Recruitment.module.js",
                "~/apphome/conponents/recruitment/RecruitmentController.js",
                "~/apphome/conponents/support/supPort.module.js",
                "~/apphome/conponents/support/supPortController.js",
                "~/apphome/conponents/resetpassword/resetpassword.module.js",
                "~/apphome/conponents/resetpassword/resetpasswordController.js",
                "~/apphome/conponents/contact/Contact.module.js",
                "~/apphome/conponents/contact/ContactController.js",
                "~/apphome/conponents/catelogy/Catelogy.module.js",
                "~/apphome/conponents/catelogy/CatelogyController.js",
                "~/apphome/conponents/historyoderproduct/Historyoder_product.module.js",
                "~/apphome/conponents/historyoderproduct/Historyoder_productController.js",
                "~/apphome/conponents/historyoderproduct/History_detailController.js",
                "~/apphome/conponents/coomingsoon/coomingsoon.module.js",
                "~/apphome/conponents/coomingsoon/coomingsoonController.js",
                "~/apphome/conponents/loginadmin/LoginAdmin.module.js",
                "~/apphome/conponents/loginadmin/LoginAdminController.js",
                "~/apphome/shared/services/commonService.js",
                "~/Assets/apphome/js/moment.js",
                "~/node_modules/sweetalert2/dist/sweetalert2.js",
                "~/Assets/apphome/js/swiper-bundle.min.js",
                 "~/Assets/apphome/js/FileSaver.js" ,
                 "~/Assets/apphome/js/html2canvas.min.js" 


                ));
        }
    }
}
