﻿
using AutoMapper;
using Newtonsoft.Json;
using Platform.Common;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Platform.Web.Api
{
   


    [RoutePrefix("api/hinhanh")]
   // [Authorize]
    public class HinhAnhController : ApiControllerBase
    {
        IHinhAnhsService _hinhAnhService;
        ICoSoService _coSoService;
        public HinhAnhController(ILoiService loiService, ICoSoService coSoService, IHinhAnhsService hinhAnhService) : base(loiService)
        {
            this._hinhAnhService = hinhAnhService;
            this._coSoService = coSoService;
        }


        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _hinhAnhService.GetAll();
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }
        [Route("layhinhanhtheocoso")]
        [HttpGet]
        public HttpResponseMessage layhinhanhtheocoso(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _hinhAnhService.gethinhanhcoso().Where(x=>x.MaCoSo!="000");
                
               

               
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }





        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, HinhAnhViewModel tuyenDungViewModel)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newThongBao = new HinhAnhs();
                    newThongBao.UpdateHinhAnh(tuyenDungViewModel);
                    _hinhAnhService.Add(newThongBao);
                    _hinhAnhService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });

        }





    }
}