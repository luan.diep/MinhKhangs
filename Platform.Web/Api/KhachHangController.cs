﻿using AutoMapper;
using Newtonsoft.Json.Linq;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;
using System.Web;
using System.Threading.Tasks;
using System.IO;
using Platform.Common;
using Microsoft.Build.Tasks;

namespace Platform.Web.Api
{
    [RoutePrefix("api/khachhang")]
    //[Authorize]
    public class KhachHangController : ApiControllerBase
    {
        IKhachHangService _khachHangService;
        //INhanVienService _nhanVienService;
        public KhachHangController(ILoiService loiService, IKhachHangService khachHangService/* INhanVienService nhanvienService*/ ) : base(loiService)
        {
            this._khachHangService = khachHangService;
            //this._nhanVienService = nhanvienService;
        }


        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<KhachHangViewModel> KhachHangVMs)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in KhachHangVMs)
                    {
                        string laytatca = _khachHangService.GetLastID().Split(new[] { "KH" }, StringSplitOptions.None)[1];
                        string str = "" + (Convert.ToInt32(laytatca) + 1);
                        string pad1 = "000000";

                        string MaCuoiCung = pad1.Substring(0, pad1.Length - str.Length) + str;
                        string mamoi = "KH" + MaCuoiCung;

                        var newThongBao = new KhachHang();
                        newThongBao.UpdateKhachHang(item);
                        newThongBao.MaKhachHang = mamoi;
                        _khachHangService.Add(newThongBao);
                        _khachHangService.Save();
                    }
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }


        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, KhachHangViewModel khachHang)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    string laytatca = _khachHangService.GetLastID().Split(new[] { "KH" }, StringSplitOptions.None)[1];
                    string str = "" + (Convert.ToInt32(laytatca) + 1);
                    string pad1 = "000000";

                    string MaCuoiCung = pad1.Substring(0, pad1.Length - str.Length) + str;
                    string mamoi = "KH" + MaCuoiCung;
                    KhachHang newKhachHang = new KhachHang();
                    khachHang.MaKhachHang = mamoi;
                    khachHang.NguoiTao = User.Identity.Name;
                    khachHang.NgayTao = DateTime.Now;
                    khachHang.DangHoatDong = true;
                    khachHang.SoNgayDuocNo = 0;
                    khachHang.SoNoToiDa = 0;
                    khachHang.SoLuongGiaoDich = 0;
                    khachHang.TongGiaoDich = 0;
                    khachHang.GiaTriGiaoDichLonNhat = 0;
                    khachHang.DaXoa = false; // mới tạo thì chưa được xóa

                    newKhachHang.UpdateKhachHang(khachHang);
                    _khachHangService.Add(newKhachHang);
                    _khachHangService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });

        }

        [Route("delete")]
        [HttpDelete]
        [HttpGet]
        // [AllowAnonymous]
        public HttpResponseMessage Delete(HttpRequestMessage request, string ID)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldProductCategory = _khachHangService.DELETE(int.Parse(ID));
                    _khachHangService.Save();

                    var responseData = Mapper.Map<KhachHang, KhachHangViewModel>(oldProductCategory);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage getallkhachHang(HttpRequestMessage request, int page, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;

                var ListKhachHang = _khachHangService.GetAll();
                totalRow = ListKhachHang.Count();
                var query = ListKhachHang.OrderBy(x => x.MaKhachHang).Skip(page * pageSize).Take(pageSize);
                var responseData = Mapper.Map<IEnumerable<KhachHang>, IEnumerable<KhachHang>>(query);
                var PaginationSet = new PaginationSet<KhachHang>()
                {
                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,/*tong so ban ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/
                };

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);

                return response;
            });
        }


        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _khachHangService.GetAll();
                var b = listCategory.OrderBy(x => x.MaKhachHang);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, b);


                return response;
            });
        }

         [Route("getmaKH")]
        public HttpResponseMessage getmaKH(HttpRequestMessage request,string email)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _khachHangService.getmaKH(email);
              
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }






        [Route("getbyid")]
        public HttpResponseMessage GetById(HttpRequestMessage request, string id)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _khachHangService.GetByID(id);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

        [Route("GetLastID")]
        public HttpResponseMessage GetById(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var LastID = _khachHangService.GetLastID();
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, LastID);
                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, KhachHang khachHang)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _khachHangService.Add(khachHang);
                    _khachHangService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage update(HttpRequestMessage request, KhachHangViewModel khachHangVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var vienchucDb = _khachHangService.GetByID(khachHangVM.MaKhachHang);

                    vienchucDb.UpdateKhachHang(khachHangVM);
                    _khachHangService.Update(vienchucDb);
                    _khachHangService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }


        [Route("creates")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Creates(HttpRequestMessage request, IEnumerable<KhachHangViewModel> khachHangViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in khachHangViewModels)
                    {
                        var newquaTrinhDaoTao = new KhachHang();
                        newquaTrinhDaoTao.UpdateKhachHang(item);
                        _khachHangService.Add(newquaTrinhDaoTao);
                        _khachHangService.Save();
                    }
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
       


        //[Route("getkhachhangfilter")]
        //public HttpResponseMessage GetKhachHangFilter(HttpRequestMessage request, int page, string ThuocTinh = null,
        //    DateTime? NgayDau = null,
        //    DateTime? NgayCuoi = null,
        //    double? GiaTriMin = null,
        //    double? GiaTriMax = null,
        //    string MaNhomKhachHang = null,
        //    string filters = null,
        //    string PhanLoai = null,
        //    string MaNhanVien = null,
        //    bool? DangHoatDong = null,
        //    bool xemtatca = true, int pageSize = 20)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {

        //        IEnumerable<KhachHang> data = _khachHangService.GetAll();
        //        //xemtatca = true;
        //        if (!xemtatca)
        //        {
        //            if (ThuocTinh.Equals("ngaytao"))
        //            {
        //                data = data.Where(x => NgayDau <= x.NgayTao && x.NgayTao <= NgayCuoi);
        //            }
        //            else if (ThuocTinh.Equals("sinhnhat"))
        //            {
        //                data = _khachHangService.getTheoSinhNhat(NgayDau, NgayCuoi);
        //            }
        //            else if (ThuocTinh.Equals("NgayGiaoDich"))
        //            {
        //                data = _khachHangService.getTheoNgayGiaoDich(NgayDau, NgayCuoi);
        //            }
        //            else if (ThuocTinh.Equals("TongBan"))
        //            {
        //                data = _khachHangService.getTheoTongBan(GiaTriMin, GiaTriMax, NgayDau, NgayCuoi);
        //            }

        //        }

        //        if (!string.IsNullOrEmpty(filters)) // vừa filter theo tìm kiếm và phân loại
        //        {
        //            data = _khachHangService.GetByKeyWord(filters);
        //        }

        //        if (!string.IsNullOrEmpty(PhanLoai)) // vừa filter theo tìm kiếm và phân loại
        //        {
        //            data = data.Where(x => x.PhanLoai == PhanLoai);
        //        }


        //        if (!string.IsNullOrEmpty(MaNhomKhachHang))
        //        {
        //            data = data.Where(x => x.NhomKH_NCC == MaNhomKhachHang);
        //        }
        //        if (!string.IsNullOrEmpty(MaNhanVien))
        //        {
        //            data = data.Where(x => (x.NVBanHang == MaNhanVien));
        //        }

        //        if (DangHoatDong != null)
        //        {
        //            data = data.Where(x => x.DangHoatDong == DangHoatDong);
        //        }

        //        int totalRow = 0;

        //        totalRow = data.Count();
        //        var query = data.OrderBy(x => x.MaKhachHang).Skip(page * pageSize).Take(pageSize);

        //        IEnumerable<KhachHangViewModel> responseData = Mapper.Map<IEnumerable<KhachHang>, IEnumerable<KhachHangViewModel>>(query);
        //        //gắn người tạo
        //        foreach (KhachHangViewModel kh in responseData)
        //        {
        //            if (kh.NVBanHang != null)
        //            {
        //                kh.NguoiTao = _nhanVienService.GetByID(kh.NVBanHang).HoVaTen;
        //            }
        //        }

        //        var PaginationSet = new PaginationSet<KhachHangViewModel>()
        //        {
        //            Items = responseData,
        //            Page = page,
        //            TotalCount = totalRow,/*tong so ban ghi*/
        //            TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/
        //        };

        //        HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);

        //        return response;
        //    });
        //}




        [Route("exporttoexcelByMaKhachHang")]
        [HttpGet]
        public async Task<HttpResponseMessage> ExportKhachHangToXlsx(HttpRequestMessage request, string MaKhachHang)
        {
            string fileName = string.Concat("KhachHang" + DateTime.Now.ToString("-yyyy-MM-dd-hh-mm-sss") + ".xlsx");
            var folderReport = @"/Reports";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string fullPath = Path.Combine(filePath, fileName);
            try
            {
                var data = _khachHangService.GetKhachHangTheoMa(MaKhachHang).ToList();
                var khachHangTheoMa = Mapper.Map<List<KhachHang>, List<KhachHang>>(data);
                await ReportHelper.GenerateXls(khachHangTheoMa, fullPath);
                return request.CreateErrorResponse(HttpStatusCode.OK, Path.Combine(folderReport, fileName));
            }
            catch (Exception ex)
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

      

        [Route("getbykeyword")]
        public HttpResponseMessage getbykeyword(HttpRequestMessage request,string keyword=null)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _khachHangService.GetAll();
                if (listCategory.Count()>0)
                {
                    if (!string.IsNullOrEmpty(keyword))
                    {
                        listCategory = listCategory.Where(x => x.TenKhachHang.ToLower().Contains(keyword) || x.MaKhachHang.ToLower().Contains(keyword)|| ((x.SoDienThoai!=null)&&(x.SoDienThoai.Contains(keyword)))|| ((x.Email != null) && (x.Email.ToLower().Contains(keyword))));
                    }
                    listCategory = listCategory.Where(x => x.MaKhachHang != "KH000000");
                }
                var b = listCategory.OrderBy(x => x.MaKhachHang).ToList();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, b);


                return response;
            });
        }
        [Route("capnhatsaukhixacnhan")]
        [HttpPut]
        public HttpResponseMessage capnhatsaukhixacnhan(HttpRequestMessage request, KhachHangViewModel khachHangVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var khachhang = _khachHangService.GetByID(khachHangVM.MaKhachHang);
                    khachhang.TenKhachHang = khachHangVM.TenKhachHang;
                    khachhang.SoDienThoai = khachHangVM.SoDienThoai;
                    khachhang.DiaChi = khachHangVM.DiaChi;
                    _khachHangService.Update(khachhang);
                    _khachHangService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
    }
}
