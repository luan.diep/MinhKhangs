﻿using AutoMapper;
using Newtonsoft.Json.Linq;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;

namespace Platform.Web.Api
{
    [RoutePrefix("api/nhomhanghoa")]
   
    public class NhomHangHoaController : ApiControllerBase
    {
        INhomHangHoaService _nhomHangHoaService;
        INhomHangHoaConService _nhomHangHoaConService;

        public NhomHangHoaController(ILoiService loiService, INhomHangHoaService nhomHangHoaService, INhomHangHoaConService nhomHangHoaConService) : base(loiService)
        {
            this._nhomHangHoaService = nhomHangHoaService;
            this._nhomHangHoaConService = nhomHangHoaConService;
        }
        //[Route("createExcel")]
        //[HttpPost]
        //// [AllowAnonymous]
        //public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<ChiTietBaoGiaViewModel> chiTietBaoGiaVM)
        //{


        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        if (!ModelState.IsValid)
        //        {
        //            response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
        //        }
        //        else
        //        {
        //            foreach (var item in chiTietBaoGiaVM)
        //            {
        //                var newThongBao = new ChiTietBaoGia();
        //                newThongBao.UpdateChiTietBaoGia(item);

        //                _nhomHangHoaService.Add(newThongBao);
        //                _nhomHangHoaService.Save();
        //            }


        //            //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
        //            response = request.CreateResponse(HttpStatusCode.OK);
        //        }

        //        return response;
        //    });
        //}


        //[Route("update")]
        //[HttpPut]
        //[AllowAnonymous]
        //public HttpResponseMessage update(HttpRequestMessage request, ChiTietBaoGiaViewModel chiTietBaoGiaViewModel)

        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        if (!ModelState.IsValid)
        //        {
        //            request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        //        }
        //        else
        //        {

        //            var vienchucDb = _nhomHangHoaService.getID(Convert.ToInt32(chiTietBaoGiaViewModel.ID));

        //            vienchucDb.UpdateChiTietBaoGia(chiTietBaoGiaViewModel);
        //            _nhomHangHoaService.Update(vienchucDb);
        //            _nhomHangHoaService.Commit();

        //            response = request.CreateResponse(HttpStatusCode.OK);

        //        }
        //        return response;
        //    });
        //}


       


       

        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _nhomHangHoaService.GetAll();
                //  var responseData = Mapper.Map<IEnumerable<ChiTietBaoGia>,IEnumerable<ChiTietBaoGiaViewModel>>(listCategory);
                // var b = listCategory.OrderBy(x => x.MaChiTietBaoGia.Length + x.MaChiTietBaoGia);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

        
        [Route("GroupNhomHangHoa")]
        [HttpGet]
        public HttpResponseMessage GroupNhomHangHoa(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {

                var AllNhomHangHoa = Mapper.Map <IEnumerable<NhomHangHoa>, List<NhomHangHoaGroup>>(_nhomHangHoaService.GetAll());
                List<int> remove = new List<int>();
                foreach (var item in AllNhomHangHoa)
                {
                    var listnhomcon = _nhomHangHoaService.getMaNhom(item.MaNhomHH).ToList();
                    if (listnhomcon.Count() != 0) {
                        item._List = (List<NhomHangHoaGroups>)_nhomHangHoaService.getMaNhoms(listnhomcon);
                        foreach (var item1 in listnhomcon)
                        {
                            remove.Add(item1.MaNhomHHPhu.Value);
                        }
                    }
                }


                foreach (var i in remove)
                {
                    var index = AllNhomHangHoa.FindIndex(x => x.MaNhomHH == i);
                    if (index != -1)
                    {
                        AllNhomHangHoa.Remove(AllNhomHangHoa[index]);

                    }
                }
               




                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, AllNhomHangHoa);


                return response;
            });
        }







         [Route("getallCatelogy")]
        public HttpResponseMessage getallCatelogy(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _nhomHangHoaService.getallCatelogy().ToList();
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }


        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, NhomHangHoaViewModel nhomHang)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newnhomHang = new NhomHangHoa();
                    newnhomHang.UpdateNhomHangHoa(nhomHang);
                    _nhomHangHoaService.Add(newnhomHang);
                    _nhomHangHoaService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });

        }






    }
}
