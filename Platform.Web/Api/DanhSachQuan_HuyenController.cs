﻿using AutoMapper;
using Newtonsoft.Json.Linq;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;

namespace Platform.Web.Api
{
    [RoutePrefix("api/danhsachquan_huyen")]

    public class DanhSachQuan_HuyenController : ApiControllerBase
    {
        IDanhSachQuan_HuyenService _danhSachQuan_HuyenService;
        IDanhSachTinh_TPService _danhSachTinh_TPService;

        public DanhSachQuan_HuyenController(ILoiService loiService, IDanhSachQuan_HuyenService danhSachQuan_HuyenService, IDanhSachTinh_TPService danhSachTinh_TPService) : base(loiService)
        {
            this._danhSachQuan_HuyenService = danhSachQuan_HuyenService;
            this._danhSachTinh_TPService = danhSachTinh_TPService;
        }
      
       
		 
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _danhSachQuan_HuyenService.GetAll().Where(x => x.MaTinh_TP != "00");
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }
         
        [Route("ChonTheoTinh")]
        [HttpGet]
        public HttpResponseMessage ChonTheoTinh(HttpRequestMessage request,string matinh)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _danhSachQuan_HuyenService.ChonTheoTinh(matinh).Where(x=>x.MaTinh_TP!="00");
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }


        [Route("laytatcadanhsach")]
        [HttpGet]
        public HttpResponseMessage laytatcadanhsach(HttpRequestMessage request,string keyword=null)
        {
            return CreateHttpResponse(request, () =>
            {
                var list = new List<string>();
                var listQuaHuyen = _danhSachQuan_HuyenService.GetAll();
                foreach (var item in listQuaHuyen)
                {
                    list.Add(item.TenQuan_Huyen);
                }
                var listtinh_tp = _danhSachTinh_TPService.GetAll();
                foreach (var item in listtinh_tp)
                {
                    list.Add(item.TenTinh_TP);
                }
                if (!string.IsNullOrEmpty(keyword))
                {
                    list = list.Where(x => x.ToLower().Contains(keyword.ToLower())).ToList();
                }

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, list);
                return response;
            });
        }
        [Route("getbykeyword")]
        public HttpResponseMessage getbykeyword(HttpRequestMessage request,string keyword=null)
        {
            return CreateHttpResponse(request, () =>
            {
                var list = _danhSachQuan_HuyenService.LayTatCaKemTinh();
               
                foreach (var item in list)
                {
                    item.TenQuan_Huyen = item.TenQuan_Huyen + ", " + item.DanhSachTinh_TP.TenTinh_TP;
                }

                if (!string.IsNullOrEmpty(keyword))
                {
                    list = list.Where(x => x.TenQuan_Huyen.ToLower().Contains(keyword.ToLower()));
                }
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, list);
                return response;
            });
        }

    }
}
