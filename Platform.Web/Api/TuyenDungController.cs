﻿
using Platform.Common;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Platform.Web.Api
{
   


    [RoutePrefix("api/tuyendung")]
   // [Authorize]
    public class TuyenDungController : ApiControllerBase
    {
        ITuyenDungService _tuyenDungService;
        public TuyenDungController(ILoiService loiService, ITuyenDungService tuyenDungService) : base(loiService)
        {
            this._tuyenDungService = tuyenDungService;
        }


        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _tuyenDungService.GetAll();
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }

        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, TuyenDungViewModel tuyenDungViewModel)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newThongBao = new TuyenDung();
                    newThongBao.UpdateTuyenDung(tuyenDungViewModel);
                    _tuyenDungService.Add(newThongBao);
                    _tuyenDungService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });

        }





    }
}