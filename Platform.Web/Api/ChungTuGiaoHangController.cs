﻿using AutoMapper;
using Newtonsoft.Json.Linq;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;

namespace Platform.Web.Api
{
    [RoutePrefix("api/chungtugiaohang")]
    [Authorize]
    public class ChungTuGiaoHangController : ApiControllerBase
    {
        IChungTuGiaoHangService _chungTuGiaoHangService;
    


        public ChungTuGiaoHangController(ILoiService loiService, IChungTuGiaoHangService chungTuGiaoHangService) : base(loiService)
        {
            this._chungTuGiaoHangService = chungTuGiaoHangService;
        
        }


        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, ChungTuGiaoHangViewModel chungTuGiaoHang)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newThongBao = new ChungTuGiaoHang();
                    newThongBao.UpdateChungTuGiaoHang(chungTuGiaoHang);

                    _chungTuGiaoHangService.Add(newThongBao);
                    _chungTuGiaoHangService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });

        }

        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _chungTuGiaoHangService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });

        }

     

        [Route("laymacuoicung")]
        [HttpGet]
        public HttpResponseMessage laymacuoicung(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _chungTuGiaoHangService.GetLastID();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });

        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage createExcel(HttpRequestMessage request, IEnumerable<ChungTuGiaoHangViewModel> chungTuGiaoHangVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in chungTuGiaoHangVm)
                    {
                        string laytatca = _chungTuGiaoHangService.GetLastID().Split(new[] { "GH" }, StringSplitOptions.None)[1];
                        string str = "" + (Convert.ToInt32(laytatca) + 1);
                        string pad1 = "000000";

                        string MaCuoiCung = pad1.Substring(0, pad1.Length - str.Length) + str;
                        string mamoi = "GH" + MaCuoiCung;

                        var newThongBao = new ChungTuGiaoHang();
                        newThongBao.UpdateChungTuGiaoHang(item);
                        newThongBao.MaChungTuGiaoHang = mamoi;
                        _chungTuGiaoHangService.Add(newThongBao);
                        _chungTuGiaoHangService.Save();
                    }


                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        //[Route("getgiaohang")]
        //[HttpGet]
        //public HttpResponseMessage getgiaohang(HttpRequestMessage request, DateTime ngaydau, DateTime ngaycuoi, bool xemtatca, int page, int pageSize = 20)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        int totalRow = 0;
        //        var nhapkho = _chungTuGiaoHangService.getgiaohang(ngaydau, ngaycuoi, xemtatca);

        //        totalRow = nhapkho.Count();
        //        var query = nhapkho.OrderByDescending(x => x.MaChungTuGiaoHang).Skip(page * pageSize).Take(pageSize);

        //        var responseData = Mapper.Map<IEnumerable<getchungtubanhang>, IEnumerable<getchungtubanhang>>(query);

        //        var PaginationSet = new PaginationSet<getchungtubanhang>()
        //        {
        //            Items = responseData,
        //            Page = page,
        //            TotalCount = totalRow,/*tong so bai ghi*/
        //            TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/
        //        };

        //        HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);
        //        return response;
        //    });
        //}


    }
}
