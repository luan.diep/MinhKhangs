﻿using AutoMapper;
using Newtonsoft.Json.Linq;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using Platform.Common;
using Platform.Data.Repositories;
using System.Web.Script.Serialization;
using WebGrease.Css.Extensions;

namespace Platform.Web.Api
{
    [RoutePrefix("api/hangHoa")]
  //  [Authorize]
    public class HangHoaController : ApiControllerBase
    {
        IHangHoaService _hangHoaService;
        IChiTietDonMuaHangService _chiTietDonMuaHangService;
        IChiTietDonDatHangService _chiTietDonDatHangService;
        IChungTuGiaoHangService _chungTuGiaoHangService;
        IHangHoa_CoSoService _hangHoa_CoSoService;
        IDonViTinhService _donViTinhService;
        INhomHangHoaService _nhomhanghoaService;
        IHangHoaYeuThichService _hanghoayeuthichService;
        IKhachHangService _khachHang;
      

        public HangHoaController(ILoiService loiService,
            IHangHoaService hangHoaService,
            IHangHoaYeuThichService hangHoaYeuThichService,
            IKhachHangService khachHang,
            IDonViTinhService donViTinhService,
            INhomHangHoaService nhomhanghoa,
          
            IChiTietDonDatHangService chiTietDonDatHangService,
         
            IChungTuGiaoHangService chungTuGiaoHangService,
            IChiTietDonMuaHangService chiTietDonMuaHangService,

            IHangHoa_CoSoService hangHoa_CoSoService

            ) : base(loiService)
        {
            this._hangHoaService = hangHoaService;
            this._hangHoa_CoSoService = hangHoa_CoSoService;
         
            this._donViTinhService = donViTinhService;
            this._nhomhanghoaService = nhomhanghoa;
           
            this._chiTietDonDatHangService = chiTietDonDatHangService;
           
            this._chungTuGiaoHangService = chungTuGiaoHangService;
            this._chiTietDonMuaHangService = chiTietDonMuaHangService;
            this._hanghoayeuthichService = hangHoaYeuThichService;
            this._khachHang = khachHang;
          

        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<HangHoaViewModel> hangHoaVM)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in hangHoaVM)
                    {
                        string MaHangHoaCuoiCung;
                        string laytatca = _hangHoaService.GetMaHangHoaCuoiCung();
                        if (laytatca != null)
                        {
                            var MaCuoiCung = laytatca;
                            string str = "" + (Convert.ToInt32(MaCuoiCung) + 1);
                            string pad1 = "000000000000";

                            MaHangHoaCuoiCung = pad1.Substring(0, pad1.Length - str.Length) + str;
                        }
                        else
                        {
                            MaHangHoaCuoiCung = "000000000001";

                        }
                        var newThongBao = new HangHoa();
                        newThongBao.UpdateHangHhoa(item);
                        newThongBao.NgayNhap = DateTime.Now;
                        newThongBao.MaHang = MaHangHoaCuoiCung;
                        _hangHoaService.Add(newThongBao);
                        _hangHoaService.Save();
                    }
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }


        [Route("update")]
        [HttpPut]
        public HttpResponseMessage update(HttpRequestMessage request, HangHoaViewModel hangHoaViewModel)

        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var vienchucDb = _hangHoaService.GetByID(hangHoaViewModel.MaHang);

                    vienchucDb.UpdateHangHhoa(hangHoaViewModel);
                    vienchucDb.NgaySua = DateTime.Now;
                    _hangHoaService.Update(vienchucDb);
                    _hangHoaService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }



        [Route("thaydoingungkinhdoanh")]
        [HttpPut]
        public HttpResponseMessage thaydoingungkinhdoanh(HttpRequestMessage request, HangHoaViewModel hangHoaViewModel)

        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var vienchucDb = _hangHoaService.GetByID(hangHoaViewModel.MaHang);
                    vienchucDb.NgaySua = DateTime.Now;
                    vienchucDb.NgungKinhDoanh = hangHoaViewModel.NgungKinhDoanh;
                    _hangHoaService.Update(vienchucDb);
                    _hangHoaService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        [Route("layhanghoadathang")]
        [HttpGet]
        [AllowAnonymous]

        public HttpResponseMessage getallhanghoadathang(HttpRequestMessage request, int page, int pageSize)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var hanghoa = _hangHoaService.layhanghoaonl();
                var maKH = _khachHang.getmaKH(User.Identity.Name);
                var allheart= _hanghoayeuthichService.getmaKH(maKH);
                foreach (var item in allheart)
                {
                    foreach (var items in hanghoa)
                    {
                        if (item.MaHang == items.MaHang) {
                            items.heart = item.DaXoa;
                        }

                    }
                }


                //foreach (var item in hanghoa)
                //{
                //    item.TenDonViTinh = _donViTinhService.GetByID(item.MaDonViTinh).TenDonViTinh;
                //}
               
                hanghoa=hanghoa.OrderBy(x => x.MaNhomHH);
                totalRow = hanghoa.Count();
                var query = hanghoa.Skip(page * pageSize).Take(pageSize);
                var responseData = Mapper.Map<IEnumerable<HangHoaOnl>, IEnumerable<HangHoaOnl>>(query);
                var PaginationSet = new PaginationSet<HangHoaOnl>()
                {
                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,/*tong so bai ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/
                };
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);
                return response;
            });
        }


        [Route("layhanghoadathangs")]
        [HttpGet]
        [AllowAnonymous]

        public HttpResponseMessage layhanghoadathangs(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {

                List<HangHoaOnl> GroupList = new List<HangHoaOnl>();
                var searchnhomHH = _nhomhanghoaService.getMaNhom(id);
                if (searchnhomHH.Count() > 0)
                {
                    foreach (var item in searchnhomHH)
                    {
                        GroupList.AddRange(_hangHoaService.layhanghoaonls(item.MaNhomHHPhu.Value));
                    }


                }
                else {
                    GroupList.AddRange(_hangHoaService.layhanghoaonls(id)) ;
                }

                var maKH = _khachHang.getmaKH(User.Identity.Name);
                var allheart = _hanghoayeuthichService.getmaKH(maKH);
                foreach (var item in allheart)
                {
                    foreach (var items in GroupList)
                    {
                        if (item.MaHang == items.MaHang)
                        {
                            items.heart = item.DaXoa;
                        }

                    }
                }




                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, GroupList);
                return response;
            });
        }





        [Route("getallhanghoafilter")]
        [HttpGet]
        public HttpResponseMessage getallhanghoafilter(HttpRequestMessage request, int page, int pageSize = 20,string nhomhang=null,string ncc=null,string ngungkinhdoanh=null,string timkiem=null,int? thuonghieu=null, string vitri =null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var  listCategory = _hangHoaService.GetAll().Where(x=>x.DaXoa!=true);
                if (!string.IsNullOrEmpty(nhomhang))
                {
                    listCategory = listCategory.Where(x => x.MaNhomHH == Convert.ToInt32(nhomhang));
                }
                if (!string.IsNullOrEmpty(ncc))
                {
                    listCategory = listCategory.Where(x => x.MaNhaCungCap == (ncc));
                }
                if (!string.IsNullOrEmpty(ngungkinhdoanh))
                {
                    if (ngungkinhdoanh=="false")
                    {
                        listCategory = listCategory.Where(x => x.NgungKinhDoanh == false||x.NgungKinhDoanh==null);
                    }
                    else
                    {
                        listCategory = listCategory.Where(x => x.NgungKinhDoanh == true);
                    }
                }
                if (!string.IsNullOrEmpty(timkiem))
                {
                    listCategory = listCategory.Where(x => x.MaHang.Contains(timkiem)||x.TenHang.Contains(timkiem));
                }
                if (thuonghieu.HasValue)
                {
                    listCategory = listCategory.Where(x => x.MaThuongHieu == thuonghieu);
                }
                if (!string.IsNullOrEmpty(vitri))
                {
                    listCategory = listCategory.Where(x => x.ViTri==(vitri));
                }

                var hanghoa = Mapper.Map<IEnumerable<HangHoa>, IEnumerable<HangHoaViewModel>>(listCategory);
                foreach (var item in hanghoa)
                {
                    item.TenDonViTinh = _donViTinhService.GetByID(item.MaDonViTinh).TenDonViTinh;
                }
                totalRow = hanghoa.Count();
                var query = hanghoa.OrderByDescending(x => x.MaHang).Skip(page * pageSize).Take(pageSize);
                var responseData = Mapper.Map<IEnumerable<HangHoaViewModel>, IEnumerable<HangHoaViewModel>>(query);
                var PaginationSet = new PaginationSet<HangHoaViewModel>()
                {
                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,/*tong so bai ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/
                };
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);
                return response;
            });
        }



        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, HangHoaViewModel khachHang)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newThongBao = new HangHoa();
                    newThongBao.UpdateHangHhoa(khachHang);
                    newThongBao.NgayNhap = DateTime.Now;
                    newThongBao.GiaNhapCuoi = newThongBao.GiaBan;
                    if (newThongBao.BaoHanh==null)
                    {
                        newThongBao.BaoHanh = "00";
                    }
                    if (newThongBao.NguonGoc==null)
                    {
                        newThongBao.NguonGoc = "00";
                    }
                    if (newThongBao.HanSuDung==null)
                    {
                        newThongBao.HanSuDung = DateTime.Parse("9999-12-31");
                    }
                    newThongBao.NguoiSua = newThongBao.NguoiNhap;
                    newThongBao.NgaySua = newThongBao.NgayNhap;
                    newThongBao.NgungKinhDoanh = false;
                    newThongBao.DaXoa = false;
                  
                    _hangHoaService.Add(newThongBao);
                    _hangHoaService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });

        }

        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _hangHoaService.GetAll().Where(x=>x.DaXoa==false||x.DaXoa==null);
               var HangHoaMoi= Mapper.Map<IEnumerable<HangHoa>, IEnumerable<HangHoaViewModel>>(listCategory);
                foreach (var item in HangHoaMoi)
                {
                    item.TenDonViTinh = _donViTinhService.GetByID(item.MaDonViTinh).TenDonViTinh;
                }

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, HangHoaMoi);


                return response;
            });
        }



        [Route("Getcategory_product")]
        public HttpResponseMessage Getcategory_product(HttpRequestMessage request,int manhom, int page, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                List<HangHoa> listCategory = new List<HangHoa>();
                var tennhom = "";
                var countnhom = _nhomhanghoaService.getMaNhom(manhom).ToList().AsEnumerable();
                
                if (countnhom.Count() > 0)
                {
                    

                    countnhom.ForEach((item, index) =>
                    {
                        listCategory.AddRange(_hangHoaService.getmanhom(item.MaNhomHHPhu.Value));
                        //if (index== countnhom.Count() - 1) {
                        //    listCategory.AddRange(_hangHoaService.getmanhom(item.MaNhomHHPhu.Value));
                        //}
                    });
                    tennhom = _hangHoaService.gettennhom(manhom);
                    
                }
                else {

                    listCategory.AddRange(_hangHoaService.GetAll().Where(x => x.MaNhomHH == manhom));
                    tennhom = _hangHoaService.gettennhom(manhom);
                }
               


                var map = Mapper.Map<List<HangHoa>, IEnumerable<HangHoaViewModel>>(listCategory);
                var maKH = _khachHang.getmaKH(User.Identity.Name);
                var allheart = _hanghoayeuthichService.getmaKH(maKH);
                foreach (var item in allheart)
                {
                    foreach (var items in map)
                    {
                        if (item.MaHang == items.MaHang)
                        {
                            items.heart = item.DaXoa;
                        }

                    }
                }

                foreach (var item in map)
                {
                    item.TenNhom = _nhomhanghoaService.GetByID(item.MaNhomHH).TenNhom;

                }
                totalRow = map.Count();
                var query = map.OrderByDescending(x => x.MaHang).Skip(page * pageSize).Take(pageSize);
                var responseData = Mapper.Map<IEnumerable<HangHoaViewModel>, IEnumerable<HangHoaViewModel>>(query);
                var PaginationSet = new PaginationSet<HangHoaViewModel>()
                {
                    Items = responseData,
                    Page = page,
                    TenNhom = tennhom,
                    TotalCount = totalRow,/*tong so bai ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/
                };
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);
                return response;
            });
        }





         [Route("gethinhanh")]
        public HttpResponseMessage gethinhanh(HttpRequestMessage request,string mahang)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _hangHoaService.GetByID(mahang);
              
               
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        
         [Route("homecategory")]
         [HttpGet]
        public HttpResponseMessage homecategory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _hangHoaService.homecategory();
              
               
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
         [Route("GroupProduct")]
         [HttpGet]
        public HttpResponseMessage GroupProduct(HttpRequestMessage request ,string id, int page, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {

                int totalRow = 0;

                var HangHoa = _hangHoaService.GetByID(id);
                var listCategory = _hangHoaService.Groupproduct(HangHoa.MaNhomHH).Where(x=>x.MaHang!=id);
                totalRow = listCategory.Count();
                var query = listCategory.OrderByDescending(x => x.MaHang).Skip(page * pageSize).Take(pageSize);
                var PaginationSet = new PaginationSet<Homecaterogy>()
                {
                    Items = query,
                    Page = page,
                    TotalCount = totalRow,/*tong so bai ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/
                };

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);


                return response;
            });
        }




        //[Route("getbyid")]
        //[HttpGet]
        //public HttpResponseMessage getbyid(HttpRequestMessage request,string id,string macoso)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {


        //        var listCategory = _hangHoaService.lay1HangHoaTheoCoSo(id,macoso);
        //        var hangHoa = Mapper.Map<HangHoa, HangHoaViewModel>(listCategory);
        //        if (hangHoa!=null)
        //        {
        //            var tinhchat = _tinhChatHangHoaService.GetByID(listCategory.MaTinhChat);
        //            hangHoa.TinhChatHangHoa = Mapper.Map<TinhChatHangHoa, TinhChatHangHoaViewModel>(tinhchat);
        //            var nhomhh = _nhomHangHoaService.GetByID(listCategory.MaNhomHH);
        //            hangHoa.NhomHangHoa = Mapper.Map<NhomHangHoa, NhomHangHoaViewModel>(nhomhh);
        //            hangHoa.getdvt = _hangHoa_DonViTinhService.getdvt(listCategory.MaHang);
        //            hangHoa.TenDonViTinh = _donViTinhService.GetByID(hangHoa.MaDonViTinh).TenDonViTinh;
                   
        //        }
        //        HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, hangHoa);


        //        return response;
        //    });
        //}


        [Route("getMaHangHoaCuoiCung")]
        [HttpGet]
        public HttpResponseMessage getMaHangHoaCuoiCung(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                string MaHangHoaCuoiCung = _hangHoaService.GetMaHangHoaCuoiCung();
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, MaHangHoaCuoiCung);
                return response;
            });
        }


        [Route("getdvt")]
        [HttpGet]
        public HttpResponseMessage getdvt(HttpRequestMessage request, int madvt)
        {
            return CreateHttpResponse(request, () =>
            {

                      var tendvt = _donViTinhService.GetByID(madvt);
                      var  hangHoa = Mapper.Map<DonViTinh, DonViTinhViewModel>(tendvt);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, hangHoa);
                return response;
            });
        }

        //[Route("GetHangHoaForThietLap")]
        //[HttpGet]
        //public HttpResponseMessage getHangHoaForThietLap(HttpRequestMessage request, int page, int pageSize = 20,string timkiem=null,string nhomhang=null)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {

        //        int totalRow = 0;
        //        var listCategory = _hangHoaService.GetAll();
        //        if (!string.IsNullOrEmpty(nhomhang))
        //        {
        //            listCategory = listCategory.Where(x => x.MaNhomHH == Convert.ToInt32(nhomhang));
        //        }
        //        if (!string.IsNullOrEmpty(timkiem))
        //        {
        //            listCategory = listCategory.Where(x => x.MaHang.Contains(timkiem)||x.TenHang.Contains(timkiem));
        //        }

        //        var hanghoa = Mapper.Map<IEnumerable<HangHoa>, IEnumerable<HangHoaViewModel>>(listCategory);
        //        if (hanghoa != null)
        //        {
        //            foreach (var item in hanghoa)
        //            {
                       
        //                var nhomhh = _nhomHangHoaService.GetByID(item.MaNhomHH);
        //                item.NhomHangHoa = Mapper.Map<NhomHangHoa, NhomHangHoaViewModel>(nhomhh);
        //                item.TenDonViTinh = _donViTinhService.GetByID(item.MaDonViTinh).TenDonViTinh;
        //            }
        //        }
        //        totalRow = hanghoa.Count();
        //        var query = hanghoa.OrderByDescending(x => x.MaHang).Skip(page * pageSize).Take(pageSize);
        //        var responseData = Mapper.Map<IEnumerable<HangHoaViewModel>, IEnumerable<HangHoaViewModel>>(query);
        //        var PaginationSet = new PaginationSet<HangHoaViewModel>()
        //        {
        //            Items = responseData,
        //            Page = page,
        //            TotalCount = totalRow,/*tong so bai ghi*/
        //            TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/
        //        };
        //        HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);
        //        return response;


               
        //    });
        //}
        [Route("exporttoexcel")]
        [HttpGet]
        public async Task<HttpResponseMessage> ExportProductsToXlsx(HttpRequestMessage request, string nhomhang = null, string ncc = null, string ngungkinhdoanh = null, string timkiem = null)
        {
            string fileName = string.Concat("HangHoa_" + DateTime.Now.ToString("yyyyMMddhhmmsss") + ".xlsx");
            var folderReport = @"/Reports";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string fullPath = Path.Combine(filePath, fileName);
            try
            {
                var data = _hangHoaService.GetAll().Where(x=>x.DaXoa!=true).ToList();
                if (!string.IsNullOrEmpty(nhomhang))
                {
                    data = data.Where(x => x.MaNhomHH == Convert.ToInt32(nhomhang)).ToList();
                }
                if (!string.IsNullOrEmpty(ncc))
                {
                    data = data.Where(x => x.MaNhaCungCap == (ncc)).ToList();
                }
                if (!string.IsNullOrEmpty(ngungkinhdoanh))
                {
                    if (ngungkinhdoanh == "false")
                    {
                        data = data.Where(x => x.NgungKinhDoanh == false || x.NgungKinhDoanh == null).ToList();
                    }
                    else
                    {
                        data = data.Where(x => x.NgungKinhDoanh == true).ToList();
                    }
                }
                if (!string.IsNullOrEmpty(timkiem))
                {
                    data = data.Where(x => x.MaHang.Contains(timkiem) || x.TenHang.Contains(timkiem)).ToList();
                }

                var products = Mapper.Map<List<HangHoa>, List<ExportedProduct>>(data);
                await ReportHelper.GenerateXls(products, fullPath);
                return request.CreateErrorResponse(HttpStatusCode.OK, Path.Combine(folderReport, fileName));
            }
            catch (Exception ex)
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [Route("xuat1hanghoa")]
        [HttpGet]
        public async Task<HttpResponseMessage> xuat1hanghoa(HttpRequestMessage request,string id)
        {
            string fileName = string.Concat("HangHoa_"+ id+'_' + DateTime.Now.ToString("yyyyMMddhhmmsss") + ".xlsx");
            var folderReport = @"/Reports";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string fullPath = Path.Combine(filePath, fileName);
            try
            {
                var data = _hangHoaService.GetAll().Where(x=>x.MaHang==id).ToList();
                var products = Mapper.Map<List<HangHoa>, List<ExportedProduct>>(data);
                await ReportHelper.GenerateXls(products, fullPath);
                return request.CreateErrorResponse(HttpStatusCode.OK, Path.Combine(folderReport, fileName));
            }
            catch (Exception ex)
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }


      

        [Route("getallfilterforsmartphone")]
        [HttpGet]
        public HttpResponseMessage getallfilterforsmartphone(HttpRequestMessage request,  string nhomhang = null, string ncc = null, string ngungkinhdoanh = null, string timkiem = null, string thuonghieu = null, string vitri = null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var listCategory = _hangHoaService.GetAll().Where(x => x.DaXoa != true);
                if (!string.IsNullOrEmpty(nhomhang))
                {
                    listCategory = listCategory.Where(x => x.MaNhomHH == Convert.ToInt32(nhomhang));
                }
                if (!string.IsNullOrEmpty(ncc))
                {
                    listCategory = listCategory.Where(x => x.MaNhaCungCap == (ncc));
                }
                if (!string.IsNullOrEmpty(ngungkinhdoanh))
                {
                    if (ngungkinhdoanh == "false")
                    {
                        listCategory = listCategory.Where(x => x.NgungKinhDoanh == false || x.NgungKinhDoanh == null);
                    }
                    else
                    {
                        listCategory = listCategory.Where(x => x.NgungKinhDoanh == true);
                    }
                }
                if (!string.IsNullOrEmpty(timkiem))
                {
                    listCategory = listCategory.Where(x => x.MaHang.Contains(timkiem) || x.TenHang.Contains(timkiem));
                }
                if (!string.IsNullOrEmpty(thuonghieu))
                {
                    listCategory = listCategory.Where(x => x.MaThuongHieu == Convert.ToInt32(thuonghieu));
                }
                if (!string.IsNullOrEmpty(vitri))
                {
                    listCategory = listCategory.Where(x => x.ViTri == (vitri));
                }

                var hanghoa = Mapper.Map<IEnumerable<HangHoa>, IEnumerable<HangHoaViewModel>>(listCategory);
                foreach (var item in hanghoa)
                {
                    item.TenDonViTinh = _donViTinhService.GetByID(item.MaDonViTinh).TenDonViTinh;
                }
                totalRow = hanghoa.Count();
                var responseData = Mapper.Map<IEnumerable<HangHoaViewModel>, IEnumerable<HangHoaViewModel>>(hanghoa);
                
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        //[Route("gethoatdong")]
        //[HttpGet]
        //public HttpResponseMessage gethoatdong(HttpRequestMessage request, string mahang)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {

        //        var dondathang = _chiTietDonDatHangService.hoatDongHangHoa(mahang);
        //        var ctbh = _chiTietChungTuBanHangService.hoatDongHangHoa(mahang);
        //        var ctgh = _chungTuGiaoHangService.hoatDongHangHoa(mahang);
        //        var dathang = _chiTietDonMuaHangService.hoatDongHangHoa(mahang);
        //        var nhapkho = _chiTietPhieuNhapKhoService.hoatDongHangHoa(mahang);
        //        var trahangnhap = _chiTietTraLaiHangMuaService.hoatDongHangHoa(mahang);
        //        var chuyenkho = _chiTietChuyenKhoService.hoatDongHangHoa(mahang);
        //        var phieuchi = _chiTietPhieuChiService.hoatDongHangHoa(mahang);
        //        var phieuthu = _chiTietThuService.hoatDongHangHoa(mahang);

        //        var merge = dondathang.Concat(ctbh).Concat(ctgh).Concat(dathang).Concat(nhapkho).Concat(trahangnhap).Concat(chuyenkho).Concat(phieuchi).Concat(phieuthu).OrderByDescending(x=>x.ThoiGian);
                
        //        HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, merge);
        //        return response;
        //    });
        //}

        
        [Route("chitiethanghoadathang")]
        [HttpGet]
    //    [AllowAnonymous]
        public HttpResponseMessage chitiethanghoadathang(HttpRequestMessage request,  string mahang)
        {
            return CreateHttpResponse(request, () =>
            {
                var hh = _hangHoaService.layhanghoaonlById(mahang);
               

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, hh);
                return response;
            });
        }

       

        [Route("getbyname")]
        [HttpGet]
        public HttpResponseMessage getbyname(HttpRequestMessage request, string id = null,string locmahang=null)
        {
            return CreateHttpResponse(request, () =>
            {
                IEnumerable<HangHoa> list = new List<HangHoa>();
                if (!string.IsNullOrEmpty(id))
                {
                    list = _hangHoaService.GetByName(id).ToList();
                }
                else
                {
                    list = _hangHoaService.GetAll().ToList();
                }
                


                var map = Mapper.Map<IEnumerable<HangHoa>, IEnumerable<HangHoaViewModel>>(list).ToList();
                foreach (var item in map)
                {
                    item.TenDonViTinh = _donViTinhService.GetByID(item.MaDonViTinh).TenDonViTinh;
                }
                if (!string.IsNullOrEmpty(locmahang))
                {
                    var lismahang = new JavaScriptSerializer().Deserialize<List<string>>(locmahang);                   
                    foreach (var item1 in lismahang)
                    {
                        map.RemoveAll(x => x.MaHang == item1);
                    }
                }

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, map);


                return response;
            });
        }
    }
    public class ExportedProduct { 
        public string MaHang { get; set; }
        public int MaDonViTinh { get; set; }
        public string TenHang { get; set; }
        public int MaNhomHH { get; set; }
        public int MaTinhChat { get; set; }
        public string DonViTinh { get; set; }
        public string BaoHanh { get; set; }
        public string NguonGoc { get; set; }
        public string MoTa { get; set; }
        public Nullable<int> GiaNhap { get; set; }
        public Nullable<int> GiaBan { get; set; }
        public Nullable<int> GiaKhuyenMai { get; set; }
        public Nullable<double> VAT { get; set; }
        public Nullable<double> ChietKhau { get; set; }
        public string HanSuDung { get; set; }
        public string ThanhPham { get; set; }
        public string SoLo { get; set; }
        public string HinhAnh { get; set; }
        public string NguoiSua { get; set; }
        public Nullable<DateTime> NgaySua { get; set; }
        public Nullable<DateTime> NgayNhap { get; set; }
        public string NguoiNhap { get; set; }
        public string MaVach { get; set; }
        public string MaNhaCungCap { get; set; }
        public Nullable<int> MaThuongHieu { get; set; }
        public Nullable<int> DinhMucTonItNhat { get; set; }
        public Nullable<int> DinhMucTonNhieuNhat { get; set; }
        public Nullable<bool> NgungKinhDoanh { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public Nullable<bool> YeuThich { get; set; }
        public string GhiChu { get; set; }
        public Nullable<double> TrongLuong { get; set; }
        public string TenDonViTinh { get; set; }
        public Nullable<double> DonGia { get; set; }
    }
}
