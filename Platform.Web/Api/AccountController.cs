﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Platform.Common;
using Platform.Common.Exceptions;
using Platform.Model;
using Platform.Model.Models;
using Platform.Service;
using Platform.Web.App_Start;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace Platform.Web.Api
{
    [RoutePrefix("api/account")]
    public class AccountController : ApiController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private IKhachHangService _khachhangservice;

        public AccountController()
        {
        }
       


        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, IKhachHangService khachhangservice)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            this._khachhangservice = khachhangservice;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public async Task<HttpResponseMessage> Login(HttpRequestMessage request, string userName, string password, bool rememberMe)
        {
            

            var result = await SignInManager.PasswordSignInAsync(userName, password, rememberMe, shouldLockout: false);
            if (result==SignInStatus.Success)
            {
                var user = _userManager.FindByNameAsync(userName);
                var applicationUserViewModel = Mapper.Map<ApplicationUser_KH, ApplicationUserViewModel>(user.Result);
                return request.CreateResponse(HttpStatusCode.OK, applicationUserViewModel);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, result);

            }




        }
        [HttpPost]
        [Authorize]
        [Route("logout")]
        public HttpResponseMessage Logout(HttpRequestMessage request)
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie, DefaultAuthenticationTypes.ExternalBearer);
            authenticationManager.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
            return request.CreateResponse(HttpStatusCode.OK, new { success = true });
        }
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                //if my function is not found error it will print it as it's
                foreach (var msg in error.Split('.'))
                {
                    ModelState.AddModelError("", VietHoaLoi.TranslateIdentityResult(msg.TrimStart(' ')) ?? msg);
                }
            }
        }

      


        [Route("DoiMatKhau")]
        [HttpPost]

        public async Task<HttpResponseMessage> DoiMatKhau(HttpRequestMessage request, ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _userManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        var user = await _userManager.FindByIdAsync(User.Identity.GetUserId());
                        if (user != null)
                        {
                            await _signInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        }
                        return request.CreateResponse(HttpStatusCode.OK, model);
                    }

                    AddErrors(result);
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }


        }


        [Route("DatLaiMatKhau")]
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<HttpResponseMessage> DatLaiMatKhau(HttpRequestMessage request, ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _userManager.FindByIdAsync(model.Id);
                    string resetToken = await _userManager.GeneratePasswordResetTokenAsync(result.Id);
                    IdentityResult passwordChangeResult = await _userManager.ResetPasswordAsync(result.Id, resetToken, model.NewPassword);
                    if (passwordChangeResult.Succeeded)
                    {
                        var user = await _userManager.FindByIdAsync(model.Id);
                        if (user != null)
                        {
                            await _signInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        }
                        return request.CreateResponse(HttpStatusCode.OK, model);
                    }

                    AddErrors(passwordChangeResult);
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }


        }
        [HttpPost]
        [Route("confirm")]
        public HttpResponseMessage confirmAsync(HttpRequestMessage request, ConfirmViewModel confirmVm)
        {
            if (ModelState.IsValid)
            {
                string id = User.Identity.GetUserId();
                ApplicationUser_KH user = UserManager.FindById(id);
                if (!UserManager.CheckPassword(user, confirmVm.ConfirmPassword))
                {
                    ModelState.AddModelError("Password", "Incorrect password.");
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.OK);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

        }



        [HttpPost]
        [Route("dangky")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> dangkyAsync(HttpRequestMessage request, ApplicationUserViewModel registerVm)
        {
            if (ModelState.IsValid)
            {

                var AllKhachhang =  _khachhangservice.GetAll();
                var checkKH = AllKhachhang.Any(x => x.Email == registerVm.Email);
                if (checkKH)
                {
                    ModelState.AddModelError("email", "Email đã tồn tại");
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }


                
                var userByEmail = await _userManager.FindByEmailAsync(registerVm.Email);
                if (userByEmail != null)
                {
                    ModelState.AddModelError("email", "Email đã tồn tại");
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }

                //String content = HttpContext.Current.Server.MapPath("~/Assets/apphome/teample/Contextteample.html"); 
                //content = content.Replace("{{user}}", registerVm.Email);
                //content = content.Replace("{{Email}}", registerVm.Email);
                //content = content.Replace("{{pass}}", registerVm.Password);
                //MailHelper.SendMail(registerVm.Email, "Thông tin liên hệ", content);
               


                
                var user = new ApplicationUser_KH()
                {
                    UserName = registerVm.Email  ,
                    Email = registerVm.Email,
                    FullName = registerVm.FullName,
                    PhoneNumber = registerVm.PhoneNumber,
                };

                var result = await UserManager.CreateAsync(user, registerVm.Password);
                if (result.Succeeded)
                {
                    string laytatca = _khachhangservice.GetLastID().Split(new[] { "KH" }, StringSplitOptions.None)[1];
                    string str = "" + (Convert.ToInt32(laytatca) + 1);
                    string pad1 = "000000";
                    string MaCuoiCung = pad1.Substring(0, pad1.Length - str.Length) + str;
                    string mamoi = "KH" + MaCuoiCung;
                    KhachHangViewModel khachHang = new KhachHangViewModel();
                    KhachHang newKhachHang = new KhachHang();
                    khachHang.MaKhachHang = mamoi;
                    khachHang.DiaChi = registerVm.DiaChi;
                    khachHang.TenKhachHang = registerVm.FullName;
                    khachHang.SoDienThoai = registerVm.PhoneNumber;
                    khachHang.Email = registerVm.Email;
                    khachHang.PhanLoai = "Cá Nhân";
                    khachHang.MaQuan_Huyen = registerVm.MaQuan_Huyen;
                    khachHang.Tinh_TP = registerVm.Tinh_TP;

                    khachHang.NguoiTao = User.Identity.Name;
                    khachHang.NgayTao = DateTime.Now;
                    khachHang.DangHoatDong = true;
                    khachHang.SoNgayDuocNo = 0;
                    khachHang.SoNoToiDa = 0;
                    khachHang.SoLuongGiaoDich = 0;
                    khachHang.TongGiaoDich = 0;
                    khachHang.GiaTriGiaoDichLonNhat = 0;
                    khachHang.DaXoa = false; // mới tạo thì chưa được xóa

                    newKhachHang.UpdateKhachHang(khachHang);
                    _khachhangservice.Add(newKhachHang);
                    _khachhangservice.Save();

                    return request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    AddErrors(result);
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

        }


       


        [HttpPost, AllowAnonymous]
        [Route("ResetPassword")]
        public HttpResponseMessage ResetPassword(HttpRequestMessage request, RecoveryPassword recoveryPassword)
        {
            if (ModelState.IsValid)
            {
                string content = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Assets/teample/Contextteample.html"));
                var user = _userManager.FindByEmail(recoveryPassword.email);
                if (user != null)
                {
                    var resetToken = _userManager.GeneratePasswordResetToken(user.Id);
                   
                    recoveryPassword.url += "?userId=" + user.Id + "&code=" + resetToken;
                    content = content.Replace("{{user}}", user.FullName);
                    content = content.Replace("{{email}}", user.Email);
                    content = content.Replace("{{code}}", resetToken);
                    content = content.Replace("{{url}}", recoveryPassword.url);
                    //content = content.Replace("{{sdt}}", phanHoi.SoDienThoai);
                  var A= MailHelper.SendMail(recoveryPassword.email, "Thông tin liên hệ", content);
                    if (A != "ok")
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, A);
                       
                    }
                    else {
                        return request.CreateResponse(HttpStatusCode.OK, A);

                    }
                   
                }
                else {

                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Email chưa được đăng ký");
                }
                

               
                
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

        }
         [HttpPost]
        [Route("ConfirmResetPassword")]
        public async Task<HttpResponseMessage>  ConfirmResetPassword(HttpRequestMessage request, RecoveryPassword recoveryPassword)
        {
            if (ModelState.IsValid)
            {
                var result = await _userManager.ResetPasswordAsync(recoveryPassword.id, recoveryPassword.code, recoveryPassword.newpassword);
                if (result.Succeeded)
                {


                    return request.CreateResponse(HttpStatusCode.OK);
                }
                else {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

        


        
    }






    [Route("update")]
        [HttpPut]
        public async Task<HttpResponseMessage> updateAsync(HttpRequestMessage request, ApplicationUserViewModel applicationUserViewModel)

        {
            if (ModelState.IsValid)
            {
                var appUser = await _userManager.FindByIdAsync(applicationUserViewModel.Id);
                try
                {
                    applicationUserViewModel.BirthDay = DateTime.Now;
                    appUser.UpdateUser(applicationUserViewModel);
                    var result =await _userManager.UpdateAsync(appUser);
                    if (result.Succeeded)
                    {
                       
                       
                        var AllKhachhang = _khachhangservice.GetByEmail(applicationUserViewModel.Email);
                        if (AllKhachhang!=null) {

                            AllKhachhang.TenKhachHang = applicationUserViewModel.FullName;
                            AllKhachhang.DiaChi = applicationUserViewModel.Address;
                            _khachhangservice.Update(AllKhachhang);
                            _khachhangservice.Save();
                        }



                        return request.CreateResponse(HttpStatusCode.OK);

                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(",", result.Errors));
                }
                catch (NameDuplicatedException dex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }
    }
}
