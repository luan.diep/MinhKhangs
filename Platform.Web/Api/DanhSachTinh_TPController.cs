﻿using AutoMapper;
using Newtonsoft.Json.Linq;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;

namespace Platform.Web.Api
{
    [RoutePrefix("api/danhsachtinh_tp")]
  
    public class DanhSachTinh_TPController : ApiControllerBase
    {
        IDanhSachTinh_TPService _danhSachTinh_TPService;

        public DanhSachTinh_TPController(ILoiService loiService, IDanhSachTinh_TPService danhSachTinh_TPService) : base(loiService)
        {
            this._danhSachTinh_TPService = danhSachTinh_TPService;
        }
      
       
		 
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _danhSachTinh_TPService.GetAll().Where(x=>x.MaTinh_TP!="00");

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }



		
    }
}
