﻿using AutoMapper;
using Newtonsoft.Json.Linq;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;
using Platform.Web.App_Start;
using Microsoft.AspNet.Identity;

namespace Platform.Web.Api
{
    [RoutePrefix("api/chitietchungtudathang")]

    public class ChiTietChungTuDatHangController : ApiControllerBase
    {
        IChiTietChungTuDatHangService _chiTietChungTuDatHangService;
        IChungTuDatHangService _chungTuDatHangService;
        IKhachHangService _khachHangService;
        ITrangThaiGiaoHangService _trangThaiGiaoHangService;
        private ApplicationUserManager _userManager;
        public ChiTietChungTuDatHangController(ILoiService loiService, ITrangThaiGiaoHangService trangThaiGiaoHangService, IChiTietChungTuDatHangService chiTietChungTuDatHangService, ApplicationUserManager userManager, IChungTuDatHangService chungTuDatHangService, IKhachHangService khachHangService) : base(loiService)
        {
            this._chiTietChungTuDatHangService = chiTietChungTuDatHangService;
            this._chungTuDatHangService = chungTuDatHangService;
            this._khachHangService = khachHangService;
            this._userManager = userManager;
            this._trangThaiGiaoHangService = trangThaiGiaoHangService;
        }
        

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage update(HttpRequestMessage request, ChiTietChungTuDatHangViewModel chiTietChungTuDatHangViewModel)

        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                   
                    if (chiTietChungTuDatHangViewModel.KhuyenMai == null)
                    {
                        var id = _chiTietChungTuDatHangService.getbyIdKhongCoMaGoc(chiTietChungTuDatHangViewModel.MaHang, chiTietChungTuDatHangViewModel.MaChungTuDatHang);
                        chiTietChungTuDatHangViewModel.MaChiTietChungTuDatHang = id.MaChiTietChungTuDatHang;
                        var vienchucDb = _chiTietChungTuDatHangService.GetByID(chiTietChungTuDatHangViewModel.MaChiTietChungTuDatHang);

                        vienchucDb.UpdateChiTietChungTuDatHang(chiTietChungTuDatHangViewModel);
                        _chiTietChungTuDatHangService.Update(vienchucDb);
                        _chiTietChungTuDatHangService.Commit();
                    }
                    else if (chiTietChungTuDatHangViewModel.KhuyenMai != null) {
                        var id = _chiTietChungTuDatHangService.getbyId(chiTietChungTuDatHangViewModel.MaHang, chiTietChungTuDatHangViewModel.MaChungTuDatHang);
                        chiTietChungTuDatHangViewModel.MaChiTietChungTuDatHang = id.MaChiTietChungTuDatHang;
                        var vienchucDb = _chiTietChungTuDatHangService.GetByID(chiTietChungTuDatHangViewModel.MaChiTietChungTuDatHang);

                        vienchucDb.UpdateChiTietChungTuDatHang(chiTietChungTuDatHangViewModel);
                        _chiTietChungTuDatHangService.Update(vienchucDb);
                        _chiTietChungTuDatHangService.Commit();

                    }
                   

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
        



        [Route("delete")]
        [HttpDelete]
        public HttpResponseMessage Delete(HttpRequestMessage request, string mahang,string mact)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var List = _chiTietChungTuDatHangService.Listcart(User.Identity.GetUserName()).ToList();
                    var count = false;
                    if (List.Count() == 1)
                    {
                        var id = _chiTietChungTuDatHangService.ID(mact, mahang);
                        _chiTietChungTuDatHangService.delete(id.MaChiTietChungTuDatHang);
                        _chiTietChungTuDatHangService.Commit();
                        var idtrangthai =_trangThaiGiaoHangService.ID(mact);
                        _trangThaiGiaoHangService.delete(idtrangthai.MaChiTietTrangThai);
                        _trangThaiGiaoHangService.Commit();
                        var chungtu = _chungTuDatHangService.GetByID(mact);
                        _chungTuDatHangService.delete(chungtu.MaChungTuDatHang);
                        _chungTuDatHangService.Commit();
                        count = true;

                    }
                    else {
                    var id = _chiTietChungTuDatHangService.ID(mact, mahang);
                    _chiTietChungTuDatHangService.delete(id.MaChiTietChungTuDatHang);
                    _chiTietChungTuDatHangService.Commit();
                    
                    var chungtu = _chungTuDatHangService.GetByID(mact);
                    var chungtuviewmodel = new ChungTuDatHangViewModel();
                    chungtuviewmodel = Mapper.Map<ChungTuDatHang, ChungTuDatHangViewModel>(chungtu);
                    chungtuviewmodel.TongTienHang -= id.ThanhTien;
                    chungtuviewmodel.TongTienThanhToan -= id.ThanhTien;
                    chungtu.UpdateChungTuDatHang(chungtuviewmodel);
                    _chungTuDatHangService.Update(chungtu);
                    _chungTuDatHangService.Commit();
                    }
                    response = request.CreateResponse(HttpStatusCode.OK,count);
                }
                return response;
            });
        }



        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request,ChiTietChungTuDatHangViewModel chiTietChungTuDatHangVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                   
                        var newThongBao = new ChiTietChungTuDatHang();
                   
                        newThongBao.UpdateChiTietChungTuDatHang(chiTietChungTuDatHangVM);
                        
                        _chiTietChungTuDatHangService.Add(newThongBao);
                        _chiTietChungTuDatHangService.Save();
                    
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });

        }

        [Route("checktrung")]
        [HttpPost]
        public HttpResponseMessage checktrung(HttpRequestMessage request, Cart_product CartClient)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var chitiet = _chiTietChungTuDatHangService.ID(CartClient.MaChungTuDatHang, CartClient.MaHang);
                    var chitietview = new ChiTietChungTuDatHangViewModel();
                    chitietview = Mapper.Map<ChiTietChungTuDatHang, ChiTietChungTuDatHangViewModel>(chitiet);

                    var chungtu = _chungTuDatHangService.GetByID(CartClient.MaChungTuDatHang);
                    var chungtuviewmodel = new ChungTuDatHangViewModel();
                    chungtuviewmodel = Mapper.Map<ChungTuDatHang, ChungTuDatHangViewModel>(chungtu);
                    chungtuviewmodel.TongTienHang -= chitietview.ThanhTien;
                    chungtuviewmodel.TongTienThanhToan -= chitietview.ThanhTien;

                    chitietview.SoLuong = CartClient.SoLuong;
                    chitietview.ThanhTien = CartClient.ThanhTien;
                    chitiet.UpdateChiTietChungTuDatHang(chitietview);
                    _chiTietChungTuDatHangService.Update(chitiet);
                    _chiTietChungTuDatHangService.Commit();

                  
                    
                    chungtuviewmodel.TongTienHang += chitietview.ThanhTien;
                    chungtuviewmodel.TongTienThanhToan += chitietview.ThanhTien;
                    chungtu.UpdateChungTuDatHang(chungtuviewmodel);
                    _chungTuDatHangService.Update(chungtu);
                    _chungTuDatHangService.Commit();
                    var List1 = _chiTietChungTuDatHangService.Listcart(User.Identity.GetUserName()).Where(x => x.MaHang == CartClient.MaHang).FirstOrDefault();
                    response = request.CreateResponse(HttpStatusCode.Created, List1);


                }
                return response;
            });
        }
        [Route("checkkotrung")]
        [HttpPost]
        public HttpResponseMessage checkkotrung(HttpRequestMessage request, Cart_product CartClient)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var List = _chiTietChungTuDatHangService.Listcart(User.Identity.GetUserName()).ToList();
                    if (List.Count() == 0)
                    {
                        var chungtu = new ChungTuDatHang();
                        var chungtuviewmodel = new ChungTuDatHangViewModel();
                        var trangthaigiaohang = new TrangThaiGiaoHang();
                        var trangthaigiaohangviewmodel = new TrangThaiGiaoHangViewModel();
                        var maKH = _khachHangService.getmaKH(User.Identity.Name);
                        var maCTnew = _chungTuDatHangService.GetNewID();
                        chungtuviewmodel.MaChungTuDatHang = maCTnew;
                        chungtuviewmodel.MaKhachHang = maKH;
                        chungtuviewmodel.NgayChungTu = DateTime.Now;
                        chungtuviewmodel.TongTienHang = CartClient.ThanhTien;
                        chungtuviewmodel.TongTienThanhToan = CartClient.ThanhTien;
                        chungtuviewmodel.MaTinhTrang = 1;
                        chungtuviewmodel.MaSoNhanVien = "0000";
                        chungtuviewmodel.MaKenhBanHang = "04";
                        chungtuviewmodel.DaThanhToan = false;
                        chungtuviewmodel.IsViewed = true;
                        chungtuviewmodel.DaThayDoi = false;
                        chungtuviewmodel.DaGhiSo = true;
                        chungtuviewmodel.SoTienDaTra = 0;
                        chungtuviewmodel.TienChietKhau = 0;
                        chungtuviewmodel.TienThueGTGT = 0;
                        chungtuviewmodel.NgayHoachToan = DateTime.Now;
                        
                        

                        chungtu.UpdateChungTuDatHang(chungtuviewmodel);
                        _chungTuDatHangService.Add(chungtu);
                        _chungTuDatHangService.Save();
                        trangthaigiaohangviewmodel.MaChungTuDatHang = maCTnew;
                        trangthaigiaohangviewmodel.MaTrangThai = 1;
                        trangthaigiaohangviewmodel.NgayGio = DateTime.Now;
                        trangthaigiaohangviewmodel.MaNhanVienTiepNhan = "0000";
                        trangthaigiaohangviewmodel.ChoHienThi =true;
                        trangthaigiaohang.UpdateTrangThaiGiaoHang(trangthaigiaohangviewmodel);
                        _trangThaiGiaoHangService.Add(trangthaigiaohang);
                        _trangThaiGiaoHangService.Save();
                        var chitiet = new ChiTietChungTuDatHang();
                        var Map = Mapper.Map<Cart_product, ChiTietChungTuDatHangViewModel>(CartClient);
                        Map.MaChungTuDatHang = maCTnew;
                        chitiet.UpdateChiTietChungTuDatHang(Map);
                        _chiTietChungTuDatHangService.Add(chitiet);
                        _chiTietChungTuDatHangService.Save();
                        var List1 = _chiTietChungTuDatHangService.Listcart(User.Identity.GetUserName()).Where(x => x.MaHang == CartClient.MaHang).FirstOrDefault();
                        response = request.CreateResponse(HttpStatusCode.Created, List1);

                    }
                    else { 
                    var chitiet = new ChiTietChungTuDatHang();
                    var Map = Mapper.Map<Cart_product, ChiTietChungTuDatHangViewModel>(CartClient);
                    Map.MaChungTuDatHang = List[0].MaChungTuDatHang;
                    chitiet.UpdateChiTietChungTuDatHang(Map);
                    _chiTietChungTuDatHangService.Add(chitiet);
                    _chiTietChungTuDatHangService.Save();

                    var chungtu = _chungTuDatHangService.GetByID(List[0].MaChungTuDatHang);
                    var chungtuviewmodel = new ChungTuDatHangViewModel();
                    chungtuviewmodel = Mapper.Map<ChungTuDatHang, ChungTuDatHangViewModel>(chungtu);
                    chungtuviewmodel.TongTienHang += CartClient.ThanhTien;
                    chungtuviewmodel.TongTienThanhToan += CartClient.ThanhTien;
                    chungtu.UpdateChungTuDatHang(chungtuviewmodel);
                    _chungTuDatHangService.Update(chungtu);
                    _chungTuDatHangService.Save();
                    var List1 = _chiTietChungTuDatHangService.Listcart(User.Identity.GetUserName()).Where(x=>x.MaHang== CartClient.MaHang).FirstOrDefault();
                    response = request.CreateResponse(HttpStatusCode.Created, List1);
                    }
                }
                return response;
            });
        }






        [Route("Listcart")]
        [HttpPost]
        public HttpResponseMessage Listcart(HttpRequestMessage request, IEnumerable<Cart_product> CartClient)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var List = _chiTietChungTuDatHangService.Listcart(User.Identity.GetUserName()).ToList();
                    if (CartClient.Count() == 0)
                    {
                       return response = request.CreateResponse(HttpStatusCode.Created, List);
                    }
                    else
                    {
                        if (List.Count() == 0)
                        {

                            var chungtu = new ChungTuDatHang();
                            var chungtuviewmodel = new ChungTuDatHangViewModel();
                            var trangthaigiaohang = new TrangThaiGiaoHang();
                            var trangthaigiaohangviewmodel = new TrangThaiGiaoHangViewModel();
                            var maKH = _khachHangService.getmaKH(User.Identity.Name);
                            var maCTnew = _chungTuDatHangService.GetNewID();
                            chungtuviewmodel.MaChungTuDatHang = maCTnew;
                            chungtuviewmodel.MaKhachHang = maKH;
                            chungtuviewmodel.NgayChungTu = DateTime.Now;
                            chungtuviewmodel.MaTinhTrang = 1;
                            chungtuviewmodel.MaSoNhanVien = "0000";
                            chungtuviewmodel.MaKenhBanHang = "04";
                            chungtuviewmodel.DaThanhToan = false;
                            chungtuviewmodel.IsViewed = true;
                            chungtuviewmodel.DaThayDoi = false;
                            chungtuviewmodel.DaGhiSo = true;
                            chungtuviewmodel.SoTienDaTra = 0;
                            chungtuviewmodel.TienChietKhau = 0;
                            chungtuviewmodel.TienThueGTGT = 0;
                            chungtuviewmodel.NgayHoachToan = DateTime.Now;
                            chungtuviewmodel.TongTienHang = CartClient.Sum(x=>x.ThanhTien);
                            chungtuviewmodel.TongTienThanhToan = CartClient.Sum(x => x.ThanhTien);
                            chungtu.UpdateChungTuDatHang(chungtuviewmodel);
                            _chungTuDatHangService.Add(chungtu);
                            _chungTuDatHangService.Save();
                            trangthaigiaohangviewmodel.MaChungTuDatHang = maCTnew;
                            trangthaigiaohangviewmodel.MaTrangThai = 1;
                            trangthaigiaohangviewmodel.NgayGio = DateTime.Now;
                            trangthaigiaohangviewmodel.MaNhanVienTiepNhan = "0000";
                            trangthaigiaohangviewmodel.ChoHienThi = true;
                            trangthaigiaohang.UpdateTrangThaiGiaoHang(trangthaigiaohangviewmodel);
                            _trangThaiGiaoHangService.Add(trangthaigiaohang);
                            _trangThaiGiaoHangService.Save();

                            foreach (var item in CartClient)
                            {
                                var chitiet = new ChiTietChungTuDatHang();
                                var Map = Mapper.Map<Cart_product, ChiTietChungTuDatHangViewModel>(item);
                                Map.MaChungTuDatHang = maCTnew;
                                chitiet.UpdateChiTietChungTuDatHang(Map);
                                _chiTietChungTuDatHangService.Add(chitiet);
                                _chiTietChungTuDatHangService.Save();
                            }

                            var List1 = _chiTietChungTuDatHangService.Listcart(User.Identity.Name).ToList();
                            response = request.CreateResponse(HttpStatusCode.Created, List1);
                        }
                        else {

                          

                            foreach (var cartnew in CartClient)
                            {
                                var index = List.FindIndex(x => x.MaHang == cartnew.MaHang);
                                if (index != -1)
                                {
                                    
                                    var chitiet = _chiTietChungTuDatHangService.ID(List[index].MaChungTuDatHang, List[index].MaHang);
                                    var chitietview = new ChiTietChungTuDatHangViewModel();
                                    chitietview = Mapper.Map<ChiTietChungTuDatHang, ChiTietChungTuDatHangViewModel>(chitiet);
                                    chitietview.SoLuong += cartnew.SoLuong;
                                    chitietview.ThanhTien += cartnew.ThanhTien;
                                    chitiet.UpdateChiTietChungTuDatHang(chitietview);
                                    _chiTietChungTuDatHangService.Update(chitiet);
                                    _chiTietChungTuDatHangService.Commit();
                                    var chungtu = _chungTuDatHangService.GetByID(List[index].MaChungTuDatHang);
                                    var chungtuviewmodel = new ChungTuDatHangViewModel();
                                    chungtuviewmodel = Mapper.Map<ChungTuDatHang, ChungTuDatHangViewModel>(chungtu);
                                    chungtuviewmodel.TongTienHang += cartnew.ThanhTien;
                                    chungtuviewmodel.TongTienThanhToan += cartnew.ThanhTien;
                                    chungtu.UpdateChungTuDatHang(chungtuviewmodel);
                                    _chungTuDatHangService.Update(chungtu);
                                    _chungTuDatHangService.Commit();


                                }
                                else {

                                    var chitiet = new ChiTietChungTuDatHang();
                                    var Map = Mapper.Map<Cart_product, ChiTietChungTuDatHangViewModel>(cartnew);
                                    Map.MaChungTuDatHang = List[0].MaChungTuDatHang;
                                    chitiet.UpdateChiTietChungTuDatHang(Map);
                                    _chiTietChungTuDatHangService.Add(chitiet);
                                    _chiTietChungTuDatHangService.Save();

                                    var chungtu = _chungTuDatHangService.GetByID(List[0].MaChungTuDatHang);
                                    var chungtuviewmodel = new ChungTuDatHangViewModel();
                                    chungtuviewmodel = Mapper.Map<ChungTuDatHang, ChungTuDatHangViewModel>(chungtu);
                                    chungtuviewmodel.TongTienHang += cartnew.ThanhTien;
                                    chungtuviewmodel.TongTienThanhToan += cartnew.ThanhTien;
                                    chungtu.UpdateChungTuDatHang(chungtuviewmodel);
                                    _chungTuDatHangService.Update(chungtu);
                                    _chungTuDatHangService.Save();



                                }


                            }

                            var List1 = _chiTietChungTuDatHangService.Listcart(User.Identity.Name).ToList();
                            response = request.CreateResponse(HttpStatusCode.Created, List1);


                        }

                    }



                    
                   


                }
                return response;
            });
        }




        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _chiTietChungTuDatHangService.GetAll();
                //  var responseData = Mapper.Map<IEnumerable<ChiTietChungTuDatHang>,IEnumerable<ChiTietChungTuDatHangViewModel>>(listCategory);
               // var b = listCategory.OrderBy(x => x.MaChiTietChungTuDatHang.Length + x.MaChiTietChungTuDatHang);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

         [Route("laygiohang")]
         [HttpGet]
        public HttpResponseMessage laygiohang(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var listcart = _chiTietChungTuDatHangService.Listcart(User.Identity.Name).ToList();
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listcart);
                return response;
            });
        }







        [Route("getbyMaHang")]
        public HttpResponseMessage getbyMaHang(HttpRequestMessage request,string mahang,string mact)
        {
            return CreateHttpResponse(request, () =>
            {

                var listCategory = _chiTietChungTuDatHangService.GetByChiTietMaHang(mahang, mact);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }






        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage getbyid(HttpRequestMessage request,int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _chiTietChungTuDatHangService.GetByID(id);
               HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }



        [Route("getbyemail")]
        [HttpGet]
        public HttpResponseMessage getbyemail(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var iduser = User.Identity.GetUserName();
                Console.WriteLine(iduser);
                var listCategory = _chiTietChungTuDatHangService.getchitietchungtudathangbyemail(iduser);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }


        /******lấy danh sách hàng hóa của chứng từ đặt hàng đó********/
        [Route("getchitietchungtudathang")]
        [HttpGet]
        public HttpResponseMessage getchitietchungtudathang(HttpRequestMessage request,string Id)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _chiTietChungTuDatHangService.getchitietchungtudathang(Id);
               HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }
        

      

        [Route("getalll")]
        [HttpGet]
        public HttpResponseMessage getall(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _chiTietChungTuDatHangService.GetAll();
                
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }
        [Route("capnhatsaukhixacnhan")]
        [HttpPut]
        public HttpResponseMessage capnhatsaukhixacnhan(HttpRequestMessage request, IEnumerable<ChiTietChungTuDatHangViewModel> chiTietChungTuDatHangViewModel)

        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var chitietdaco = _chiTietChungTuDatHangService.GetByMaCTDH(chiTietChungTuDatHangViewModel.FirstOrDefault().MaChungTuDatHang);
                    var map = Mapper.Map<IEnumerable<ChiTietChungTuDatHang>, IEnumerable<ChiTietChungTuDatHangViewModel>>(chitietdaco).ToList();
                    foreach (var item in map)
                    {
                        var checkdaxoa = chiTietChungTuDatHangViewModel.ToList().FindIndex(x => x.MaChiTietChungTuDatHang == item.MaChiTietChungTuDatHang);
                        if (checkdaxoa == -1)
                        {
                            _chiTietChungTuDatHangService.delete(item.MaChiTietChungTuDatHang);
                            _chiTietChungTuDatHangService.Commit();
                        }
                    }
                    foreach (var item in chiTietChungTuDatHangViewModel)
                    {
                        var checkdaco = map.FindIndex(x => x.MaChiTietChungTuDatHang == item.MaChiTietChungTuDatHang);
                        if (checkdaco == -1)
                        {
                            var newPhongHoc = new ChiTietChungTuDatHang();
                            newPhongHoc.UpdateChiTietChungTuDatHang(item);
                            _chiTietChungTuDatHangService.Add(newPhongHoc);
                        }
                        else
                        {
                            var tim = _chiTietChungTuDatHangService.GetByID(item.MaChiTietChungTuDatHang);
                            tim.UpdateChiTietChungTuDatHang(item);
                            _chiTietChungTuDatHangService.Update(tim);
                        }

                        _chiTietChungTuDatHangService.Commit();
                    }
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });
        }


    }
}
