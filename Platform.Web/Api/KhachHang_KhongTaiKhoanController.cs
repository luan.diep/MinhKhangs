﻿using AutoMapper;
using Newtonsoft.Json.Linq;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;
using System.Web;
using System.Threading.Tasks;
using System.IO;
using Platform.Common;
using Microsoft.Build.Tasks;
using FluentDate;

namespace Platform.Web.Api
{
    [RoutePrefix("api/khachhangkhongtaikhoan")]
  
    public class KhachHang_KhongTaiKhoanController : ApiControllerBase
    {
        IKhachHang_KhongTaiKhoanService _khachHangService;
        IChiTietChungTuDatHangService _chiTietChungTuDatHangService;
        IChungTuDatHangService _chungTuDatHangService;
        ITrangThaiGiaoHangService _trangThaiGiaoHangService;
        public KhachHang_KhongTaiKhoanController(ILoiService loiService, IChungTuDatHangService chungTuDatHangService, IChiTietChungTuDatHangService chiTietChungTuDatHangService, ITrangThaiGiaoHangService trangThaiGiaoHangService, IKhachHang_KhongTaiKhoanService khachHangService ) : base(loiService)
        {
            this._khachHangService = khachHangService;
            this._chungTuDatHangService = chungTuDatHangService;
            this._chiTietChungTuDatHangService = chiTietChungTuDatHangService;
            this._trangThaiGiaoHangService = trangThaiGiaoHangService;
        }


        

        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, CreateKHModel khachHang)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    //thêm khách hàng
                    string laytatca = _khachHangService.GetLastID().Split(new[] { "KTK" }, StringSplitOptions.None)[1];
                    string str = "" + (Convert.ToInt32(laytatca) + 1);
                    string pad1 = "00000";
                    string MaCuoiCung = pad1.Substring(0, pad1.Length - str.Length) + str;
                    string mamoi = "KTK" + MaCuoiCung;
                    KhachHang_KhongTaiKhoan newKhachHang = new KhachHang_KhongTaiKhoan();
                    KhachHang_KhongTaiKhoanViewModel newKhachHangs = new KhachHang_KhongTaiKhoanViewModel();

                    newKhachHangs.MaKhachHang = mamoi;
                    newKhachHangs.TenKhachHang = khachHang.TenKhachHang;
                    newKhachHangs.DiaChi = khachHang.DiaChi;
                    newKhachHangs.SoDienThoai = khachHang.SoDienThoai;
                    //newKhachHangs.Tinh_TP = khachHang.Tinh_TP;
                    //newKhachHangs.MaQuan_Huyen = khachHang.MaQuan_Huyen;
                   
                    newKhachHang.UpdateKhachHang_KhongTaiKhoan(newKhachHangs);
                    _khachHangService.Add(newKhachHang);
                    _khachHangService.Save();
                    //thêm chứng từ đặt hàng
                    ChungTuDatHang CTDHModel = new ChungTuDatHang();
                    ChungTuDatHangViewModel CTDHViewmodel = new ChungTuDatHangViewModel();
                    string laytatca1 = _chungTuDatHangService.GetNewID();
                    string mamoi1 = laytatca1;
                    CTDHViewmodel.MaChungTuDatHang = mamoi1;
                    CTDHViewmodel.MaKhachHang = mamoi;
                    CTDHViewmodel.NgayHoachToan = DateTime.Now;
                    CTDHViewmodel.NgayChungTu = DateTime.Now;
                    CTDHViewmodel.TongTienHang = khachHang.chitiet.Sum(x=>x.ThanhTien);
                    CTDHViewmodel.TongTienThanhToan = khachHang.chitiet.Sum(x => x.ThanhTien);
                    CTDHViewmodel.DaThanhToan = false;
                    CTDHViewmodel.MaHinhThucThanhToan = 4;
                    CTDHViewmodel.MaKenhBanHang = "04";
                    CTDHViewmodel.MaSoNhanVien = "0000";
                    CTDHViewmodel.SoTienDaTra = 0;
                    CTDHViewmodel.DaGhiSo = true;
                    CTDHViewmodel.DaThayDoi = false;
                    CTDHViewmodel.TienThueGTGT = khachHang.chitiet.Sum(x => x.TienThueGTGT);
                    CTDHViewmodel.TienChietKhau = khachHang.chitiet.Sum(x => x.TienChietKhau);
                    //CTDHViewmodel.MaHinhThucThanhToan = 4;
                    CTDHViewmodel.MaTinhTrang = 2;
                    CTDHViewmodel.ThoiGianMuonNhanHang = DateTime.Now + 1.Days();
                    CTDHViewmodel.IsViewed = false;
                    CTDHModel.UpdateChungTuDatHang(CTDHViewmodel);
                    _chungTuDatHangService.Add(CTDHModel);
                    _chungTuDatHangService.Save();
                 

                    //thêm chi tiết
                   

                    var responsechitiet = Mapper.Map<IEnumerable<ChiTietChungTuDatHang>, IEnumerable<ChiTietChungTuDatHangViewModel>>(khachHang.chitiet);

                    foreach (var item in responsechitiet)
                    {

                        ChiTietChungTuDatHang CTCTDH = new ChiTietChungTuDatHang();
                        item.MaChungTuDatHang = mamoi1;
                        CTCTDH.UpdateChiTietChungTuDatHang(item);
                        CTCTDH.GiaKhuyenMai = item.GiaKhuyenMai * item.SoLuong;
                        _chiTietChungTuDatHangService.Add(CTCTDH);
                        _chiTietChungTuDatHangService.Save();
                       
                    }

                    //thệm trạng thái
                    var newquaTrinhDaoTao = new TrangThaiGiaoHang();
                    var TrangThaiViewModel = new TrangThaiGiaoHangViewModel();
                    TrangThaiViewModel.MaChungTuDatHang = mamoi1;
                    TrangThaiViewModel.MaTrangThai = 2;
                    TrangThaiViewModel.MaNhanVienTiepNhan = "0000";
                    TrangThaiViewModel.ChoHienThi = true;
                    TrangThaiViewModel.NgayGio = DateTime.Now;
                    newquaTrinhDaoTao.UpdateTrangThaiGiaoHang(TrangThaiViewModel);
                    _trangThaiGiaoHangService.Add(newquaTrinhDaoTao);
                    _trangThaiGiaoHangService.Save();
                   
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    dict.Add("MaChungTuMoi", mamoi1);
                    dict.Add("TongTienThanhToan" , Convert.ToString(CTDHViewmodel.TongTienThanhToan));
                    dict.Add("ThoiGianMuonNhanHang" ,Convert.ToString(CTDHViewmodel.ThoiGianMuonNhanHang) );
                    dict.Add("HinhThucThanhToan", _chungTuDatHangService.LayTenHinhThucThanhToan(CTDHViewmodel.MaHinhThucThanhToan.Value));
                    dict.Add("TenKhachHang", khachHang.TenKhachHang);
                    dict.Add("SoDienThoai", khachHang.SoDienThoai);
                    Hashtable ht = new Hashtable(dict);
                    response = request.CreateResponse(HttpStatusCode.OK, ht);
                }
                return response;
            });

        }

        [Route("delete")]
        [HttpDelete]
        [HttpGet]
        // [AllowAnonymous]
        public HttpResponseMessage Delete(HttpRequestMessage request, string ID)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldProductCategory = _khachHangService.DELETE(int.Parse(ID));
                    _khachHangService.Save();

                    var responseData = Mapper.Map<KhachHang_KhongTaiKhoan, KhachHang_KhongTaiKhoanViewModel>(oldProductCategory);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

       

        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _khachHangService.GetAll();
                var b = listCategory.OrderBy(x => x.MaKhachHang);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, b);


                return response;
            });
        }






        [Route("getbyid")]
        public HttpResponseMessage GetById(HttpRequestMessage request, string id)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _khachHangService.GetByID(id);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

     

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage update(HttpRequestMessage request, KhachHang_KhongTaiKhoanViewModel khachHangVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var vienchucDb = _khachHangService.GetByID(khachHangVM.MaKhachHang);

                    vienchucDb.UpdateKhachHang_KhongTaiKhoan(khachHangVM);
                    _khachHangService.Update(vienchucDb);
                    _khachHangService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }


    
       


      

       
    }
}
