﻿using AutoMapper;
using Newtonsoft.Json.Linq;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;

namespace Platform.Web.Api
{
    [RoutePrefix("api/thanhpham")]
    [Authorize]
    public class ThanhPhamController : ApiControllerBase
    {
        IThanhPhamService _thanhPhamService;

        public ThanhPhamController(ILoiService loiService, IThanhPhamService thanhPhamService) : base(loiService)
        {
            this._thanhPhamService = thanhPhamService;
        }
        
        [Route("getbymahang")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage getbymahang(HttpRequestMessage request,string mahang)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _thanhPhamService.getbymahang(mahang);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }


    }
}
