﻿
using AutoMapper;
using Newtonsoft.Json.Linq;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;

namespace Platform.Web.Api
{
    [RoutePrefix("api/hanghoayeuthich")]

    public class HangHoaYeuThichController : ApiControllerBase
    {
        IHangHoaYeuThichService _hangHoaYeuThichService;
        IKhachHangService _khachHangService;
        

        public HangHoaYeuThichController(ILoiService loiService, IHangHoaYeuThichService hangHoaYeuThichService, IKhachHangService khachHangService) : base(loiService)
        {
            this._hangHoaYeuThichService = hangHoaYeuThichService;
            this._khachHangService = khachHangService;
           
        }



        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var listCategory = _hangHoaYeuThichService.GetAll();
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);
                return response;
            });
        }

        [Route("createHeart")]
        public HttpResponseMessage createHeart(HttpRequestMessage request,HangHoaYeuThich hangHoaYeuThich)
        {
            return CreateHttpResponse(request, () =>
            {

                var checkmahang = _hangHoaYeuThichService.getmahang(hangHoaYeuThich.MaHang);
                if (checkmahang!=null)
                {


                    var Map = Mapper.Map<HangHoaYeuThich, HangHoaYeuThichViewModel>(checkmahang);
                    Map.DaXoa = hangHoaYeuThich.DaXoa;
                    checkmahang.UpdateHangHoaYeuThich(Map);
                    _hangHoaYeuThichService.Update(checkmahang);
                    _hangHoaYeuThichService.Commit();






                }
                else {
                    var maKH = _khachHangService.getmaKH(User.Identity.Name);
                    var newheart = new HangHoaYeuThich();
                    var newheartviewmodel = new HangHoaYeuThichViewModel();
                    newheartviewmodel.MaKhachHang = maKH;
                    newheartviewmodel.MaHang = hangHoaYeuThich.MaHang;
                    newheartviewmodel.NgayGio = DateTime.Now;
                    newheartviewmodel.DaXoa = hangHoaYeuThich.DaXoa;

                    newheart.UpdateHangHoaYeuThich(newheartviewmodel);
                    _hangHoaYeuThichService.Add(newheart);
                    _hangHoaYeuThichService.Commit();


                }

               


               
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK,true);
                return response;
            });
        }

      

    }
}
