﻿
(function (app) {
    'use strict';

    app.service('loginService', ['$http', '$q', 'authenticationService', 'authData', 'apiService', '$interval', 'localStorageService',
        function ($http, $q, authenticationService, authData, apiService, $interval, localStorageService) {
            var userInfo;
            var deferred;
            this.login = function (userName, password) {
                deferred = $q.defer();
                var data = "grant_type=password&username=" + userName + "&password=" + password;
                $http.post('/oauth/token', data, {
                    headers:
                        { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).then(function (response) {
                    var config = {
                        params: {
                            username: response.data.username
                        }
                    };
                    apiService.get('/api/applicationuser/getbyname/', config, function (res) {
                        var date = moment().add(response.data.expires_in, 'seconds').format('DD/MM/YYYY HH:mm:ss')
                        //var date = moment().format(' ') + time

                        userInfo = {
                            accessToken: response.data.access_token,
                            userName: res.data.UserName,
                            fullName: res.data.FullName,
                            email: res.data.Email,
                            refresh_token: response.data.refresh_token,
                            expires_in: date,
                        };

                        authenticationService.setTokenInfo(userInfo);
                        authData.authenticationData.IsAuthenticated = true;
                        authData.authenticationData.accessToken = userInfo.accessToken;
                        authData.authenticationData.userName = userInfo.userName;
                        authData.authenticationData.fullName = userInfo.fullName;
                        authData.authenticationData.email = userInfo.email;
                        authData.authenticationData.refresh_token = userInfo.refresh_token;
                        authData.authenticationData.expires_in = userInfo.expires_in;
                        deferred.resolve(null);

                    }, null);

                }, function (err, status) {
                    initialValue();
                    deferred.resolve(err);
                })
                return deferred.promise;
            };



            this.refreshToken = function () {
                var loginServiceURL = '/oauth/token';
                var deferred = $q.defer();
                var token = localStorageService.get("TokenInfo");
                token = JSON.parse(token);
                var data = "grant_type=refresh_token&refresh_token=" + token.refresh_token + "&client_id=" + token.userName;

                $http.post(loginServiceURL, data).then(function (response) {
                    var config = {
                        params: {
                            username: token.userName
                        }
                    };
                    apiService.get('/api/applicationuser/getbyname/', config, function (res) {
                        var time = moment().add(response.data.expires_in, 'seconds').format('HH:mm:ss')
                        var date = moment().format('DD/MM/yyyy ') + time
                        userInfo = {
                            accessToken: response.data.access_token,
                            userName: res.data.UserName,
                            fullName: res.data.FullName,
                            email: res.data.Email,
                            refresh_token: response.data.refresh_token,
                            expires_in: date,
                        };

                        authenticationService.setTokenInfo(userInfo);
                        authData.authenticationData.IsAuthenticated = true;
                        authData.authenticationData.accessToken = userInfo.accessToken;
                        authData.authenticationData.userName = userInfo.userName;
                        authData.authenticationData.fullName = userInfo.fullName;
                        authData.authenticationData.email = userInfo.email;
                        authData.authenticationData.refresh_token = userInfo.refresh_token;
                        authData.authenticationData.expires_in = userInfo.expires_in;
                        deferred.resolve(null);

                    }, null);

                }, function (err, status) {
                    initialValue();
                    deferred.resolve(err);
                })
                return deferred.promise;
            }

            var deferedRefreshAccessToken = null;

            this.refreshAccessToken = function () {

                //If already waiting for the Promise, return it.
                if (deferedRefreshAccessToken) {
                    return deferedRefreshAccessToken.promise
                }
                else {
                    deferedRefreshAccessToken = $q.defer();
                    //Get $http service with $injector to avoid circular dependency
                    var loginServiceURL = '/oauth/token';
                    var token = localStorageService.get("TokenInfo");
                    token = JSON.parse(token);
                    var data = "grant_type=refresh_token&refresh_token=" + token.refresh_token + "&client_id=" + token.userName;
                    $http.post(loginServiceURL, data).then(function (response) {
                        var config = {
                            params: {
                                username: token.userName
                            }
                        };
                        apiService.get('/api/applicationuser/getbyname/', config, function (res) {
                            var time = moment().add(response.data.expires_in, 'seconds').format('HH:mm:ss')
                            var date = moment().format('DD/MM/yyyy ') + time
                            userInfo = {
                                accessToken: response.data.access_token,
                                userName: res.data.UserName,
                                fullName: res.data.FullName,
                                email: res.data.Email,
                                refresh_token: response.data.refresh_token,
                                expires_in: date,
                            };

                            authenticationService.setTokenInfo(userInfo);
                            authData.authenticationData.IsAuthenticated = true;
                            authData.authenticationData.accessToken = userInfo.accessToken;
                            authData.authenticationData.userName = userInfo.userName;
                            authData.authenticationData.fullName = userInfo.fullName;
                            authData.authenticationData.email = userInfo.email;
                            authData.authenticationData.refresh_token = userInfo.refresh_token;
                            authData.authenticationData.expires_in = userInfo.expires_in;
                            deferedRefreshAccessToken.resolve(response.data.access_token);
                            deferedRefreshAccessToken = null;
                        }, function (err, status) {
                            deferedRefreshAccessToken.reject(err);
                            deferedRefreshAccessToken = null;
                        })
                    }, function myError(error) {
                        deferedRefreshAccessToken.reject(error);
                        deferedRefreshAccessToken = null;
                    })
                    return deferedRefreshAccessToken.promise;
                }

            }

            this.logOut = function () {
                apiService.post('/api/account/logout', null, function (response) {
                    authenticationService.removeToken();
                    initialValue();

                }, null);
            };
            function initialValue() {
                authData.authenticationData.IsAuthenticated = false;
                authData.authenticationData.userName = "";
                authData.authenticationData.fullName = "";
                authData.authenticationData.email = "";
                authData.authenticationData.accessToken = "";

                authData.authenticationData.refresh_token = "";
                authData.authenticationData.expires_in = "";
            }
        }]);
})(angular.module('apphome.common'));