﻿(function () {
    angular.module('apphome.service', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('service', {
          url: "/service",
          parent: 'base',
            templateUrl: "apphome/conponents/service/ServiceView.html?v=" + window.appVersion,
            controller: "ServiceController"
      })

    }
})(); 