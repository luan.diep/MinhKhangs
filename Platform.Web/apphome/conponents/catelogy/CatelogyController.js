﻿
(function (app) {
    app.controller('CatelogyController', CatelogyController);
    CatelogyController.$inject = ['$scope', 'loginService', '$injector', 'notificationService', 'localStorageService', 'apiService', '$timeout', '$state','$stateParams','$location'];
    function CatelogyController($scope, loginService, $injector, notificationService, localStorageService, apiService, $timeout, $state, $stateParams, $location) {
        $scope.TotalPagesproduct = [];/*total page product*/
        $scope.limit = 0;
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        $scope.Get_product_catelory = function (item,page) {
            if (item) {
                $location.search('id',item)
            }
            page = page || 0;
            var config = {
                params: {
                    manhom: !item ? $stateParams.id : item,
                    page: page,
                    pageSize: 28,
                }
            }
            apiService.get('api/hanghoa/Getcategory_product', config, function (result) {
                $scope.All_category_product = result.data.Items;
                if ($scope.limit == 0) {
                    var item = [];
                    for (var i = 1; i <= result.data.TotalPages; i++) {
                        item.push(i)
                        

                    }
                    $scope.TotalPagesproduct = item;
                    $scope.TenNhomHH = result.data.TenNhom;
                    $scope.MaNhomHH = result.data.MaNhomHH;
                    $scope.limit += 1
                   
                }
               
            }, function () {

            })
        }
       


        
       
      
       
        
        $('.popularity_view_sortz1, #fliter_cate').hover(function () {
            $('#fliter_cate').css("display", "block")
        }, function () {
                $('#fliter_cate').css("display", "none")
        })
        $(document).ready(function () {
            var paginationPage = parseInt($('.cdp').attr('actpage'), 10);
            $('.content_detail__pagination').delegate('.cdp_i','click', function () {
                var go = $(this).attr('data-filter').replace('#!', '');
                if (go === '+1') {
                    paginationPage++;
                    $scope.Get_product_catelory("", paginationPage - 1)
                    $("html, body").animate({
                        scrollTop: 0
                    }, 600);
                } else if (go === '-1') {
                    paginationPage--;
                    $scope.Get_product_catelory("", paginationPage - 1)
                    $("html, body").animate({
                        scrollTop: 0
                    }, 600);
                } else {
                    paginationPage = parseInt(go, 10);
                    $("html, body").animate({
                        scrollTop: 0
                    }, 600);
                }
                $('.cdp').attr('actpage', paginationPage);
            });
           
        });


        $scope.click_heart = function (mahang,boolen) {
            var heartmodel = {};
            heartmodel.MaHang = mahang;
            heartmodel.DaXoa = boolen;
            if ($scope.DaDangNhap == false) {
                $scope.openlogin()
            }
            else {
               
                apiService.post('api/hanghoayeuthich/createHeart', heartmodel, function (result) {
                    var index = $scope.All_category_product.findIndex(x => x.MaHang == mahang);
                    $scope.All_category_product[index].heart = boolen;


                }, function () {



                })

            }
        }

        $('.new_proMenu_box1 .picture_array_Icbtn.choosez .picture_array_ListIc ').css("display", "none")
        $('.new_proMenu_box1 .video_array_Icbtn  .List-icon  ').css("display", "none")
        $('.new_proMenu_box1 li').on('click', function () {
            if (this.id == 'vertical-list-product') {
                $(".new_proList_LTBOX .new_proList_mainListL  ").removeClass('horizontal')  
                $(".new_proList_LTBOX .new_proList_mainListL  ").addClass('vertical')
                $('.new_proMenu_box1 .picture_array_Icbtn.choosez .picture_array_ListIc ').css("display", "none")
                $('.new_proMenu_box1 .picture_array_Icbtn.choosez .List-icon ').css("display", "block")
                $('.new_proMenu_box1 .video_array_Icbtn  .List-icon  ').css("display", "none")
                $('.new_proMenu_box1 .video_array_Icbtn  .video_array_ListIc   ').css("display", "block")

            }
            else if (this.id == 'horizontal-list-product') {
                $(".new_proList_LTBOX .new_proList_mainListL  ").removeClass('vertical')  
                $(".new_proList_LTBOX .new_proList_mainListL ").addClass('horizontal')
                $('.new_proMenu_box1 .picture_array_Icbtn.choosez .picture_array_ListIc ').css("display", "none")
                $('.new_proMenu_box1 .video_array_Icbtn  .List-icon  ').css("display", "block")
                $('.new_proMenu_box1 .video_array_Icbtn  .video_array_ListIc   ').css("display", "none")
                $('.new_proMenu_box1 .picture_array_Icbtn.choosez .picture_array_ListIc ').css("display", "block")
                $('.new_proMenu_box1 .picture_array_Icbtn.choosez .List-icon ').css("display", "none")

            }
        })

     
        
        $scope.Get_product_catelory();
    }
})(angular.module('apphome.catelogy'));
