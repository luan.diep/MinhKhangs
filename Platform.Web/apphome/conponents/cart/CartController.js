﻿
(function (app) {
    app.controller('CartController', CartController);
    CartController.$inject = ['$scope', 'loginService', '$injector', 'notificationService', 'localStorageService', 'apiService','$timeout'];
    function CartController($scope, loginService, $injector, notificationService, localStorageService, apiService, $timeout) {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        if ($scope.ORDERINFORMATION == true) {
            $scope.changesuces()
        }
        /**limit volume*/
        $scope.checkvalue = function() {

            if ($(".shopping_cart_01").val() >= 1) {
                $(".cart_qty_reduce.choosez").css({ "cursor": "pointer", "pointer-events": "visible" });
                $(".cart_qty_reduce.choosez .icon").css({ "color": "#8d8d8f" });
            } else {
                $(".cart_qty_reduce.choosez").css({ "cursor": "none", "pointer-events": "none" });
                $(".cart_qty_reduce.choosez .icon").css({ "color": "#CCCCCC" });
            }

        }




        $scope.printDiv=function () {

         
            html2canvas($("#DivIdToPrint"), {
                onrendered: function (canvas) {
                    theCanvas = canvas;
                    canvas.toBlob(function (blob) {
                        saveAs(blob, "Hinh.png");
                    });
                }
            });
        }
     
        //if ($(".shopping_cart_01").attr() > 1) {
        //    $(".cart_qty_reduce.choosez").css({ "cursor": "pointer", "pointer-events": "visible" });
        //    $(".cart_qty_reduce.choosez .icon").css({ "color": "#8d8d8f" });
        //} else {
        //    $(".cart_qty_reduce.choosez").css({ "cursor": "none", "pointer-events": "none" });
        //    $(".cart_qty_reduce.choosez .icon").css({ "color": "#CCCCCC" });
        //}
    }
})(angular.module('apphome.cart'));
