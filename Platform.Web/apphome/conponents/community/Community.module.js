﻿(function () {
    angular.module('apphome.community', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('welcome', {
            url: "/welcome",
          parent: 'base',
            templateUrl: "apphome/conponents/community/WelcomeView.html?v=" + window.appVersion,
            controller: "WelcomeController"
        })

            .state('blog', {
                url: "/blog",
            parent: 'base',
            templateUrl: "apphome/conponents/community/BlogView.html?v=" + window.appVersion,
            controller: "BlogController"
        })

    }
})(); 