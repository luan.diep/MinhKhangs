﻿
(function (app) {
    app.controller('History_detailController', History_detailController);
    History_detailController.$inject = ['$scope', 'loginService', '$injector', 'notificationService', 'localStorageService', 'apiService', '$timeout','$stateParams'];
    function History_detailController($scope, loginService, $injector, notificationService, localStorageService, apiService, $timeout, $stateParams) {
        $scope.Listdetail_history = [];
        $scope.Timeline = [];
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        $scope.DanhSachHangHoaChiTiet = function () {
            var config = {
                params: {
                    mact: $stateParams.id
                }
            }
            apiService.get('api/chungtudathang/Detai_oderproduct', config, function (result) {
               
                $scope.Listdetail_history = result.data.ChiTiet;
                $scope.Timeline = result.data.TrangThaiGiaoHang;
                var index = $scope.Timeline.findIndex(x => x.MaTrangThai == 4)
                if (index != -1) {
                    $scope.Timelinefalse = $scope.Timeline[index]
                    $scope.Timeline.splice(index,1)
                }

                if ($scope.Timeline[$scope.Timeline.length - 1].MaTrangThai > 2 || $scope.Timelinefalse.MaTrangThai==4) {
                    $("#btn-huy").addClass("disable")
                }

            }, function () {
                console.log('Khong lay duoc chi tiet');
            })
        }
       
        $scope.HuyDon = function () {
            if ($scope.Timeline.length <= 2) {
                Swal.fire({
                    title: 'Bạn chắc muốn hủy hay không ?',
                    showDenyButton: true,
                    confirmButtonText: `Xác nhận hủy`,

                }).then((result) => {
                    if (result.isConfirmed) {
                        var config = {
                            params: {
                                mact: $scope.Listdetail_history[0].MaChungTuDatHang,

                            }
                        }
                        apiService.get('api/chungtudathang/huy', config, function (result) {
                            if (result.data == "") {
                                $scope.Cart.length = 0;
                                $scope.DanhSachHangHoaChiTiet();
                            }
                        }, function () {
                        })
                    } else {

                    }

                })

            }
        }  


        $scope.$watch('Listdetail_history', item => {
            var i;
            if (item) {
                item.forEach(value => {
                    value.ThanhTien = value.DonGia * value.SoLuong
                    value.TotalGia = value.DonGia * value.SoLuong
                    i = value
                })
            }


        }, true)

        $scope.DanhSachHangHoaChiTiet();

    }
})(angular.module('apphome.historyoderproduct'));
