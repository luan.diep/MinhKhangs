﻿(function () {
    angular.module('apphome.historyoderproduct', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('historyoderproduct', {
          url: "/historyoderproduct",
          parent: 'base',
            templateUrl: "apphome/conponents/historyoderproduct/Historyoder_productView.html?v=" + window.appVersion,
            controller: "Historyoder_productController"
      })
        .state('history_detail', {
            url: "/history_detail?:id",
          parent: 'base',
            templateUrl: "apphome/conponents/historyoderproduct/History_detailView.html?v=" + window.appVersion,
            controller: "History_detailController"
      })

    }
})(); 