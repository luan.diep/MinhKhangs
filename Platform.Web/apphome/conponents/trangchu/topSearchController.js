﻿(function (app) {
    app.controller('topSearchController', topSearchController);

    topSearchController.$inject = ['$state', '$scope','apiService'];

    function topSearchController($state, $scope, apiService) {
        $scope.options = {
            dataTextField: 'TenHang',
            template: '<span class="k-state-default"><h2>#: data.TenHang #</h3></span>',
            height: 700,
            dataSource: {
                type: "json",
                serverFiltering: true,
                transport: {
                    read: {
                        url: "/api/hanghoa/getbyname",
                        beforeSend: function (req) {
                            req.setRequestHeader('Authorization', 'Bearer ' + $scope.authentication.accessToken);
                        }
                    },
                    parameterMap: function (data, type) {
                        if (data.filter.filters.length > 0) {
                            return {
                                id: data.filter.filters[0].value
                            };
                        }
                        else {
                            return {
                                id: ""
                            };
                        }

                    }

                }
            },

        }
        $scope.onSelect = function (ev) {
            var item = ev.item;
            var widget = ev.sender;
            var info = widget.dataItem(item);
            $state.go('chitiethanghoa', {
                id: info.MaHang
            })
        };



        
    }
})(angular.module('apphome'));

