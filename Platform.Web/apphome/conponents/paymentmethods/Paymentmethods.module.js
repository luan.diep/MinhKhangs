﻿(function () {
    angular.module('apphome.paymentmethods', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('paymentmethods', {
          url: "/paymentmethods",
          parent: 'base',
            templateUrl: "apphome/conponents/paymentmethods/PaymentmethodsView.html?v=" + window.appVersion,
            controller: "PaymentmethodsController"
      })

    }
})(); 