﻿(function () {
    angular.module('apphome.fssupport', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('fssupport', {
          url: "/fssupport",
          parent: 'base',
            templateUrl: "apphome/conponents/fssupport/FssupportView.html?v=" + window.appVersion,
            controller: "FssupportController"
      })

    }
})(); 