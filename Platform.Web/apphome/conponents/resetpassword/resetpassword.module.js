﻿(function () {
    angular.module('apphome.resetpassword', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('resetpassword', {
            url: "/resetpassword?userId&:code",
          
            templateUrl: "apphome/conponents/resetpassword/resetpasswordView.html?v=" + window.appVersion,
            controller: "resetpasswordController"
      })

    }
})(); 