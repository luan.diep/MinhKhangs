﻿(function () {
    angular.module('apphome.coomingsoon', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('coomingsoon', {
          url: "/coomingsoon",
          parent: 'base',
            templateUrl: "apphome/conponents/coomingsoon/coomingsoonView.html?v=" + window.appVersion,
            controller: "coomingsoonController"
      })

    }
})(); 