﻿(function () {
    angular.module('apphome.recruitment', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('recruitment', {
          url: "/recruitment",
          parent: 'base',
            templateUrl: "apphome/conponents/recruitment/RecruitmentView.html?v=" + window.appVersion,
            controller: "RecruitmentController"
      })

    }
})(); 