﻿using AutoMapper;
using Platform.Model;
using Platform.Model.Models;
using Platform.Web.Api;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.mapping
{
    public class AutoMapperConfiguation
    {
        public static void Configure()
        {
          
            Mapper.CreateMap<Loi, LoiViewModel>();



            Mapper.CreateMap<ApplicationGroup_KH, ApplicationGroupViewModel>();
            Mapper.CreateMap<ApplicationRole_KH, ApplicationRoleViewModel>();
            Mapper.CreateMap<ApplicationUser_KH, ApplicationUserViewModel>();
          

         
          
         
            Mapper.CreateMap<ChucVu, ChucVuViewModel>();
            Mapper.CreateMap<HangHoa, HangHoaViewModel>();
        
          
           
            Mapper.CreateMap<ChiTietDonMuaHang, ChiTietDonMuaHangViewModel>();
        
            Mapper.CreateMap<ChiTietChungTuMuaHang, ChiTietChungTuMuaHangViewModel>();
       
            Mapper.CreateMap<DonDatHang, DonDatHangViewModel>();
            Mapper.CreateMap<ChiTietDonDatHang, ChiTietDonDatHangViewModel>();
            
            Mapper.CreateMap<HoaDon_BanHang, HoaDon_BanHangViewModel>();
            Mapper.CreateMap<ChiTietHoaDon_BanHang, ChiTietHoaDon_BanHangViewModel>();
          
            Mapper.CreateMap<NhomHangHoa, NhomHangHoaViewModel>();
        
          
            Mapper.CreateMap<DonViTinh, DonViTinhViewModel>();
       
            Mapper.CreateMap<CoSo,CoSoViewModel>();
         
           
          
            Mapper.CreateMap<DoiTacGiaoHang, DoiTacGiaoHangViewModel>();
            Mapper.CreateMap<KhachHang, KhachHangViewModel>();
            Mapper.CreateMap<ChungTuGiaoHang, ChungTuGiaoHangViewModel>();
          

            //Mapper.CreateMap<DoiTacGiaoHang, ExportedDoiTacGiaoHangModel>();
         
         
            Mapper.CreateMap<ChungTuDatHang, ChungTuDatHangViewModel> ();
            Mapper.CreateMap<ChiTietChungTuDatHang, ChiTietChungTuDatHangViewModel> ();
            Mapper.CreateMap<TrangThaiGiaoHang, TrangThaiGiaoHangViewModel> ();
            Mapper.CreateMap<ChungTuDatHang, DanhSachChiTietTrangThaiDonGiaoHang> ();
            Mapper.CreateMap<KhachHang_KhongTaiKhoanViewModel, KhachHang_KhongTaiKhoanViewModel> ();
            Mapper.CreateMap<GioHangTam, ChiTietChungTuDatHangViewModel> ();
            Mapper.CreateMap<GioHangTam, ListChiTietChungTuDatHang> ();
            Mapper.CreateMap<GioHangTam, ChiTietChungTuDatHang> ();
            Mapper.CreateMap<ListChiTietChungTuDatHang, ChiTietChungTuDatHang> ();
            Mapper.CreateMap<HangHoa_CoSo, HangHoa_CoSoViewModel> ();
            Mapper.CreateMap<Cart_product, ChiTietChungTuDatHangViewModel> ();
            Mapper.CreateMap<HangHoaYeuThich, HangHoaYeuThichViewModel> ();
            Mapper.CreateMap<NhomHangHoa, NhomHangHoaGroup> ();
            Mapper.CreateMap<NhomHangHoaGroup, NhomHangHoaGroups> ();
           

           




        }
    }
}