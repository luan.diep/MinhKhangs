﻿(function () {
    angular.module('apphome.chitietsanpham', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('chitietsanpham', {
          url: "/chitietsanpham?id",
          parent: 'base',
            templateUrl: "apphome/conponents/chitietsanpham/chiTietSanPhamView.html?v=" + window.appVersion,
            controller: "chiTietSanPhamController"
      })

    }
})(); 