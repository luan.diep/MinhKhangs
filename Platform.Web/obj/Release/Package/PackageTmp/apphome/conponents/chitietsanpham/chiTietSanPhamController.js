﻿

(function (app) {
    app.controller('chiTietSanPhamController', chiTietSanPhamController);
    chiTietSanPhamController.$inject = ['$scope', 'loginService', '$injector', 'notificationService', 'localStorageService', 'apiService', '$timeout','$stateParams'];
    function chiTietSanPhamController($scope, loginService, $injector, notificationService, localStorageService, apiService, $timeout, $stateParams) {
        $("html, body").animate({
            scrollTop: 0
        }, 600);

        $scope.TotalPages = [];
        $(document).ready(function () {
            var paginationPage = parseInt($('.cdp').attr('actpage'), 10);
            $('.content_detail__pagination').delegate('.cdp_i', 'click', function () {
                var top = $(".Contentbox").offset().top;
                $("html, body").animate({
                    scrollTop:top
                }, 600);
                var go = $(this).attr('data-filter').replace('#!', '');
                if (go === '+1') {
                    paginationPage++;
                    $scope.GroupProduct(null,paginationPage - 1)
                } else if (go === '-1') {
                    paginationPage--;
                    $scope.GroupProduct(null,paginationPage - 1)
                } else {
                    paginationPage = parseInt(go, 10);
                }
                $('.cdp').attr('actpage', paginationPage);
            });
        });

        var heght = $('#products_tab_description').offset().top;
        //$(document).ready(function () {
        //    if ($(window).width() > 768) {
        //        $(window).on('scroll', function () {
        //            if ($(this).scrollTop() >= heght && $(window).width() > 480) {
        //                $('.product_sticky').addClass('active');
        //            } else if ($(this).scrollTop() < heght && $(window).width() > 480) {
        //                $('.product_sticky').removeClass('active');
        //            }
        //        });

        //        $('.product_tab01_ul li').click(function () {
        //            $('.product_tab01_ul li').removeClass('active');
        //            $(this).addClass("active");

        //        })
        //    }
          
           
        //});


        $scope.layChiTietHangHoa= function (item) {
            var mahang;
            if (!item) {
                mahang = $stateParams.id;
            } else {
                mahang = item;
            }

            var config = {
                params: {
                    mahang: mahang,
                }
            }

            apiService.get('api/hanghoa/chitiethanghoadathang', config, function (result) {
                $scope.ChiTietHangHoa = result.data;
                

                $scope.listimage = [];
                var image = [];
                $scope.listimage = JSON.parse($scope.ChiTietHangHoa.NhieuHinhAnh);

                angular.forEach($scope.listimage, function (i) {
                    var json = { HinhAnh: i }
                    image.push(json)

                })

                if (image.length == 0) {
                    $scope.ChiTietHangHoa.NhieuHinhAnhs = [];
                    $scope.ChiTietHangHoa.NhieuHinhAnhs.push({ HinhAnh: $scope.ChiTietHangHoa.HinhAnh });
                    $(document).ready(function () {
                    var mySwiper = new Swiper('.proImg_check_carousel_cont.swiper-container', {
                        slidesPerView: 3,
                        loop: true,
                        autoplay: {
                            delay: 2500,
                            disableOnInteraction: false,
                        },
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: true,
                        },
                        navigation: {
                            nextEl: '.swiper-button-next',
                            prevEl: '.swiper-button-prev',
                        }
                    });
                      
                    });


                } else {
                    $scope.ChiTietHangHoa.NhieuHinhAnhs = image;
                    $(document).ready(function () {
                    var mySwiper = new Swiper('.proImg_check_carousel_cont.swiper-container', {
                        slidesPerView: 3,
                        loop: true,
                        autoplay: {
                            delay: 2500,
                            disableOnInteraction: false,
                        },
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: true,
                        },
                        navigation: {
                            nextEl: '.swiper-button-next',
                            prevEl: '.swiper-button-prev',
                        }
                    });
                    });

                }

               
                
                $scope.ChiTietHangHoa.SoLuong = 1;
                $scope.Mota = $scope.ChiTietHangHoa.MoTa;
                $('#mota').html($scope.Mota)
                layThanhPham();
                $("html, body").animate({
                    scrollTop: 0
                }, 600);
                $scope.GroupProduct(mahang)
            }, function () {
                console.log('Khong lay duoc chi tiet hang hoa');
            })
        }




        $scope.GroupProduct = function (i,page) {
            page = page || 0;
            if (!i) {
                mahang = $stateParams.id;
            } else {
                mahang = i;
            }
            var config = {
                params: {
                    id: mahang,
                    page: page,
                    pageSize: 8
                }
            }
            apiService.get('api/hanghoa/GroupProduct', config, function (result) {
                $scope.SanPhamLienquan = result.data.Items;
              
                for (var i = 1; i <= result.data.TotalPages; i++) {
                    $scope.TotalPages.push(i)
                }
              
            }, function () {
                console.log('Khong lay duoc hang hoa');
            })

            

        }















        $scope.limit = 1;
        function layThanhPham() {

            var config = {
                params: {
                    mahang: $stateParams.id
                }
            }
            apiService.get('api/thanhpham/getbymahang', config, function (result) {
                $scope.ThanhPham = result.data;
            }, function () {
                console.log('khong lay dc thanh pham')
            })
        }








        /*modal image zoom*/
        $scope.OpenModal = function (i) {
            var link = 'https://mekong-agri.com';
            $(".detail_veoImg_alert").css('display','block')
           
        }  
        $('.proImg_check_carousel_cont ul').delegate('li', "click", function () {
            $('.swiper_slide_active').removeClass('swiper_slide_active')
            $(this).addClass('swiper_slide_active')
            var a = $(this).find('img').attr('src');
            $('.proImg_alink img').attr("src", a)
            
            //$(this).addClass('.swiper_slide_active');
        })


      

        $('.detail_veoImg_big_list1 div ul').delegate('.checked_showImg_li', 'click', function () {
            var _imgInl = $('.detail_veoImg_big_list1 div ul li').length;
            
            var _imgInt = $(this).index();
            var widthbig = $(".detail_veoImg_big_list ul li").eq(_imgInt).width() * (_imgInt)
            $('.detail_veoImg_big_list ul li').css({ 'transform': 'translate3d(-' + widthbig + 'px, 0px, 0px)', 'transition': '0.5s' });
            $('.detail_veoImg_big_list1 div ul').find(".choosez").removeClass("choosez")
            $(this).addClass("choosez")
           
           
            
        });
        $('.detail_veoImg_next').on('click', function () {
            var _length_img_big = $('.detail_veoImg_big_list ul li').length - 1;
            var index_img_con = $('.detail_veoImg_big_list1 div ul').find(".choosez").index()+1;
            var widthbig = $(".detail_veoImg_big_list ul li").eq(index_img_con).width() * (index_img_con)
            if (_length_img_big == $('.detail_veoImg_big_list1 div ul').find(".choosez").index()) {
                $('.detail_veoImg_big_list ul li').css({ 'transform': 'translate3d(-' + 0 + 'px, 0px, 0px)', 'transition': '0.1s' });
                $('.detail_veoImg_big_list1 div ul').find(".choosez").removeClass("choosez")
                $('.detail_veoImg_big_list1 div ul li').eq(0).addClass("choosez")
            } else {
                $('.detail_veoImg_big_list1 div ul').find(".choosez").removeClass("choosez")
                $('.detail_veoImg_big_list1 div ul li').eq(index_img_con).addClass("choosez")
                $('.detail_veoImg_big_list ul li').css({ 'transform': 'translate3d(-' + widthbig + 'px, 0px, 0px)', 'transition': '0.5s'});

            }
           
           
            
        });
        $('.detail_veoImg_pre').on('click', function () {
            var _length_img_big = $('.detail_veoImg_big_list ul li').length - 1;
            var index_img_con = $('.detail_veoImg_big_list1 div ul').find(".choosez").index()-1;
            var widthbig = $(".detail_veoImg_big_list ul li").eq(index_img_con).width() * (index_img_con)
            if (($('.detail_veoImg_big_list1 div ul').find(".choosez").index())== 0) {
                $('.detail_veoImg_big_list ul li').css({ 'transform': 'translate3d(-' + ($(".detail_veoImg_big_list ul li").eq(_length_img_big).width() * (_length_img_big)) + 'px, 0px, 0px)', 'transition': '0.1s'});
                $('.detail_veoImg_big_list1 div ul').find(".choosez").removeClass("choosez")
                $('.detail_veoImg_big_list1 div ul li').eq(_length_img_big).addClass("choosez")
            } else {
                $('.detail_veoImg_big_list1 div ul').find(".choosez").removeClass("choosez")
                $('.detail_veoImg_big_list1 div ul li').eq(index_img_con).addClass("choosez")
                $('.detail_veoImg_big_list ul li').css({ 'transform': 'translate3d(-' + widthbig + 'px, 0px, 0px)', 'transition': '0.5s'});
            }
           
           
            
        });




        $scope.layChiTietHangHoa();



        
    }
})(angular.module('apphome.chitietsanpham'));


//app.directive("foo", function () {
//    return {
//        restrict: "EA",
//        scope: {
//            n: "@"
//        },
//        template: "<div>{{n}}</div>",
//        link: function (scope) {
           
//        }
//    };
//});
