﻿(function () {
    angular.module('apphome.catelogy', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('catelogy', {
            url: "/catelogy?id",
            templateUrl: "apphome/conponents/catelogy/CatelogyView.html?v=" + window.appVersion,
            parent: 'base',
            controller: "CatelogyController"
      })

    }
})(); 