﻿(function () {
    angular.module('apphome.loginadmin', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('loginadmin', {
          url: "/loginadmin",
          parent: 'base',
            templateUrl: "apphome/conponents/loginadmin/LoginAdminView.html?v=" + window.appVersion,
            controller: "LoginAdminController"
      })

    }
})(); 