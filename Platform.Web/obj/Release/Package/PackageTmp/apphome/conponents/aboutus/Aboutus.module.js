﻿(function () {
    angular.module('apphome.aboutus', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('aboutus', {
          url: "/aboutus",
          parent: 'base',
            templateUrl: "apphome/conponents/aboutus/AboutusView.html?v=" + window.appVersion,
            controller: "AboutusController"
      })

    }
})(); 