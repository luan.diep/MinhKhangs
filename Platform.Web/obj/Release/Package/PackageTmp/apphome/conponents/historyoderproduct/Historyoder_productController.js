﻿
(function (app) {
    app.controller('Historyoder_productController', Historyoder_productController);
    Historyoder_productController.$inject = ['$scope', 'loginService', '$injector', 'notificationService', 'localStorageService', 'apiService','$timeout'];
    function Historyoder_productController($scope, loginService, $injector, notificationService, localStorageService, apiService, $timeout) {
        $scope.status = 0;
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        $scope.getlichsu = function (page) {
            page = page || 0;
            var config = {
                params: {
                    page: page,
                    pageSize: 5,
                }
            }
            apiService.get('api/chungtudathang/DanhSachChiTietTrangThaiDonGiaoHang', config, function (result) {
                $scope.history = result.data.Items;
                if ($scope.status == 0) {
                    var Itempage = [];
                    for (var i = 1; i <= result.data.TotalPages; i++) {
                        Itempage.push(i)

                    }
                    $scope.TotalPages = Itempage;
                    $scope.status+=1
                }
                angular.forEach($scope.history, function (item) {
                    item.TenTrangThai = item.TrangThaiGiaoHang[item.TrangThaiGiaoHang.length - 1].TenTrangThai;
                });

            }, function () {
                console.log('Khong lay duoc lich su');
            })

        }
        $scope.getlichsu();
        $(document).ready(function () {
            var paginationPage = parseInt($('.cdp').attr('actpage'), 10);
            $('.cdp_i').on('click', function () {
                var go = $(this).attr('data-filter').replace('#!', '');
                if (go === '+1') {
                    paginationPage++;
                    $scope.getlichsu(paginationPage-1)
                } else if (go === '-1') {
                    paginationPage--;
                    $scope.getlichsu(paginationPage-1)
                } else {
                    paginationPage = parseInt(go, 10);
                }
                $('.cdp').attr('actpage', paginationPage);
            });
        });


    }
})(angular.module('apphome.historyoderproduct'));
