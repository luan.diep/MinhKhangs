﻿(function () {
    angular.module('apphome.trangchu', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('trangchu', {
          url: "/trangchu?id",
          parent: 'base',
          templateUrl: "apphome/conponents/trangchu/trangChuView.html?v=" + window.appVersion,
          controller: "trangChuController"
      })

    }
})();