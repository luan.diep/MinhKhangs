﻿(function (app) {
	app.controller('rootController', rootController);

	rootController.$inject = ['$state', 'authData', 'loginService', '$scope', 'authenticationService', 'apiService', '$q', '$timeout', '$interval', '$filter', 'notificationService', '$window', '$ngBootbox', 'localStorageService', '$state', 'vcRecaptchaService', '$location', 'cfpLoadingBar'];

	function rootController($state, authData, loginService, $scope, authenticationService, apiService, $q, $timeout, $interval, $filter, notificationService, $window, $ngBootbox, localStorageService, $state, vcRecaptchaService, $location, cfpLoadingBar) {
		$scope.DaDangNhap = false;
		$scope.checkCoTK = true;
		$scope.Buttom = false;
		$scope.TrangThaiKTK = true;
		$scope.Cart = [];
		$scope.history = [];
		$scope.fliter_search = [];
		$scope.Detailproduct = {}
		$scope.DangNhap = {
			UserName: "",
			PassWord: "",
		}
		$scope.DangKy = {
			DiaChi: $scope.Location
		};
		$scope.group = {}
		$scope.SoLuong = 1;
		$scope.Listdetail_history = [];
		$scope.authentication = authData.authenticationData
		$scope.ORDERINFORMATION = false;
		$scope.succsess = []
		$scope.Location = ""
		$scope.showpogotpass = false


		//capcha
		$scope.responsesKTK = null;
		$scope.widgetIds = null;
		$scope.model = {
			key: '6Lc8V_8ZAAAAABVK6vaMg7r-ga_RS4TQV3a8MWQJ'
		};
		$scope.setResponses = function (response) {
			console.info('Response available');


			$scope.responsesKTK = response;
			console.info(response);
		};

		$scope.setWidgetIds = function (widgetId) {
			console.info('Created widget ID: %s', widgetId);

			$scope.widgetIds = widgetId;
		};

		$scope.cbExpirations = function () {
			console.info('Captcha expired. Resetting response object');

			vcRecaptchaService.reload($scope.widgetIds);

			$scope.responsesKTK = null;
		};

		$scope.responsesDK = null;
		$scope.widgetId = null;
		$scope.model = {
			key: '6Lc8V_8ZAAAAABVK6vaMg7r-ga_RS4TQV3a8MWQJ'
		};
		$scope.setResponse = function (response) {
			console.info('Response available');
			$scope.responsesDK = response;
		};

		$scope.setWidgetId = function (widgetId) {
			$scope.widgetId = widgetId;
		};

		$scope.cbExpiration = function () {
			vcRecaptchaService.reload($scope.widgetId);

			$scope.responsesDK = null;
		};


		$scope.AllCoSo = function () {
			apiService.get('api/coso/All', null, function (result) {
				$scope.AllCoSos = result.data;

			}, function () {

			})

		}


		$scope.getmenucon = function (i, o) {

			var configs = {
				params: {
					id: i,

				},
				ignoreLoadingBar: true
			}

			apiService.get("api/hanghoa/layhanghoadathangs", configs, function (results) {
				$scope.TatCaHangHoaDHs = results.data
				$(".home_solution_right_ul li").addClass("active");
				$scope.submenu = o;

			}, function () {


			})

		}


		$('.header_main_firstul .header_main_list, .header_main_list_more_all').hover(function () {
			$('.header_main_list_more_all').css('display', 'block');
		}, function () {
			$('.header_main_list_more_all').css('display', 'none');
			//$('.header_main_firstul li').removeClass('active');

			//if ($state.current.name == "trangchu") {
			//    $(".header_main_firstul li").eq(0).addClass("active")

			//}
			// if ($state.current.name == "product") {
			//    $(".header_main_firstul li").eq(1).addClass("active")

			//}
			// if ($state.current.name == "fssupport") {
			//    $(".header_main_firstul li").eq(3).addClass("active")

			//} else {


			//}
		})

		$(document).ready(function () {
			//$('.home_solution_left_ul').on('click', '.home_solution_left_ul_li', function (evt) {
			//    //backgound a
			//    $('.home_solution_left_ul li .home_solution_left_ul_li_a').removeClass("active");
			//    //menu right
			//    $('.home_solution_right_ul li').removeClass("active");
			//    //var indexmenucon_li = $(this).find(".home_solution_left_ul_con li").length;
			//    $(".fas.fa-chevron-right.icon").css("display", "block")
			//    $(".fas.fa-chevron-down.icon").css("display", "none")
			//    $('.home_solution_left_ul_con').removeClass("active")
			//    var findul = $(this).find("ul li");
			//    if (findul.length > 0) {
			//        $(this).find(".home_solution_left_ul_li_a .fas.fa-chevron-right.icon").css("display", "none")
			//        $(this).find(" .home_solution_left_ul_li_a .fas.fa-chevron-down.icon").css("display", "block")
			//        $(this).find(".home_solution_left_ul_con").addClass("active")

			//    } 
			//    $(this).find('.home_solution_left_ul_li_a').addClass("active");
			//    $(this).addClass("active");


			//});
			//opensubmenu
			$scope.opensubmenu = function (item, title) {
				if (item.length > 0) {
					$scope.ListSubmenu = item;
					$(".header_sidebar_second_list_submenu").css("left", "0")
					$(".header_sidebar_second_list_mainTxt").text(title)
				}


			}

			$('.home_solution_left_ul').on('mouseenter mouseleave', '.home_solution_left_ul_li_a', function (evt) {
				//đếm li trong ul nằm ngang hàng với home_solution_left_ul_li_a
				var findindex = $(this).closest('.home_solution_left_ul_li').find('.home_solution_left_ul_con li').length;
				if (findindex > 0) {
					if ($(this).closest('.home_solution_left_ul_li').find('.home_solution_left_ul_con.active').length > 0) {
						//$(this).closest('.home_solution_left_ul_li').find('.home_solution_left_ul_con').removeClass("active")
						//$(".fas.fa-chevron-right.icon").css("display", "block")
						//$(".fas.fa-chevron-down.icon").css("display", "none")
					} else {
						//backgound a
						$('.home_solution_left_ul li .home_solution_left_ul_li_a').removeClass("active");
						//menu right
						$('.home_solution_right_ul li').removeClass("active");
						//var indexmenucon_li = $(this).find(".home_solution_left_ul_con li").length;
						$(".fas.fa-chevron-right.icon").css("display", "block")
						$(".fas.fa-chevron-down.icon").css("display", "none")
						$('.home_solution_left_ul_con').removeClass("active")

						$(this).find(".fas.fa-chevron-right.icon").css("display", "none")
						$(this).find(".fas.fa-chevron-down.icon").css("display", "block")
						$(this).closest('.home_solution_left_ul_li').find('.home_solution_left_ul_con').addClass("active")


						$(this).addClass("active");
						$(this).closest('.home_solution_left_ul_li').addClass("active");
					}
				} else {

					//backgound a
					$('.home_solution_left_ul li .home_solution_left_ul_li_a').removeClass("active");
					//menu right
					$('.home_solution_right_ul li').removeClass("active");
					//var indexmenucon_li = $(this).find(".home_solution_left_ul_con li").length;
					$(".fas.fa-chevron-right.icon").css("display", "block")
					$(".fas.fa-chevron-down.icon").css("display", "none")
					$('.home_solution_left_ul_con').removeClass("active")
					$(this).addClass("active");
					$(this).closest('.home_solution_left_ul_li').addClass("active");
				}


			})


			$('.header_main_firstul li').hover(function () {
				$('.header_main_firstul li').removeClass('active');
				$(this).addClass("active");
				$('.header_main_list_more.nav').removeClass("show");
				var myindex = $(this).index();
				$(".header_main_list_more.nav:eq(" + myindex + ")").addClass("show");
				//var left = $('.header_main_firstul li').offset.left-10

				//var width = 40;
				//var a = $('.home_drop_down_carrier')
				//a.stop().animate({
				//    left: left,
				//    width: width
				//});
			});


		});

		$(document).ready(function () {

			$("#email_login").keyup("input", function () {

				$('#email_erro_login').css('display', 'none');

			}, function () {
				$('#email_erro_login').css('display', 'block');

			});
			$("#password_login").keyup("input", function () {
				$('#passw_erro_login').css('display', 'none');
			}, function () {
				$('#passw_erro_login').css('display', 'inline-block');

			});
			/*ckeck show pass login**/
			$('#show_hide_passlogin').click(function () {
				if ('password' == $('#password_login').attr('type')) {
					$('#password_login').prop('type', 'text');
					$('#show_hide_passlogin').text('ẩn');
				} else {
					$('#password_login').prop('type', 'password');
					$('#show_hide_passlogin').text('hiện');
				}
			});

			$('#show_hide_pass').click(function () {
				if ('password' == $('#Password_regist').attr('type')) {
					$('#Password_regist').prop('type', 'text');
					$('#show_hide_pass').text('ẩn');
				} else {
					$('#Password_regist').prop('type', 'password');
					$('#show_hide_pass').text('hiện');
				}
			});
			$('#show_hide_passconfrim').click(function () {
				if ('password' == $('#ConfirmPassword_regist').attr('type')) {
					$('#ConfirmPassword_regist').prop('type', 'text');
					$('#show_hide_passconfrim').text('ẩn');
				} else {
					$('#ConfirmPassword_regist').prop('type', 'password');
					$('#show_hide_passconfrim').text('hiện');
				}
			});


			$("#firstname_regist").keyup("input", function () {
				$('.require.firstname').css('display', 'none');
			}, function () {
				$('.require.firstname').css('display', 'inline-block');

			});
			$("#lastname_regist").keyup("input", function () {
				$('.require').css('display', 'none');
			}, function () {
				$('.require').css('display', 'inline-block');

			});
			$("#email_regist").keyup("input", function () {
				$('.require').css('display', 'none');
			}, function () {
				$('.require').css('display', 'inline-block');

			});
			$("#Password_regist").keyup("input", function () {
				$('.require').css('display', 'none');
			}, function () {
				$('.require').css('display', 'inline-block');

			});
			$("#ConfirmPassword_regist").keyup("input", function () {
				$('.require').css('display', 'none');
			}, function () {
				$('.require').css('display', 'inline-block');

			});
			$("#telephone_regist").keyup("input", function () {
				$('.require').css('display', 'none');
			}, function () {
				$('.require').css('display', 'inline-block');

			});
		});


		/*nhóm hàng hóa catelory*/

		$scope.getmenu = function () {
			apiService.get("api/nhomhanghoa/GroupNhomHangHoa", null, function (result) {
				$scope.AllNhomHangs = result.data
				var configs = {
					params: {
						id: $scope.AllNhomHangs[0].MaNhomHH,

					}
				}
				apiService.get("api/hanghoa/layhanghoadathangs", configs, function (results) {
					$scope.TatCaHangHoaDHs = results.data
					$('.home_solution_left_ul li .home_solution_left_ul_li_a').eq(0).addClass("active");

				}, function () {

				})

			}, function () {


			})

		}

		$scope.getmenu();


		/**parematesr category_product*/
		$scope.IdCategoryproduct = function (manhom) {
			$state.go('catelogy', {
				id: manhom

			});
			$('.header_main_list_more_all').css('display', 'none');
		}
		$scope.IdCategoryproductmobile = function (manhom, length) {
			if (length <= 0 || length == undefined) {
				$state.go('catelogy', {
					id: manhom

				});
				$('.header_main_list_more_all').css('display', 'none');
				$scope.closemenumobile()
			}

		}


		$scope.readmoremenu = function () {
			var id = $('.header_list_more_ul_main_all_con').attr('id')
			$state.go('catelogy', {
				id: id

			});
			$('.header_main_list_more_all').css('display', 'none');
		}


		$scope.token = localStorageService.get("TokenInfo");

		function DuyTriDangNhap() {

			apiService.get('api/applicationUser/getbyid', null, function (result) {
				$scope.DaDangNhap = true;
			}, function () {
				//console.log('chưa đăng nhập');
				$scope.DaDangNhap = false;
			})
		}
		$scope.logOut = function () {
			$scope.Thongtin = {};
			$scope.DaDangNhap = false;
			loginService.logOut();
			localStorageService.set("TokenInfo", null);
			localStorageService.remove("TokenInfo");
			$window.location.reload();
			$state.go('trangchu');
			$('.hreder_wrapp').css("padding", '20px 0 16px');
			$(".top_sign_a").find("span").text("Đăng nhập");
		}

		if ($scope.token) {
			DuyTriDangNhap();
		}

		$scope.dangKy = dangKy;

		function dangKy() {
			if ($scope.DangKy.FullName == undefined || $scope.DangKy.FullName.length == 0) {
				$('.Fullname_regist_error_prompt').css('display', 'block')
				$('.Fullname_regist_error_prompt').text('Vui lòng nhập tên của bạn.');
				return;

			} else {
				$('.Fullname_regist_error_prompt').css('display', 'none')
				$('.Fullname_regist_error_prompt').text('');

			}
			if ($scope.DangKy.Email == undefined || $scope.DangKy.Email.length == 0) {
				$('.Email_regist_error_prompt').css('display', 'block')
				$('.Email_regist_error_prompt').text('Hãy điền địa chỉ email của bạn.')
				return;
			} else {
				$('.Email_regist_error_prompt').css('display', 'none')
				$('.Email_regist_error_prompt').text('')
			}

			if ($scope.DangKy.PhoneNumber == undefined || $scope.DangKy.PhoneNumber.length == 0) {
				$('.PhoneNumber_regist_error_prompt').css('display', 'block')
				$('.PhoneNumber_regist_error_prompt').text('Xin vui lòng điền số điện thoại của bạn.')
				return;
			} else {
				$('.PhoneNumber_regist_error_prompt').css('display', 'none')
				$('.PhoneNumber_regist_error_prompt').text('')
			}

			if ($scope.DangKy.PhoneNumber.length <= 8) {
				$('.PhoneNumber_regist_error_prompt').css('display', 'block')
				$('.PhoneNumber_regist_error_prompt').text('Phải nhập đúng số điện thoại.')
				return;
			} else {
				$('.PhoneNumber_regist_error_prompt').css('display', 'none')
				$('.PhoneNumber_regist_error_prompt').text('')
			}
			if ($scope.DangKy.DiaChi == undefined || $scope.DangKy.DiaChi.length == 0) {
				$('.DiaChi_regist_error_prompt').css('display', 'block')
				$('.DiaChi_regist_error_prompt').text('Xin vui lòng điền địa chỉ của bạn.')
				return;
			} else {
				$('.DiaChi_regist_error_prompt').css('display', 'none')
				$('.DiaChi_regist_error_prompt').text('')
			}


			if ($scope.DangKy.Password == undefined || $scope.DangKy.Password.length == 0) {
				//$scope.ErroPass = "Mật khẩu dài ít nhất 6 ký tự"
				$('.password_regist_error_prompt').css('display', 'block')
				$('.password_regist_error_prompt').text('Mật khẩu không được bỏ trống.')
				$("#LoginModal").animate({
					scrollTop: 0
				}, 600);
				return
			} else {
				$('.password_regist_error_prompt').css('display', 'none')
				$('.password_regist_error_prompt').text('')
			}
			if ($scope.DangKy.Password.length < 6) {
				$('.password_regist_error_prompt').css('display', 'block')
				$('.password_regist_error_prompt').text('Mật khẩu dài ít nhất 6 ký tự')
				$("#LoginModal").animate({
					scrollTop: 0
				}, 600);
				return
			} else {
				$('.password_regist_error_prompt').css('display', 'none')
				$('.password_regist_error_prompt').text('')
			}

			if ($scope.DangKy.ConfirmPassword == undefined || $scope.DangKy.ConfirmPassword.length == 0) {
				$('.ConfirmPassword_regist_error_prompt').css('display', 'block')
				$('.ConfirmPassword_regist_error_prompt').text('Nhập lại mật khẩu không được bỏ trống.')
				$("#LoginModal").animate({
					scrollTop: 0
				}, 600);
				return
			} else {
				$('.ConfirmPassword_regist_error_prompt').css('display', 'none')
				$('.ConfirmPassword_regist_error_prompt').text('')

			}
			if ($scope.DangKy.Password !== $scope.DangKy.ConfirmPassword) {
				//$scope.ErroPass = "Mật khẩu không trùng khớp"
				$('.ConfirmPassword_regist_error_prompt').css('display', 'block')
				$('.ConfirmPassword_regist_error_prompt').text('Mật khẩu không trùng khớp')
				$("#LoginModal").animate({
					scrollTop: 0
				}, 600);
				return;
			} else {
				$('.ConfirmPassword_regist_error_prompt').css('display', 'none')
				$('.ConfirmPassword_regist_error_prompt').text('')

			}
			if ($scope.responsesDK == null) {
				alert("Vui lòng xác thực bạn không phải là robot")
				return
			} else {


				$('.password_regist_error_prompt').css('display', 'none')
				$('.password_regist_error_prompt').text('')
				$scope.DangKy.UserName = $scope.DangKy.Email;
				apiService.post('/api/account/dangky', $scope.DangKy, function (result) {
					notificationService.displaySuccess('Đăng ký thành công')
					//vcRecaptchaService.reload($scope.widgetIds);
					$scope.DangNhap.UserName = $scope.DangKy.Email;
					$scope.DangNhap.PassWord = $scope.DangKy.Password;
					$scope.dangNhap();
					$scope.DangNhap = {};
					$scope.DangKy = {
						DiaChi: $scope.Location
					};
					$scope.MuaKTK = {
						DiaChi: $scope.Location
					};
					$scope.ErroEmail = "";
					$scope.ErroPass = "";
					$scope.confirm_Password = "";
					$('#LoginModal').modal('hide')
					$scope.dangnhap = true
				}, addFailed)
			}


		}


		$scope.ChiChoSo = function ($event) {
			if (isNaN(String.fromCharCode($event.keyCode))) {
				$event.preventDefault();
			}
		};
		////Mua hàng không tài khoản

		$scope.SummitKTK = SummitKTK;
		$scope.MuaKTK = {
			DiaChi: $scope.Location
		};

		function SummitKTK() {

			if ($scope.MuaKTK.Fullname == undefined || $scope.MuaKTK.Fullname.length == 0) {
				$('.Fullname_ktk_error_prompt').css('display', 'block')
				$('.Fullname_ktk_error_prompt').text('Vui lòng nhập họ và tên của bạn.')
				return
			} else {
				$('.Fullname_ktk_error_prompt').css('display', 'none')
				$('.Fullname_ktk_error_prompt').text('')
			}
			if ($scope.MuaKTK.DiaChi == undefined || $scope.MuaKTK.DiaChi.length == 0) {
				$('.DiaChi_ktk_error_prompt').css('display', 'block')
				$('.DiaChi_ktk_error_prompt').text('Hãy điền địa chỉ email của bạn.')
				return
			} else {
				$('.DiaChi_ktk_error_prompt').css('display', 'none')
				$('.DiaChi_ktk_error_prompt').text('')
			}

			if ($scope.MuaKTK.PhoneNumber == undefined || $scope.MuaKTK.PhoneNumber.length == 0) {
				$('.PhoneNumber_ktk_error_prompt').css('display', 'block')
				$('.PhoneNumber_ktk_error_prompt').text('Xin vui lòng điền số điện thoại của bạn.')
				return
			} else {
				$('.PhoneNumber_ktk_error_prompt').css('display', 'none')
				$('.PhoneNumber_ktk_error_prompt').text('')
			}
			if ($scope.MuaKTK.PhoneNumber.length <= 8) {
				$('.PhoneNumber_ktk_error_prompt').css('display', 'block')
				$('.PhoneNumber_ktk_error_prompt').text('Phải nhập đúng số điện thoại.')
				return
			} else {
				$('.PhoneNumber_ktk_error_prompt').css('display', 'none')
				$('.PhoneNumber_ktk_error_prompt').text('')
			}

			if ($scope.responsesKTK == null) {
				alert("Vui lòng xác thực bạn không phải là robot")
				return

			} else {
				$('#LoginModal').modal('hide')
				$("html, body").animate({
					scrollTop: 0
				}, 600);

				//$scope.TrangThaiKTK = false
				//$state.go("cart");
				//$('#LoginModal').modal('hide');
				Swal.fire({
					title: 'Bạn chắc xác nhận đơn hàng không?',
					showDenyButton: true,
					confirmButtonText: `Xác nhận`,

				}).then((result) => {
					if (result.isConfirmed) {
						var Model = {
							TenKhachHang: $scope.MuaKTK.Fullname,
							SoDienThoai: $scope.MuaKTK.PhoneNumber,
							DiaChi: $scope.MuaKTK.DiaChi,
							chitiet: $scope.Cart,
							MaHinhThucThanhToan: 4,


						}

						apiService.post('/api/khachhangkhongtaikhoan/create', Model, succses, falses)

						function succses(result) {
							$scope.succsess = result.data;
							$scope.succsess.ThoiGianMuonNhanHang = moment($scope.succsess.ThoiGianMuonNhanHang, "MM/DD/YYYY hh:mm:ss A").format("DD/MM/YYYY HH:mm:ss")
							$scope.ORDERINFORMATION = true
							$scope.Cart = [];
							$scope.MuaKTK = {
								DiaChi: $scope.Location
							};
							$scope.DangKy = {
								DiaChi: $scope.Location
							};
							$scope.TrangThaiKTK = true
							Swal.fire({
								position: 'top-end',
								icon: 'success',
								title: 'Xác nhận thành công',
								showConfirmButton: false,
								timer: 2000
							})
							$('.Fullname_ktk_error_prompt').css('display', 'none')
							$('.Fullname_ktk_error_prompt').text('')

							$('.DiaChi_ktk_error_prompt').css('display', 'none')
							$('.DiaChi_ktk_error_prompt').text('')
							$('.PhoneNumber_ktk_error_prompt').css('display', 'none')
							$('.PhoneNumber_ktk_error_prompt').text('')
							$('#LoginModal').modal('hide')
							$scope.dangnhap = true
							$scope.responsesKTK = null
							vcRecaptchaService.reload($scope.widgetIds);


						}

						function falses() {
							console.log("ko thanh cong")
						}


					} else {

					}

				})


			}


		}


		function addFailed(response) {
			// $scope.errors = parseErrors(response);
			if (!response.data.ModelState) {
				notificationService.displayError(response.data.Message);
				console.log(response.data.Message)
			}
			$scope.errors = [];
			for (var key in response.data.ModelState) {
				for (var i = 0; i < response.data.ModelState[key].length; i++) {
					$scope.errors.push(response.data.ModelState[key][i]);

					if (response.data.ModelState[key][i] == "Email đã tồn tại") {

						$('#server_tipregister').css('display', 'block');
						$('#server_tipregister').text('Email đã tồn tại');
						setTimeout(function () {
							$('#server_tipregister').css('display', 'none');
						}, 4000)
					}
					if (response.data.ModelState[key][i] == "Mật khẩu phải có ít nhất một chữ hoa ('A' - 'Z')" || response.data.ModelState[key][i] == "Mật khẩu phải có ít nhất một chữ số ('0' - '9')") {

						$('#server_tipregister').css('display', 'block');
						$('#server_tipregister').text(response.data.ModelState[key][i]);
						setTimeout(function () {
							$('#server_tipregister').css('display', 'none');
						}, 4000)
					}


				}
			}

		}
		/**cart**/
		$scope.Addtocart = function (item) {
			$scope.Detailproduct = {}
			$scope.Detailproduct = item;
			$(".new_popup.new_pro_QV").addClass("show")
			document.body.style.overflow = "hidden";
		}
		/**modal cart 1 */
		$scope.Addtocart2 = function () {

			var index = $scope.Cart.findIndex(x => x.MaHang == $scope.Detailproduct.MaHang)

			if (index != -1) {
				$scope.Cart[index].SoLuong += $scope.SoLuong;
				if ($scope.DaDangNhap == true) {
					$scope.Cart[index].ThanhTien = $scope.Cart[index].DonGia * $scope.Cart[index].SoLuong;
					apiService.post('api/chitietchungtudathang/checktrung', $scope.Cart[index], function (result) {
						console("ok")

					}, function () { }, {
						ignoreLoadingBar: true
					})
				}

			} else {
				var chitiet = {
					MaHang: $scope.Detailproduct.MaHang,
					TenHang: $scope.Detailproduct.TenHang,
					HinhAnh: $scope.Detailproduct.HinhAnh,
					SoLuong: $scope.SoLuong,
					DonGia: $scope.Detailproduct.GiaBan,
					MaDonViTinh: $scope.Detailproduct.MaDonViTinh,
					MoTa: $scope.Detailproduct.MoTa,
					ThanhTien: $scope.Detailproduct.GiaBan * $scope.SoLuong,
				}
				if ($scope.DaDangNhap == true) {
					apiService.post('api/chitietchungtudathang/checkkotrung', chitiet, function (result) {
						$scope.Cart.push(result.data)

					}, function () { })

				} else {
					$scope.Cart.push(chitiet)
				}


			}
			$scope.SoLuong = 1;
			$scope.Detailproduct = {}
			$(".new_popup.new_pro_QV").removeClass("show")
			$("#product_cart_popup").find(".new_popup").addClass("show")
			document.body.style.overflow = "hidden";


		}

		$scope.ApiVolume = function (item) {
			if (item.SoLuong == 0) {
				item.SoLuong = 1
			} else if (item.SoLuong > 0) {
				if ($scope.DaDangNhap == true) {
					item.ThanhTien = item.DonGia * item.SoLuong;
					apiService.post('api/chitietchungtudathang/checktrung', item, function (result) {
						var index = $scope.Cart.findIndex(x => x.MaHang == item.MaHang)
						$scope.Cart[index].ThanhTien = result.data.ThanhTien
						$scope.Cart[index].SoLuong = result.data.SoLuong

					}, function () { }, {
						ignoreLoadingBar: true
					})
				}
			}

		}

		$scope.closecart = function () {
			$(".new_popup.new_pro_QV").removeClass("show")
			$("#product_cart_popup").find(".new_popup").removeClass("show")
			document.body.style.overflow = "auto";
		}


		/**volume input So luong */

		$scope.volume = function (i) {
			if (i == "+") {
				$scope.SoLuong += 1
			} else if (i == "-") {
				if ($scope.SoLuong == 1) {
					$scope.SoLuong = 1
				} else if ($scope.SoLuong > 1) {
					$scope.SoLuong -= 1
				}

			}

		}
		$scope.ThanhToan = function () {
			if ($scope.DaDangNhap == false) {
				$('#LoginModal').modal('show')

			} else {
				var config = {
					params: {
						mact: $scope.Cart[0].MaChungTuDatHang,

					}
				}

				Swal.fire({
					title: 'Bạn chắc xác nhận đơn hàng không ?',
					showDenyButton: true,
					confirmButtonText: `Xác nhận`,

				}).then((result) => {
					if (result.isConfirmed) {
						apiService.get('api/chungtudathang/thanhtoan', config, function (result) {
							$scope.succsess = result.data;
							$scope.succsess.ThoiGianMuonNhanHang = moment($scope.succsess.ThoiGianMuonNhanHang, "MM/DD/YYYY hh:mm:ss A").format("DD/MM/YYYY HH:mm:ss")
							$scope.ORDERINFORMATION = true

							if (result.data == "") {
								$scope.Cart = [];
								Swal.fire({
									position: 'top-end',
									icon: 'success',
									title: 'Xác nhận thành công',
									showConfirmButton: false,
									timer: 1500
								})
							}
						},
							function () { })
					} else {

					}

				})

			}

		}

		/*****lấy giỏ hàng  */

		if (localStorageService.get("TokenInfo")) {
			$scope.DaDangNhap = true;
			$scope.LayGioHang = function () {
				apiService.get('api/chitietchungtudathang/laygiohang', null, function (result) {
					if (result.data.length > 0) {
						$scope.Cart = result.data;
					}
				}, function () {

				})
			}
			$scope.LayGioHang();

		} else {
			$scope.DaDangNhap = false;
		}

		/***Xóa giỏ hàng */
		$scope.remove = function (item) {
			if ($scope.DaDangNhap == true) {
				var config = {
					params: {
						mahang: item.MaHang,
						mact: item.MaChungTuDatHang
					}
				}
				apiService.del('api/chitietchungtudathang/delete', config, function (result) {
					var index = $scope.Cart.findIndex(x => x.MaHang == item.MaHang)
					$scope.Cart.splice(index, 1)
					if (result.data == true) {
						$scope.closecart();
					}
				}, function () { })


			} else {
				var index = $scope.Cart.findIndex(x => x.MaHang == item.MaHang)
				$scope.Cart.splice(index, 1)
				if ($scope.Cart.length == 0) {
					$scope.closecart();
				}

			}

		}


		//mở đăng ký
		$scope.openregist = function () {
			$('#LoginModal').modal('show')
			$scope.dangnhap = false
			$scope.checkCoTK = false

		}
		//mở đăng nhập
		$scope.openlogin = function () {
			$('#LoginModal').modal('show')
			$scope.dangnhap = true;
			$scope.checkCoTK = false
			$scope.closemenumobile()

		}
		//Đăng nhập
		$scope.dangNhap = function () {
			if ($scope.DangNhap.UserName == "" || $scope.DangNhap.PassWord == "") {
				if ($scope.DangNhap.UserName == "") {
					$('#email_erro_login').css('display', 'block');
					$('#email_erro_login').text('Hãy điền địa chỉ email của bạn.');
				} else {
					$('#email_erro_login').css('display', 'none');
					$('#email_erro_login').text('');

				}

				if ($scope.DangNhap.PassWord == "") {
					$('#passw_erro_login').css('display', 'block');
					$('#passw_erro_login').text('Xin hãy điền mật khẩu.');
				} else {
					$('#passw_erro_login').css('display', 'none');
					$('#passw_erro_login').text('');
				}

			} else {
				loginService.login($scope.DangNhap.UserName, $scope.DangNhap.PassWord).then(function (response) {
					if (response !== null && response.data.error !== undefined) {
						$('.tishi_02').css('display', 'block');
						$('.tishi_02 span').text('Đăng nhập không đúng');
						$scope.DangNhap.UserName = "";
						$scope.DangNhap.PassWord = "";
						//notificationService.displayError("Đăng nhập không đúng.");//đăng nhập sai
						$scope.DaDangNhap = false;
						setTimeout(function () {
							$('.tishi_02').css('display', 'none');
						}, 4000);
					} else {
						$scope.DaDangNhap = true;
						$('#LoginModal').modal('hide')
						$('.hreder_wrapp').css("padding", '0')
						$('.header_sign_more_main').css("padding", '0px 20px 20px')
						$(".top_sign_a").find("span").text($scope.authentication.fullName);
						apiService.post('api/chitietchungtudathang/Listcart', $scope.Cart, function (result) {
							if (result.data.length != 0) {
								$scope.Cart = result.data;
							}
						}, function () {

						})

					}
				});

			}

		}

		$scope.$watch('Cart', item => {
			var i;
			if (item) {
				item.forEach(value => {
					value.ThanhTien = value.DonGia * value.SoLuong
					value.TotalGia = value.DonGia * value.SoLuong
					i = value
				})
			}


		}, true)

		if ($scope.token) {
			$scope.DaDangNhap = true
		}
		if ($scope.DaDangNhap == true) {
			$(".top_sign_a").find("span").text($scope.authentication.fullName);
			$('.header_sign_more_main').css("padding", '0px 20px 20px')
			$('.hreder_wrapp').css("padding", '0')
		} else {
			$('.hreder_wrapp').css("padding", '20px 0 16px')
			$(".top_sign_a").find("span").text("Đăng nhập");
		}


		/**search  more */
		$(function () {
			$('.header_main_search_txt').focus(function () {
				if ($scope.fliter_search.length == 0) {
					$('.fs_search_results, .fs_search_default').css('display', 'block')
				} else {
					$('.fs_search_results, .fs_search_default').css('display', 'block')
				}


			});


			$('.header_main_search_txt').blur(function () {
				setTimeout(function () {
					$('.fs_search_results, .fs_search_default').css('display', 'none')

				}, 400);

			});
			//Lọc tìm kiếm
			$scope.search_time_result = function () {
				var length = $scope.search_result.length;
				if (length > 0) {
					$scope.limitsearch = 10
					$('.fs_search_default').css('display', 'none')
					$('.real_time_results').css('display', 'block')
					$scope.item = {}
					$scope.fliter_search = []; /**array searc*/
					angular.forEach($scope.TatCaHangHoa, function (i) {
						if (i.TenHang.toLowerCase().replace(/[^a-zA-Z ]/g, "").includes($scope.search_result.toLowerCase().replace(/[^a-zA-Z ]/g, "")) == true) {
							$scope.item.MaHang = i.MaHang;
							$scope.item.TenHang = i.TenHang;
							$scope.item.HinhAnh = i.HinhAnh;
							$scope.item.GiaBan = i.GiaBan;
							$scope.fliter_search.push($scope.item)
							$scope.item = {}
						}
					})


				} else {
					$('.fs_search_default').css('display', 'block')
					$('.real_time_results').css('display', 'none');
					$scope.fliter_search = []
				}
			}

		});
		/**search mobile**/
		$(".m_header_proSearch_btn").click(function () {

			$(".nk-search-hint").css("display", "block")
		})
		$(".nki-arow-back").click(function () {

			$(".nk-search-hint").css("display", "none")
		})
		$scope.limitsearch = 10
		$scope.readmore = function () {
			$scope.limitsearch += 5


		}


		/**menu reponsive*/
		$('.header_top_list ').click(function () {
			var check = $('.ob_nav_bar').find('.close').length
			if (check != 0) {
				$(this).addClass('active')
				$(this).addClass('open')
				$(this).removeClass('close')

				$(".header_sidebar").css({
					"transition": "0.5s",
					"left": "0px"
				})

			} else {
				$(this).removeClass('active')
				$(this).addClass('close')
				$(this).removeClass('open')

				$(".header_sidebar").css({
					"transition": "0.5s",
					"left": "-200%"
				})
			}

		})
		$('.m_footer_03_one ').click(function () {
			if ($(this).find(".show").length != 0) {
				$(this).find(".m_footer_03_inner ").removeClass('show')
				$(this).find(".m_footer_03_two").css('display', 'none', 'transition', '.5s')
			} else {
				$(this).find(".m_footer_03_inner ").addClass('show')
				$(this).find(".m_footer_03_two").css('display', 'block', 'transition', '.5s')
			}


		})

		$scope.openinfologin = function (o) {
			if (o == 'open') {
				$('.header_sidebar_second.show_for_login').css({
					"left": "0",
					"transition": "all .3s ease-in-out"
				})
			} else {
				$('.header_sidebar_second.show_for_login').css({
					"left": "300%",
					"transition": "all .3s ease-in-out"
				})

			}

		}
		$scope.closemenumobile = function () {
			/**close menu */
			var thiss = $('.header_top_list');
			thiss.removeClass('active')
			thiss.addClass('close')
			thiss.removeClass('open')

			$(".header_sidebar").css({
				"transition": "0.5s",
				"left": "-200%"
			})

		}


		function getLocation() {

			// check to make sure geolocation is possible
			if (navigator.geolocation) {
				var options = {
					enableHighAccuracy: true,
					timeout: 5000,
					maximumAge: 0
				};
				navigator.geolocation.getCurrentPosition(success, error, options);
			} else {
				console.log('Geolocation is not supported');
			}
		}

		function error(err) {
			console.warn(`ERROR(${err.code}): ${err.message}`);
		}

		function success(pos) {

			var query = pos.coords.latitude + ',' + pos.coords.longitude;
			console.log('coordinates: ' + query);
			console.log('accuracy: ' + pos.coords.accuracy + ' meters.');

			// now we have coordinates, it is time to use them to  
			// do some reverse geocoding to get back the location information
			var api_url = 'https://api.opencagedata.com/geocode/v1/json'
			var apikey = 'e1e6a86cf1964d40a9ace53298998d73';

			var request_url = api_url +
				'?' +
				'key=' + apikey +
				'&q=' + encodeURIComponent(query) +
				'&pretty=1' +
				'&no_annotations=1';

			// now we follow the steps in the OpenCage javascript tutorial 
			// full example:
			// https://opencagedata.com/tutorials/geocode-in-javascript

			var request = new XMLHttpRequest();
			request.open('GET', request_url, true);

			request.onload = function () {
				// see full list of possible response codes:
				// https://opencagedata.com/api#codes

				if (request.status === 200) { // Success!
					var data = JSON.parse(request.responseText);
					//alert(data.results[0].formatted);
					$scope.MuaKTK.DiaChi = data.results[0].formatted.replace('Province, Vietnam', '');
					$scope.Location = data.results[0].formatted.replace('Province, Vietnam', '');
					$scope.DangKy.DiaChi = data.results[0].formatted.replace('Province, Vietnam', '');


				} else if (request.status <= 500) {
					// We reached our target server, but it returned an error

					console.log("unable to geocode! Response code: " + request.status);
					var data = JSON.parse(request.responseText);
					console.log('error msg: ' + data.status.message);
				} else {
					console.log("server error");
				}
			};
			request.onerror = function () {
				// There was a connection error of some sort
				console.log("unable to connect to server");
			};
			request.send(); // make the request
		}
		$(document).ready(function () {
			getLocation();
		})

		var test = "Vĩnh Thanh, Rach Gia City, Kien Giang "
		var pieces = test.split(",");
		//alert(pieces[pieces.length - 1]); // alerts "today?"
		//alert(pieces[pieces.length - 2]); // alerts "today?"

		$scope.SendEmail = function () {
			if ($scope.EmailRecovery == undefined || $scope.EmailRecovery.length == 0) {
				$('.EmailRecovery_Sendmail_error_prompt').css('display', 'block')
				$('.EmailRecovery_Sendmail_error_prompt').text('Vui lòng nhập email của bạn.');
				return;

			} else {
				$('.EmailRecovery_Sendmail_error_prompt').css('display', 'none')
				$('.EmailRecovery_Sendmail_error_prompt').text('');
				var config = {

					email: $scope.EmailRecovery,
					url: 'https://' + window.location.host + '/#!/resetpassword'

				}
				apiService.post('api/account/ResetPassword', config, function (result) {

					$('.EmailRecovery_Sendmail_error_prompt').css('display', 'block')
					$('.EmailRecovery_Sendmail_error_prompt').text('Chúng tôi đã gửi một email đến địa chỉ ' + $scope.EmailRecovery + ' vui lòng truy cập email trên để đổi lại mật khẩu');
					//Swal.fire({
					//	title: 'Chúng tôi đã gửi một email đến địa chỉ ' + $scope.EmailRecovery + ' vui lòng truy cập email trên để đổi lại mật khẩu',
						
					//	confirmButtonText: `Xác nhận`,

					//}).then((result) => {
					//	if (result.isConfirmed) {
					//		$scope.EmailRecovery = null;
					//		$("#EmailRecovery_Sendmail").text(" ")
						
					//	}
					//})



					
					
				}, function (erro) {
					
					$('.EmailRecovery_Sendmail_error_prompt').css('display', 'block')
					$('.EmailRecovery_Sendmail_error_prompt').text(erro.data.Message);
				})

			}

			

		}


		


		$scope.AllCoSo();


	}


})(angular.module('apphome'));