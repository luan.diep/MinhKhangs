﻿(function () {
    angular.module('apphome.contact', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('contact', {
          url: "/contact",
          parent: 'base',
            templateUrl: "apphome/conponents/contact/ContactView.html?v=" + window.appVersion,
            controller: "ContactController"
      })

    }
})(); 