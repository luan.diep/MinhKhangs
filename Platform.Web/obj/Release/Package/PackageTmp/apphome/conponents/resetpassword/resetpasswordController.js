﻿
(function (app) {
    app.controller('resetpasswordController', resetpasswordController);
    resetpasswordController.$inject = ['$scope', 'loginService', '$injector', 'notificationService', 'localStorageService', 'apiService', '$timeout', '$stateParams','$state'];
    function resetpasswordController($scope, loginService, $injector, notificationService, localStorageService, apiService, $timeout, $stateParams,$state) {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        $('#ResetPasswordModal').modal("show")

        var a = $stateParams.code;
        a = a.replace(/ /g, '+')

        $scope.Fogotpasword = {
            code: a
           

        }
      
        
        $scope.SendReset = function () {
            if ($scope.Fogotpasword.code == undefined || $scope.Fogotpasword.code == null ) {
                $('.code_Fogotpasword_error_prompt').css('display', 'block')
                $('.code_Fogotpasword_error_prompt').text('Mã token không được bỏ trống.')
                return
            } else {
                $('.code_Fogotpasword_error_prompt').css('display', 'none')
                $('.code_Fogotpasword_error_prompt').text('')
            }
            if ($scope.Fogotpasword.NewPassword == undefined || $scope.Fogotpasword.NewPassword == null || $scope.Fogotpasword.NewPassword.length == 0) {
                $('.NewPassword_Fogotpasword_error_prompt').css('display', 'block')
                $('.NewPassword_Fogotpasword_error_prompt').text('Mật khẩu không được bỏ trống')
                return
            } else {
                $('.NewPassword_Fogotpasword_error_prompt').css('display', 'none')
                $('.NewPassword_Fogotpasword_error_prompt').text('')
            }
            var config = {
                id: $stateParams.userId,
                code: $scope.Fogotpasword.code,
                newpassword: $scope.Fogotpasword.NewPassword
            }


            apiService.post('api/account/ConfirmResetPassword', config, function (result) {
                $('#ResetPasswordModal').modal("hide")
                window.location.replace('https://' + window.location.host +'/#!/trangchu');
                $scope.openlogin()
              
            


            }, function () {

                alert("erro")
            })






        }
           
        
       





    }
})(angular.module('apphome.resetpassword'));
