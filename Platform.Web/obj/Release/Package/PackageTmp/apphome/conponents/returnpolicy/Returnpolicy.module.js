﻿(function () {
    angular.module('apphome.returnpolicy', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('returnpolicy', {
          url: "/returnpolicy",
          parent: 'base',
            templateUrl: "apphome/conponents/returnpolicy/ReturnpolicyView.html?v=" + window.appVersion,
            controller: "ReturnpolicyController"
      })

    }
})(); 