﻿(function () {
    angular.module('apphome.support', ['apphome.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('support', {
          url: "/support",
          parent: 'base',
            templateUrl: "apphome/conponents/support/supPortView.html?v=" + window.appVersion,
            controller: "supPortController"
      })

    }
})(); 