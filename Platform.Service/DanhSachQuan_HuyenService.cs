﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IDanhSachQuan_HuyenService
    {
        void Add(DanhSachQuan_Huyen DanhSachQuan_Huyen);
        void Update(DanhSachQuan_Huyen DanhSachQuan_Huyen);
        void delete(int id);
        IEnumerable<DanhSachQuan_Huyen> GetAll();
        DanhSachQuan_Huyen GetByID(string id);
        void Commit();
        void Save();
        IEnumerable<DanhSachQuan_Huyen> LayTatCaKemTinh();
        IEnumerable<DanhSachQuan_Huyen> ChonTheoTinh(string matinh);
    }
    public class DanhSachQuan_HuyenService : IDanhSachQuan_HuyenService
    {
        IDanhSachQuan_HuyenRepository _DanhSachQuan_HuyenRepository;
        IUnitOfWork _unitOfWork;
        public DanhSachQuan_HuyenService(IDanhSachQuan_HuyenRepository DanhSachQuan_HuyenRepository, IUnitOfWork unitOfWork)
        {
            this._DanhSachQuan_HuyenRepository = DanhSachQuan_HuyenRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(DanhSachQuan_Huyen DanhSachQuan_Huyen)
        {
            _DanhSachQuan_HuyenRepository.Add(DanhSachQuan_Huyen);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _DanhSachQuan_HuyenRepository.Delete(id);
        }

        public IEnumerable<DanhSachQuan_Huyen> GetAll()
        {
            return _DanhSachQuan_HuyenRepository.GetAll();
        }




        public void Update(DanhSachQuan_Huyen DanhSachQuan_Huyen)
        {
            _DanhSachQuan_HuyenRepository.Update(DanhSachQuan_Huyen);
        }

        public DanhSachQuan_Huyen GetByID(string id)
        {
            return _DanhSachQuan_HuyenRepository.GetSingleById(id);
        }

        public IEnumerable<DanhSachQuan_Huyen> LayTatCaKemTinh()
        {
            return _DanhSachQuan_HuyenRepository.GetAll(new string[] { "DanhSachTinh_TP" }).ToList();
        }

        public IEnumerable<DanhSachQuan_Huyen> ChonTheoTinh(string matinh)
        {
            return _DanhSachQuan_HuyenRepository.ChonTheoTinh(matinh);
        }
    }
}