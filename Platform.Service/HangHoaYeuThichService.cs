﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IHangHoaYeuThichService
    {
        void Add(HangHoaYeuThich hangHoaYeuThich);
        void Update(HangHoaYeuThich hangHoaYeuThich);
        void delete(int id);
        IEnumerable<HangHoaYeuThich> GetAll();
        HangHoaYeuThich GetByID(string id);
        IEnumerable<HangHoaYeuThich> getmaKH(string maKH);
        HangHoaYeuThich getmahang(string mahang);

        void Commit();
        void Save();
     
    }
    public class HangHoaYeuThichService : IHangHoaYeuThichService
    {
        IHangHoaYeuThichRepository _hangHoaYeuThichRepository;
        IUnitOfWork _unitOfWork;
        public HangHoaYeuThichService(IHangHoaYeuThichRepository hangHoaYeuThichRepository, IUnitOfWork unitOfWork)
        {
            this._hangHoaYeuThichRepository = hangHoaYeuThichRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(HangHoaYeuThich hangHoaYeuThich)
        {
            _hangHoaYeuThichRepository.Add(hangHoaYeuThich);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _hangHoaYeuThichRepository.Delete(id);
        }

        public IEnumerable<HangHoaYeuThich> GetAll()
        {
            return _hangHoaYeuThichRepository.GetAll();
        }




        public void Update(HangHoaYeuThich hangHoaYeuThich)
        {
            _hangHoaYeuThichRepository.Update(hangHoaYeuThich);
        }

        public HangHoaYeuThich GetByID(string id)
        {
            return _hangHoaYeuThichRepository.GetSingleById(id);
        }

        public IEnumerable<HangHoaYeuThich> getmaKH(string maKH)
        {
            return _hangHoaYeuThichRepository.GetMulti( x=>x.MaKhachHang==maKH);
        }

        public HangHoaYeuThich getmahang(string mahang)
        {
            var item= _hangHoaYeuThichRepository.GetSingleByCondition(x => x.MaHang == mahang); ;
            if (item == null)
            {
                 item=null;
            }
            return item;
           
             
        }
    }
}