﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IChungTuGiaoHangService
    {
        void Add(ChungTuGiaoHang ChungTuGiaoHang);
        void Update(ChungTuGiaoHang ChungTuGiaoHang);
        void delete(int id);
        IEnumerable<ChungTuGiaoHang> GetAll();
        ChungTuGiaoHang GetByID(int id);
        ChungTuGiaoHang GetByID(String id);
        string GetLastID();
      
        void Commit();
        void Save();
     

    }
    public class ChungTuGiaoHangService : IChungTuGiaoHangService
    {
        IChungTuGiaoHangRepository _ChungTuGiaoHangRepository;
        IUnitOfWork _unitOfWork;
        public ChungTuGiaoHangService(IChungTuGiaoHangRepository ChungTuGiaoHangRepository, IUnitOfWork unitOfWork)
        {
            this._ChungTuGiaoHangRepository = ChungTuGiaoHangRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(ChungTuGiaoHang ChungTuGiaoHang)
        {
            _ChungTuGiaoHangRepository.Add(ChungTuGiaoHang);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _ChungTuGiaoHangRepository.Delete(id);
        }

        public IEnumerable<ChungTuGiaoHang> GetAll()
        {
            return _ChungTuGiaoHangRepository.GetAll();
        }

        public ChungTuGiaoHang GetByID(int id)
        {
            return _ChungTuGiaoHangRepository.GetSingleById(id);
        }


        public void Update(ChungTuGiaoHang ChungTuGiaoHang)
        {
            _ChungTuGiaoHangRepository.Update(ChungTuGiaoHang);
        }

        public ChungTuGiaoHang GetByID(string id)
        {
            return _ChungTuGiaoHangRepository.GetSingleById(id);
        }

        public string GetLastID()
        {
            return _ChungTuGiaoHangRepository.GetLastID();
        }

       
    }

}
