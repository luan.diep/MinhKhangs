﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface INhomHangHoaService
    {
        void Add(NhomHangHoa nhomHangHoa);
        void Update(NhomHangHoa nhomHangHoa);
        void delete(int id);
        IEnumerable<NhomHangHoa> GetAll();
        IEnumerable<Catelogy_product> getallCatelogy();
        NhomHangHoa GetByID(int id);
        NhomHangHoa getID(int id);
       
        IEnumerable<NhomHangHoa_Con> getMaNhom(int id);
        IEnumerable<NhomHangHoaGroups> getMaNhoms(List<NhomHangHoa_Con> id);

        void Commit();
        void Save();

    }

   
    public class NhomHangHoaService : INhomHangHoaService
    {
        INhomHangHoaRepository _nhomHangHoaRepository;
        IHangHoaRepository _hangHoaRepository;
        INhomHangHoaConRepository _nhomHangHoaConRepository;
        IUnitOfWork _unitOfWork;
        public NhomHangHoaService(INhomHangHoaRepository nhomHangHoaRepository, INhomHangHoaConRepository nhomHangHoaConRepository, IHangHoaRepository hangHoaRepository, IUnitOfWork unitOfWork)
        {
            this._nhomHangHoaRepository = nhomHangHoaRepository;
            this._hangHoaRepository = hangHoaRepository;
            this._nhomHangHoaConRepository = nhomHangHoaConRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(NhomHangHoa nhomHangHoa)
        {
            _nhomHangHoaRepository.Add(nhomHangHoa);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }
		 

        public void delete(int id)
        {
            _nhomHangHoaRepository.Delete(id);
        }

        public IEnumerable<NhomHangHoa> GetAll()
        {
            return _nhomHangHoaRepository.GetAll();
        }

        public NhomHangHoa GetByID(int id)
        {
            return _nhomHangHoaRepository.GetSingleById(id);
        }

       

        public void Update(NhomHangHoa nhomHangHoa)
        {
            _nhomHangHoaRepository.Update(nhomHangHoa);
        }

        public IEnumerable<Catelogy_product> getallCatelogy()
        {
            return _nhomHangHoaRepository.getallCatelogy();
        }

        public NhomHangHoa getID(int id)
        {
            return _nhomHangHoaRepository.GetSingleByCondition(x=>x.MaNhomHH==id);
        }

        

        public IEnumerable<NhomHangHoa_Con> getMaNhom(int id)
        {
            return _nhomHangHoaConRepository.GetMulti(x=>x.MaNhomHHGoc==id);
        }

        public IEnumerable<NhomHangHoaGroups> getMaNhoms(List<NhomHangHoa_Con> id)
        {
            List<NhomHangHoaGroups> list = new List<NhomHangHoaGroups>();
            foreach (var item in id)
            {
                list.Add(_nhomHangHoaRepository.getbyid(item.MaNhomHHPhu.Value));  
            }
            return list;
        }
    }
}
