﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IHangHoa_CoSoService
    {
        void Add(HangHoa_CoSo hangHoa_CoSo);
        void Update(HangHoa_CoSo hangHoa_CoSo);
        void delete(int id);
        IEnumerable<HangHoa_CoSo> GetAll();
        HangHoa_CoSo GetByID(int id);
        IEnumerable<HangHoa_CoSo> GetByMaHang(string mahang);
        HangHoa_CoSo getbycondition(string mahang,string macoso);

        IEnumerable<LayKhuyenMai> GetKhuyenMai();

        void Commit();
        void Save();


    }
    public class HangHoa_CoSoService : IHangHoa_CoSoService
    {
        IHangHoa_CoSoRepository _hangHoa_CoSoRepository;
        IHangHoaRepository _hangHoaRepository;
        IUnitOfWork _unitOfWork;
        public HangHoa_CoSoService(IHangHoa_CoSoRepository hangHoa_CoSoRepository, IHangHoaRepository hangHoaRepository, IUnitOfWork unitOfWork)
        {
            this._hangHoa_CoSoRepository = hangHoa_CoSoRepository;
            this._hangHoaRepository = hangHoaRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(HangHoa_CoSo hangHoa_CoSo)
        {
            _hangHoa_CoSoRepository.Add(hangHoa_CoSo);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }
		 

        public void delete(int id)
        {
            _hangHoa_CoSoRepository.Delete(id);
        }

        public IEnumerable<HangHoa_CoSo> GetAll()
        {
            return _hangHoa_CoSoRepository.GetAll();
        }

        public HangHoa_CoSo GetByID(int id)
        {
            return _hangHoa_CoSoRepository.GetSingleById(id);
        }

       

        public void Update(HangHoa_CoSo hangHoa_CoSo)
        {
            _hangHoa_CoSoRepository.Update(hangHoa_CoSo);
        }

        

        

      
        public HangHoa_CoSo getbycondition(string mahang, string macoso)
        {
            return _hangHoa_CoSoRepository.GetSingleByCondition(x => x.MaCoSo == macoso && x.MaHang == mahang);
        }

        public IEnumerable<HangHoa_CoSo> GetByMaHang(string mahang)
        {
            return _hangHoa_CoSoRepository.GetMulti(x => x.MaHang == mahang);
        }

       
        IEnumerable<LayKhuyenMai> IHangHoa_CoSoService.GetKhuyenMai()
        {
            return _hangHoa_CoSoRepository.GetKhuyenMai();
        }
    
    }
}
