﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface INhomHangHoaConService
    {
        void Add(NhomHangHoa_Con nhomHangHoa);
        void Update(NhomHangHoa_Con nhomHangHoa);
        void delete(int id);
        IEnumerable<NhomHangHoa_Con> GetAll();
       
        NhomHangHoa_Con GetByID(int id);
        NhomHangHoa_Con gettolist(int id);
      
        void Commit();
        void Save();

    }

   
    public class NhomHangHoaConService : INhomHangHoaConService
    {
      
        INhomHangHoaConRepository _nhomHangHoaConRepository;
        IUnitOfWork _unitOfWork;
        public NhomHangHoaConService(INhomHangHoaConRepository nhomHangHoaConRepository, IUnitOfWork unitOfWork)
        {
           
            this._nhomHangHoaConRepository = nhomHangHoaConRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(NhomHangHoa_Con nhomHangHoa)
        {
            _nhomHangHoaConRepository.Add(nhomHangHoa);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }
		 

        public void delete(int id)
        {
            _nhomHangHoaConRepository.Delete(id);
        }

        public IEnumerable<NhomHangHoa_Con> GetAll()
        {
            return _nhomHangHoaConRepository.GetAll();
        }

        public NhomHangHoa_Con GetByID(int id)
        {
            return _nhomHangHoaConRepository.GetSingleById(id);
        }

       

        public void Update(NhomHangHoa_Con nhomHangHoa)
        {
            _nhomHangHoaConRepository.Update(nhomHangHoa);
        }

        public NhomHangHoa_Con gettolist(int id)
        {
            return _nhomHangHoaConRepository.gettolist(id);
        }
    }
}
