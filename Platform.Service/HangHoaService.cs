﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IHangHoaService
    {
        void Add(HangHoa hangHoa);
        void Update(HangHoa hangHoa);
        void delete(int id);
        IEnumerable<HangHoa> GetAll();
        HangHoa GetByIDs(int id);
        HangHoa GetByID(string id);
        HangHoa gethinhanh(int mahang);
        IEnumerable< HangHoa >getmanhom(int manhom);
        HangHoa hinhanh(string mahang);
        HangHoaOnl layhanghoaonlById(string id);
        IEnumerable<HangHoa> layhanghoadathang(string keyword = null);
        IEnumerable<Homecaterogy> Groupproduct(int manhom);
        string gettennhom(int manhom);

        void Commit();
        void Save();
        string GetMaHangHoaCuoiCung(); 
      
        IEnumerable<HangHoa> GetByName(string id);
        IEnumerable<HangHoaOnl> layhanghoaonl();
        
        IEnumerable<Homecaterogy> homecategory();
        IEnumerable<HangHoaOnl> layhanghoaonls(int id);



    }
    public class HangHoaService : IHangHoaService
    {
        IHangHoaRepository _hangHoaRepository;
        INhomHangHoaRepository _nhomHangHoaRepository;
        IUnitOfWork _unitOfWork;
        public HangHoaService(IHangHoaRepository hangHoaRepository, IUnitOfWork unitOfWork, INhomHangHoaRepository nhomHangHoaRepository)
        {
            this._hangHoaRepository = hangHoaRepository;
            this._nhomHangHoaRepository = nhomHangHoaRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(HangHoa hangHoa)
        {
            _hangHoaRepository.Add(hangHoa);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }
		 

        public void delete(int id)
        {
            _hangHoaRepository.Delete(id);
        }

        public IEnumerable<HangHoa> GetAll()
        {
            return _hangHoaRepository.GetAll();
        }

        public HangHoa GetByIDs(int id)
        {
            return _hangHoaRepository.GetSingleById(id);
        }

       

        public void Update(HangHoa hangHoa)
        {
            _hangHoaRepository.Update(hangHoa);
        }

        public HangHoa GetByID(string id)
        {
            return _hangHoaRepository.GetSingleByCondition(x => x.MaHang==id);
        }

      

        public string GetMaHangHoaCuoiCung()
        {
            return _hangHoaRepository.GetMaHangHoaCuoiCung();
        }


        public IEnumerable<HangHoa> layhanghoadathang(string keyword = null)
        {
            var hh = _hangHoaRepository.GetMulti(x => x.DaXoa != true && x.NgungKinhDoanh != true && x.TonKho > 0);
            if (!string.IsNullOrEmpty(keyword))
            {
                return hh.Where(x => x.MaHang.Contains(keyword) || x.TenHang.Contains(keyword));
            }
            else
            {
                return hh;
            }

        }
      

      

        public IEnumerable<HangHoa> GetByName(string id)
        {
            return _hangHoaRepository.GetByName(id);
        }

        public HangHoa gethinhanh(int mahang)
        {
            return _hangHoaRepository.GetSingleById(mahang);
        }

        public IEnumerable<HangHoaOnl> layhanghoaonl()
        {
            return _hangHoaRepository.layhanghoaonl();
        }

        public HangHoaOnl layhanghoaonlById(string id)
        {
            return _hangHoaRepository.layhanghoaonlById(id);
        }

        public HangHoa hinhanh(string mahang)
        {
            return _hangHoaRepository.GetSingleByCondition(x=>x.MaHang==mahang);
        }

        public IEnumerable<Homecaterogy> homecategory()
        {
            var List = new List<Homecaterogy>();
            var allnhom = _nhomHangHoaRepository.GetAll();
            foreach (var item in allnhom)
            {
                var a = _hangHoaRepository.homecategory(item.MaNhomHH);
                List.AddRange(a);

            }
            return List;
        }

        public IEnumerable<Homecaterogy> Groupproduct(int manhom)
        {
            return _hangHoaRepository.Groupproduct(manhom);
        }

        public IEnumerable<HangHoaOnl> layhanghoaonls(int id)
        {
            return _hangHoaRepository.layhanghoaonls(id);
        }

        public IEnumerable< HangHoa >getmanhom(int manhom)
        {
            return _hangHoaRepository.GetMulti(x => x.MaNhomHH == manhom);
        }

        public string gettennhom(int manhom)
        {
            return _nhomHangHoaRepository.GetSingleByCondition(x => x.MaNhomHH == manhom).TenNhom;
        }
    }
}
