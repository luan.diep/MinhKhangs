﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{



    public interface IGioiThieuOnlService
    {
        void Add(GioiThieuOnl gioiThieuOnl);
        void Update(GioiThieuOnl gioiThieuOnl);
        void delete(int id);
        IEnumerable<GioiThieuOnl> GetAll();
        GioiThieuOnl GetByID(int id);
      
        void Commit();
        void Save();

    }
    public class GioiThieuOnlService : IGioiThieuOnlService
    {
        IGioiThieuOnlRepository _gioiThieuOnlRepository;
        IUnitOfWork _unitOfWork;
        public GioiThieuOnlService(IGioiThieuOnlRepository gioiThieuOnlRepository, IUnitOfWork unitOfWork)
        {
            this._gioiThieuOnlRepository = gioiThieuOnlRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(GioiThieuOnl gioiThieuOnl)
        {
            _gioiThieuOnlRepository.Add(gioiThieuOnl);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }


        public void delete(int id)
        {
            _gioiThieuOnlRepository.Delete(id);
        }

        public IEnumerable<GioiThieuOnl> GetAll()
        {
            return _gioiThieuOnlRepository.GetAll();
        }

        public GioiThieuOnl GetByID(int id)
        {
            return _gioiThieuOnlRepository.GetSingleById(id);
        }



        public void Update(GioiThieuOnl gioiThieuOnl)
        {
            _gioiThieuOnlRepository.Update(gioiThieuOnl);
        }

       
    }
}
