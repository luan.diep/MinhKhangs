﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IChungTuDatHangService
    {
        void Add(ChungTuDatHang chungTuDatHang);
        void Update(ChungTuDatHang chungTuDatHang);
        void delete(string id);
        IEnumerable<ChungTuDatHang> GetAll();
        IEnumerable<ChungTuDatHang>GetByKH(string maKH);
        
        ChungTuDatHang GetByID(string id);
        ChungTuDatHang GetBy(string id);


        IEnumerable<ChungTuDatHang> Detail_oderproduct(string maKH, string mact);
        string LayTenHinhThucThanhToan(int id);
        string GetNewID();
        void Commit();
        void Save();
        ChungTuDatHang getByMaCT(string mact);
    }
    public class ChungTuDatHangService : IChungTuDatHangService
    {
        IChungTuDatHangRepository _chungTuDatHangRepository;
        IUnitOfWork _unitOfWork;
        public ChungTuDatHangService(IChungTuDatHangRepository chungTuDatHangRepository, IUnitOfWork unitOfWork)
        {
            this._chungTuDatHangRepository = chungTuDatHangRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(ChungTuDatHang chungTuDatHang)
        {
            _chungTuDatHangRepository.Add(chungTuDatHang);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }
		 

        public void delete(string id)
        {
            _chungTuDatHangRepository.Delete(id);
        }

        public IEnumerable<ChungTuDatHang> GetAll()
        {
            return _chungTuDatHangRepository.GetAll();
        }

        public ChungTuDatHang GetByID(string id)
        {
            return _chungTuDatHangRepository.GetSingleById(id);
        }

       

        public void Update(ChungTuDatHang chungTuDatHang)
        {
            _chungTuDatHangRepository.Update(chungTuDatHang);
        }

        public string GetNewID()
        {
            return _chungTuDatHangRepository.GetNewID();
        }

        public IEnumerable<ChungTuDatHang> GetByKH(string maKH)
        {
            return _chungTuDatHangRepository.GetMulti(x=>x.MaKhachHang==maKH);
        }

        public ChungTuDatHang getByMaCT(string mact)
        {
            return _chungTuDatHangRepository.getByMaCT(mact);
        }

        public ChungTuDatHang GetBy(string id)
        {
            return _chungTuDatHangRepository.GetSingleByCondition(x=>x.MaChungTuDatHang==id);
        }

        public IEnumerable<ChungTuDatHang> Detail_oderproduct(string maKH, string mact)
        {
            return _chungTuDatHangRepository.GetMulti(x => x.MaKhachHang == maKH && x.MaChungTuDatHang == mact);
        }

        public string LayTenHinhThucThanhToan(int id)
        {
            return _chungTuDatHangRepository.LayTenHinhThucThanhToan(id);
        }
    }
}
